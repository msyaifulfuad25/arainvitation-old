<?php
    if (strpos($_SERVER['SERVER_NAME'], 'localhost') !== false) {
        $mode = 'dev';
    } else if (strpos($_SERVER['SERVER_NAME'], 'arainvitation.online') !== false) {
        $mode = 'prod1';
    } else if (strpos($_SERVER['SERVER_NAME'], 'arainvitation.syaifulfu.my.id') !== false) {
        $mode = 'prod2';
    } else if (strpos($_SERVER['SERVER_NAME'], 'arainvitation.loisense.id') !== false) {
        $mode = 'prod3';
    }


    date_default_timezone_set('Asia/Jakarta');


    if ($mode == 'dev') {
        $servername = "localhost";
        $username = "root";
        $password = "toor";
        $dbname = "db_arainvitation";
    } else if ($mode == 'prod1') {
        $servername = "localhost";
        $username = "araa4613_arainvitation";
        $password = "Arainvitation59";
        $dbname = "araa4613_arainvitation";
    } else if ($mode == 'prod2') {
        $servername = "localhost";
        $username = "syaifulfu_arainvitation";
        $password = "Arainvitation@2022";
        $dbname = "syaifulfu_arainvitation";
    } else if ($mode == 'prod3') {
        $servername = "localhost";
        $username = "loisense_arainvitation";
        $password = "Arainvitation@2023";
        $dbname = "loisense_arainvitation";
    }
    
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }


	if ($mode == 'dev') {
		$base_url = 'http://localhost:8001/';
    } else if ($mode == 'prod1') {
		$base_url = 'https://arainvitation.online/';
	} else if ($mode == 'prod2') {
		$base_url = 'https://arainvitation.syaifulfu.my.id/';
	} else if ($mode == 'prod3') {
		$base_url = 'https://arainvitation.loisense.id/';
	}

	$asset = $base_url.'public/';
    $admin_address = 'Jln. Raya Sumberdadi, Sumberdadi, Sumbergempol, Tulungagung, Jawa Timur, 66291';
    $admin_wa = '62895367255770';
    $admin_ig = 'ara.invitation';
    $template_question_wa = 'Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20di%20Ara%20Invitation%20nih.%20Bisa%20dibantu?';
    $discount = 50;

    // $price_platinum1 = 300000;
    // $price_platinum2 = 300000;
    // $price_platinum3 = 300000;
    // $price_platinum4 = 300000;
    // $price_platinum5 = 300000;
    // $price_gold1 = 200000;
    // $price_gold2 = 200000;
    // $price_gold3 = 200000;
    // $price_gold4 = 200000;
    // $price_gold5 = 200000;
    

    function url(){
        return sprintf(
          "%s://%s%s",
          isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
          $_SERVER['SERVER_NAME'],
          $_SERVER['REQUEST_URI']
        );
    }

    function convert_seconds($seconds) {
        $dt1 = new DateTime("@0");
        $dt2 = new DateTime("@$seconds");

        // return $dt1->diff($dt2)->format('%a days, %h hours, %i minutes and %s seconds');
        if ($seconds == 0) {
            return $dt1->diff($dt2)->format('baru saja');
        } else if ($seconds < 60) {
            return $dt1->diff($dt2)->format('%s detik yang lalu');
        } else if ($seconds == 60) {
            return $dt1->diff($dt2)->format('%i menit yang lalu');
        } else if ($seconds < 3600) {
            return $dt1->diff($dt2)->format('%i menit, %s detik yang lalu');
        } else if ($seconds >= 3600 && $seconds <= 3660) {
            return $dt1->diff($dt2)->format('%h jam yang lalu');
        } else if ($seconds < 86400) {
            return $dt1->diff($dt2)->format('%h jam, %i menit yang lalu');
        } else if ($seconds >= 86400 && $seconds <= 90000) {
            return $dt1->diff($dt2)->format('%a hari yang lalu');
        } else {
            return $dt1->diff($dt2)->format('%a hari, %h jam yang lalu');
        }
    }

    // testing gitignore
?>