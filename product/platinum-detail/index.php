
<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
		
        <style type="text/css">

            .wdp-comment-text img {

                max-width: 100% !important;

            }

        </style>

        
<!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
<title>Detail Platinum</title>
<meta name="description" content="Berikut adalah detail produk Undangan Website Platinum Unity Invitation."/>
<meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet"/>
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Detail Platinum" />
<meta property="og:description" content="Berikut adalah detail produk Undangan Website Platinum Unity Invitation." />
<meta property="og:url" content="https://unityinvitation.com/platinum-detail/" />
<meta property="og:site_name" content="Unity Invitation" />
<meta property="og:updated_time" content="2022-05-07T21:18:38+07:00" />
<meta property="og:image" content="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1.png" />
<meta property="og:image:secure_url" content="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1.png" />
<meta property="og:image:width" content="480" />
<meta property="og:image:height" content="720" />
<meta property="og:image:alt" content="Detail Platinum" />
<meta property="og:image:type" content="image/png" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Detail Platinum" />
<meta name="twitter:description" content="Berikut adalah detail produk Undangan Website Platinum Unity Invitation." />
<meta name="twitter:image" content="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1.png" />
<meta name="twitter:label1" content="Time to read" />
<meta name="twitter:data1" content="1 minute" />
<script type="application/ld+json" class="rank-math-schema">{"@context":"https://schema.org","@graph":[{"@type":"BreadcrumbList","@id":"https://unityinvitation.com/platinum-detail/#breadcrumb","itemListElement":[{"@type":"ListItem","position":"1","item":{"@id":"https://unityinvitation.com","name":"Home"}},{"@type":"ListItem","position":"2","item":{"@id":"https://unityinvitation.com/platinum-detail/","name":"Detail Platinum"}}]}]}</script>
<!-- /Rank Math WordPress SEO plugin -->

<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Feed" href="https://unityinvitation.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Comments Feed" href="https://unityinvitation.com/comments/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.9.3"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='bdt-uikit-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
<link rel='stylesheet' id='ep-helper-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://unityinvitation.com/wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
</style>
<link rel='stylesheet' id='WEDKU_STYLE-css'  href='https://unityinvitation.com/wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.16' type='text/css' media='all' />
<link rel='stylesheet' id='pafe-extension-style-css'  href='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
<link rel='stylesheet' id='wdp_style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
<style id='wdp_style-inline-css' type='text/css'>


        .wdp-wrapper {

          font-size: 14px

        }

    

        .wdp-wrapper {

          background: #ffffff;

        }

        .wdp-wrapper.wdp-border {

          border: 1px solid #d5deea;

        }



        .wdp-wrapper .wdp-wrap-comments a:link,

        .wdp-wrapper .wdp-wrap-comments a:visited {

          color: #54595f;

        }



        .wdp-wrapper .wdp-wrap-link a.wdp-link {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-wrap-form {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {

          border-color: #64B6EC;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {

          color: #FFFFFF;

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {

          background: #a3a3a3;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {

          color: #44525F;

        }



        .wdp-wrapper .wdp-media-btns a > span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-media-btns a > span:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-comment-status {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {

          color: #319342;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {

          color: #C85951;

        }

        .wdp-wrapper .wdp-comment-status.wdp-loading > span {

          color: #54595f;

        }



        .wdp-wrapper ul.wdp-container-comments {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {

          border-bottom: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {

          color: #54595f !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {

          color: #2a5782 !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {

          color: #54595f;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {

          color: #2a5782;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span:hover {

          color: #3D7DBC;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {

          color: #2C9E48;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {

          color: #D13D3D;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-counter-info {

          color: #9DA8B7;

        }



        .wdp-wrapper .wdp-holder span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a,

        .wdp-wrapper .wdp-holder a:link,

        .wdp-wrapper .wdp-holder a:visited {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a:hover,

        .wdp-wrapper .wdp-holder a:link:hover,

        .wdp-wrapper .wdp-holder a:visited:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled, .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {

          color: #9DA8B7;

        }

        

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {

        max-width: 28px;

        max-height: 28px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {

        margin-left: 38px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {

        max-width: 24px;

        max-height: 24px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {

        max-width: 21px;

        max-height: 21px;

    }

    
</style>
<link rel='stylesheet' id='wdp-centered-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-horizontal-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-fontello-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='exad-main-style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='fb032dce5-css'  href='https://unityinvitation.com/wp-content/uploads/essential-addons-elementor/fb032dce5.min.css?ver=1652083543' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-theme-style-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-skin-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
<style id='elementor-icons-inline-css' type='text/css'>

		.elementor-add-new-section .elementor-add-templately-promo-button{
            background-color: #5d4fff;
            background-image: url(https://unityinvitation.com/wp-content/plugins/essential-addons-for-elementor-lite/assets/admin/images/templately/logo-icon.svg);
            background-repeat: no-repeat;
            background-position: center center;
            margin-left: 5px;
            position: relative;
            bottom: 5px;
        }
</style>
<link rel='stylesheet' id='elementor-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-1399-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-1399.css?ver=1652056007' type='text/css' media='all' />
<link rel='stylesheet' id='powerpack-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='weddingpress-wdp-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-pro-css'  href='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='uael-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
<link rel='stylesheet' id='jet-tabs-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-61892-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-61892.css?ver=1652057179' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51207-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51207.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-31584-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-31584.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-official-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
<link rel='stylesheet' id='font-awesome-official-v4shim-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
<style id='font-awesome-official-v4shim-inline-css' type='text/css'>
@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
unicode-range: U+F004-F005,U+F007,U+F017,U+F022,U+F024,U+F02E,U+F03E,U+F044,U+F057-F059,U+F06E,U+F070,U+F075,U+F07B-F07C,U+F080,U+F086,U+F089,U+F094,U+F09D,U+F0A0,U+F0A4-F0A7,U+F0C5,U+F0C7-F0C8,U+F0E0,U+F0EB,U+F0F3,U+F0F8,U+F0FE,U+F111,U+F118-F11A,U+F11C,U+F133,U+F144,U+F146,U+F14A,U+F14D-F14E,U+F150-F152,U+F15B-F15C,U+F164-F165,U+F185-F186,U+F191-F192,U+F1AD,U+F1C1-F1C9,U+F1CD,U+F1D8,U+F1E3,U+F1EA,U+F1F6,U+F1F9,U+F20A,U+F247-F249,U+F24D,U+F254-F25B,U+F25D,U+F267,U+F271-F274,U+F279,U+F28B,U+F28D,U+F2B5-F2B6,U+F2B9,U+F2BB,U+F2BD,U+F2C1-F2C2,U+F2D0,U+F2D2,U+F2DC,U+F2ED,U+F328,U+F358-F35B,U+F3A5,U+F3D1,U+F410,U+F4AD;
}
</style>
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-unityinv-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
<script type='text/javascript' id='jquery-core-js-extra'>
/* <![CDATA[ */
var pp = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
<link rel="https://api.w.org/" href="https://unityinvitation.com/wp-json/" /><link rel="alternate" type="application/json" href="https://unityinvitation.com/wp-json/wp/v2/pages/61892" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://unityinvitation.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://unityinvitation.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.9.3" />
<link rel='shortlink' href='https://unityinvitation.com/?p=61892' />
<link rel="alternate" type="application/json+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fplatinum-detail%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fplatinum-detail%2F&#038;format=xml" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-180x180.png" />
<meta name="msapplication-TileImage" content="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-270x270.png" />
<style>.pswp.pafe-lightbox-modal {display: none;}</style>		<style type="text/css" id="wp-custom-css">
			.elementor-section{
	overflow:hidden;
}

.justify-cstm .pp-timeline-card{
	text-align:justify !important;
}		</style>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /></head>
<body data-rsssl=1 class="page-template page-template-elementor_canvas page page-id-61892 elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-61892">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>		<div data-elementor-type="wp-page" data-elementor-id="61892" class="elementor elementor-61892">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-daf7659 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="daf7659" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-bottom" data-negative="true">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fa3a842 wdp-sticky-section-no" data-id="fa3a842" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8b7e56a wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="8b7e56a" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="upside" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-dc24dcf wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="dc24dcf" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-07b1455 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="07b1455" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">De<I>tail</I></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-dd1b982 uael-fancytext-stack-no wdp-sticky-section-no elementor-widget elementor-widget-uael-fancy-heading" data-id="dd1b982" data-element_type="widget" data-settings="{&quot;fancytext_align_mobile&quot;:&quot;center&quot;,&quot;fancytext_align&quot;:&quot;center&quot;,&quot;fancytext_space_prefix&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;0&quot;,&quot;sizes&quot;:[]},&quot;fancytext_space_prefix_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;fancytext_space_prefix_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="uael-fancy-heading.default">
				<div class="elementor-widget-container">
					<div class="uael-module-content uael-fancy-text-node " data-animation="clip" data-speed="2500" data-clip_speed="600" data-pause_time="1000">
											<h3 class="uael-fancy-text-wrap uael-fancy-text-clip">
											<span class="uael-fancy-stack">
												<span class="uael-fancy-heading uael-fancy-text-main uael-slide-main uael-adjust-width">
								<span class="uael-slide-main_ul">
																														<span class="uael-slide-block uael-active-heading"><span class="uael-slide_text">Produk</span>
											</span>
																																										<span class="uael-slide-block "><span class="uael-slide_text">Harga</span>
											</span>
																																										<span class="uael-slide-block "><span class="uael-slide_text">Fitur</span>
											</span>
																													</span>
							</span>
												</span>
									</h3>					</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-5ae92f2 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="5ae92f2" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">PLATINUM</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-b2d9933 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="b2d9933" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-a063b86 wdp-sticky-section-no" data-id="a063b86" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-0fff736 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="0fff736" data-element_type="section" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:49}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-8ae5f52 wdp-sticky-section-no" data-id="8ae5f52" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4ce50f9 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4ce50f9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Disc</i> 50%</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c5fac60 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="c5fac60" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-44facb9 elementor-headline--style-highlight wdp-sticky-section-no elementor-widget elementor-widget-animated-headline" data-id="44facb9" data-element_type="widget" data-settings="{&quot;marker&quot;:&quot;strikethrough&quot;,&quot;highlighted_text&quot;:&quot;IDR 300.000&quot;,&quot;highlight_animation_duration&quot;:1000,&quot;highlight_iteration_delay&quot;:2000,&quot;headline_style&quot;:&quot;highlight&quot;,&quot;loop&quot;:&quot;yes&quot;}" data-widget_type="animated-headline.default">
				<div class="elementor-widget-container">
					<h3 class="elementor-headline">
				<span class="elementor-headline-dynamic-wrapper elementor-headline-text-wrapper">
					<span class="elementor-headline-dynamic-text elementor-headline-text-active">IDR 300.000</span>
				</span>
				</h3>
				</div>
				</div>
				<div class="elementor-element elementor-element-922d4e2 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="922d4e2" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-dd52fe1 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="dd52fe1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">IDR 150.000</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ac4d75c elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="ac4d75c" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9ec6e5a animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9ec6e5a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Special Promo</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-bd9369a animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="bd9369a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>MAY</i>rriage</i> Month</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-ed3287d wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="ed3287d" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-570febe elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="570febe" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Hai%20kak%2C%20saya%20mau%20order%20Undangan%20Tema%20Platinum%20nih%2C%20Bisa%20dibantu%3F" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Pesan Sekarang</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-a855c4f wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="a855c4f" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-db1c8e5 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="db1c8e5" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-0cd6fdf wdp-sticky-section-no" data-id="0cd6fdf" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c7642d5 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c7642d5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Estimasi</i><br>Pengerjaan</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-7f8d496 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7f8d496" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">5 hari</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-30b18f1 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="30b18f1" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-071d874 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="071d874" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">*Disclaimer</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-b76cc44 wdp-sticky-section-no elementor-widget elementor-widget-text-editor" data-id="b76cc44" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
							<ol><li>Untuk tampilan terbaik, silahkan akses link melalui browser Chrome dan Safari.</li><li>Pastikan menggunakan browser dengan versi yang terbaru.</li><li>Undangan tidak support force Darkmode dari gadget/browser. Karena akan berpengaruh pada tampilan Undangan.</li></ol>						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-e8c2286 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="e8c2286" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-a0ca625 wdp-sticky-section-no" data-id="a0ca625" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c55670e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c55670e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Preview Product</h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-68beabc elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="68beabc" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d0ed407 wdp-sticky-section-no" data-id="d0ed407" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-e2d81de elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="e2d81de" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-46a9c8e wdp-sticky-section-no" data-id="46a9c8e" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-01d1669 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="01d1669" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Platinum</i> 1</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-d796c3e wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="d796c3e" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="480" height="720" src="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1.png 480w, https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-1-200x300.png 200w" sizes="(max-width: 480px) 100vw, 480px" />															</div>
				</div>
				<div class="elementor-element elementor-element-20bae2b elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="20bae2b" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum1/preview/?to=Nama+Tamu" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Preview</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-bcb85a1 wdp-sticky-section-no" data-id="bcb85a1" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-1ce2602 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="1ce2602" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-c1fd66c wdp-sticky-section-no" data-id="c1fd66c" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-da107db wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="da107db" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Platinum</i> 2</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-3031057 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3031057" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="480" height="720" src="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-2.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-2.png 480w, https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-2-200x300.png 200w" sizes="(max-width: 480px) 100vw, 480px" />															</div>
				</div>
				<div class="elementor-element elementor-element-f2f9004 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="f2f9004" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum2/preview/?to=Nama+Tamu" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Preview</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-83088aa elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="83088aa" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-1c6d9f2 wdp-sticky-section-no" data-id="1c6d9f2" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-440aa9b elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="440aa9b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a1c0e1f wdp-sticky-section-no" data-id="a1c0e1f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c2dd4c4 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c2dd4c4" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Platinum</i> 3</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-64caa62 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="64caa62" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="480" height="720" src="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-3.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-3.png 480w, https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-3-200x300.png 200w" sizes="(max-width: 480px) 100vw, 480px" />															</div>
				</div>
				<div class="elementor-element elementor-element-4e96666 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="4e96666" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum3/preview/?to=Nama+Tamu" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Preview</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-54d8e71 wdp-sticky-section-no" data-id="54d8e71" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-6c1fe3c elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6c1fe3c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-e2f97db wdp-sticky-section-no" data-id="e2f97db" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-cc9fe28 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="cc9fe28" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Platinum</i> 4</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c2b4437 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="c2b4437" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="480" height="720" src="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-4.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-4.png 480w, https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-4-200x300.png 200w" sizes="(max-width: 480px) 100vw, 480px" />															</div>
				</div>
				<div class="elementor-element elementor-element-61b44ed elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="61b44ed" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum4/preview/?to=Nama+Tamu" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Preview</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-d29c3ab elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d29c3ab" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-8eaf85d wdp-sticky-section-no" data-id="8eaf85d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-df37090 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="df37090" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-db59071 wdp-sticky-section-no" data-id="db59071" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-9eeff7a wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9eeff7a" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><i>Platinum</i> 5</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-14d458d wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="14d458d" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="480" height="720" src="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-5.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-5.png 480w, https://unityinvitation.com/wp-content/uploads/2022/04/Mockup-Nu-Platinum-5-200x300.png 200w" sizes="(max-width: 480px) 100vw, 480px" />															</div>
				</div>
				<div class="elementor-element elementor-element-ed650b5 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="ed650b5" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum5/preview/?to=Nama+Tamu" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Preview</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-bf33e1e wdp-sticky-section-no" data-id="bf33e1e" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-80efbb7 wdp-sticky-section-no" data-id="80efbb7" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-2e438a1 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2e438a1" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-440fe0d wdp-sticky-section-no" data-id="440fe0d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-947cd25 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="947cd25" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">*Fitur</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-eb12654 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="eb12654" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-7edbee2 wdp-sticky-section-no elementor-widget elementor-widget-eael-feature-list" data-id="7edbee2" data-element_type="widget" data-widget_type="eael-feature-list.default">
				<div class="elementor-widget-container">
					<div class="-icon-position-left -tablet-icon-position-left -mobile-icon-position-left">
			<ul id="eael-feature-list-7edbee2" class="eael-feature-list-items circle stacked ">
			                <li class="eael-feature-list-item elementor-repeater-item-639e2e9">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-0">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Nama URL Undangan</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-2a153d8">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-1">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Unlimited Custom Nama Tamu Undangan</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-dbf559b">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-2">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Cover Pembuka</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-0e17b07">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-3">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Data Profil Mempelai</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-43c03c9">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-4">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Countdown Timer</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-bfd89fc">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-5">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Navigasi Lokasi Pernikahan (Google Maps)</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-b88f4fc">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-6">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Love Story</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-9075c3b">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-7">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Galeri Foto (15 foto)</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-e68b9a6">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-8">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Video Prewed (1 Video)</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-dc9f4a2">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-9">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Backsound Musik</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-b390075">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-10">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Live Streaming (Zoom/ Google Meet/ Youtube Live/ Instagram Live)</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-51475c1">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-11">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Amplop Digital / Online Gift</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-4c653d5">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-12">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Panduan Protokol Kesehatan</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-134bd14">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-13">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Ucapan Tamu Undangan  &amp; Konfirmasi Kehadiran</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
				                <li class="eael-feature-list-item elementor-repeater-item-e6d2912">
                    
						<div class="eael-feature-list-icon-box">
							<div class="eael-feature-list-icon-inner">

								<span class="eael-feature-list-icon fl-icon-14">

		<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
							</div>
						</div>
						<div class="eael-feature-list-content-box">
							<h3 class="eael-feature-list-title"							>Masa Aktif 8 Bulan</h3						>
						<p class="eael-feature-list-content"></p>
						</div>

					</li>
							</ul>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9c1fab2 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="9c1fab2" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-f82a91e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f82a91e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">*Fitur Tambahan</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-9731c8d elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="9731c8d" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-e1bc56e wdp-sticky-section-no elementor-widget elementor-widget-pp-price-menu" data-id="e1bc56e" data-element_type="widget" data-widget_type="pp-price-menu.default">
				<div class="elementor-widget-container">
					<div class="pp-restaurant-menu pp-restaurant-menu-style-1">
			<div class="pp-restaurant-menu-items">
														<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Revisi Minor												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													15K												</span>
											</span>
																											</div>

								
								
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Revisi Mayor												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													25k												</span>
											</span>
																											</div>

								
								
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Express 1 hari jadi												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													50k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Langsung dikerjakan setelah data lengkap									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Konfirmasi Kehadiran												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													20k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Menggunakan Google Sheets									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Pengingat Acara												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													15k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Menggunakan Google Calendar									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Tambah foto Galeri												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													25k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										 (Max 10 foto)									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Tambah foto Love Story												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													25k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Menambahkan foto pada tabel story									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Custom Font												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													15k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Request Font yang telah kami Display									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Custom Warna												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													25k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										khusus tema Gold, Platinum, Diamond, Adat									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Request Musik/ Backsong												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													15k												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Musik sesuai keinginan.									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Custom Domain												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													(Coming Soon)												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Link eksklusif nama mempelai<br>
ex: www.fulanfulanah.com									</div>
									
															</div>
						</div>
					</div>
															<div class="pp-restaurant-menu-item-wrap">
						<div class="pp-restaurant-menu-item">
															<div class="pp-restaurant-menu-image">
																	</div>
							
							<div class="pp-restaurant-menu-content">
								<div class="pp-restaurant-menu-header">
																			<h4 class="pp-restaurant-menu-title">
																							<span class="pp-restaurant-menu-title-text">
													Fitur buku Tamu												</span>
																						</h4>
																				<span class="pp-price-title-connector"></span>
																															<span class="pp-restaurant-menu-price">
																								<span class="pp-restaurant-menu-price-discount">
													(Coming Soon)												</span>
											</span>
																											</div>

								
																	<div class="pp-restaurant-menu-description">
										Fitur buku tamu &#038; QR Code <br> Kehadiran 									</div>
									
															</div>
						</div>
					</div>
								</div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-35875c4 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="35875c4" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-73715f5 wdp-sticky-section-no" data-id="73715f5" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-bbc8b8d wdp-sticky-section-no" data-id="bbc8b8d" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-8e4f59f wdp-sticky-section-no" data-id="8e4f59f" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-9941075 wdp-sticky-section-no" data-id="9941075" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-a735056 elementor-view-stacked elementor-fixed elementor-shape-circle wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="a735056" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}},&quot;sticky&quot;:&quot;bottom&quot;,&quot;_position&quot;:&quot;fixed&quot;,&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon" href="#upside">
			<i aria-hidden="true" class="fas fa-chevron-up"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
				<div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami.
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
				<div class="elementor-widget-container">
					<div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
			<div class="jet-tabs__control-wrapper">
				<div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png" alt=""></div></div><div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt=""></div></div><div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-Mandiri.png" alt=""></div></div>			</div>
			<div class="jet-tabs__content-wrapper">
				<div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div>			</div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Bimo Bramantyo</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
				<div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-no">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
				<div class="elementor-widget-container">
			
				<div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
								<nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}"><ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul></nav>
															<div class="pp-menu-toggle pp-menu-toggle-on-tablet">
											<div class="pp-hamburger">
							<div class="pp-hamburger-box">
																	<div class="pp-hamburger-inner"></div>
															</div>
						</div>
														</div>
												<nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
												<ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul>							</nav>
							</div>
						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		<link rel='stylesheet' id='elementor-post-51208-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51208.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='e-animations-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
<script type='text/javascript' id='WEDKU_SCRP-js-extra'>
/* <![CDATA[ */
var ceper = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.16' id='WEDKU_SCRP-js'></script>
<script type='text/javascript' id='wdp_js_script-js-extra'>
/* <![CDATA[ */
var WDP_WP = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","wdpNonce":"4bda080e1a","jpages":"true","jPagesNum":"5","textCounter":"true","textCounterNum":"500","widthWrap":"","autoLoad":"true","thanksComment":"Terima kasih atas ucapan & doanya!","thanksReplyComment":"Terima kasih atas balasannya!","duplicateComment":"You might have left one of the fields blank, or duplicate comments","insertImage":"Insert image","insertVideo":"Insert video","insertLink":"Insert link","checkVideo":"Check video","accept":"Accept","cancel":"Cancel","reply":"Balas","textWriteComment":"Tulis Ucapan & Doa","classPopularComment":"wdp-popular-comment","textUrlImage":"Url image","textUrlVideo":"Url video youtube or vimeo","textUrlLink":"Url link","textToDisplay":"Text to display","textCharacteresMin":"Minimal 2 karakter","textNavNext":"Selanjutnya","textNavPrev":"Sebelumnya","textMsgDeleteComment":"Do you want delete this comment?","textLoadMore":"Load more","textLikes":"Likes"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
<script type='text/javascript' id='fb032dce5-js-extra'>
/* <![CDATA[ */
var localize = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"667cbfe6aa","i18n":{"added":"Added ","compare":"Compare","loading":"Loading..."},"page_permalink":"https:\/\/unityinvitation.com\/platinum-detail\/"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/uploads/essential-addons-elementor/fb032dce5.min.js?ver=1652083543' id='fb032dce5-js'></script>
<script type='text/javascript' id='uael-frontend-script-js-extra'>
/* <![CDATA[ */
var uael_script = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-js/uael-frontend.min.js?ver=1.36.5' id='uael-frontend-script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/lib/typed/typed.min.js?ver=1.36.5' id='uael-fancytext-typed-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/lib/rvticker/rvticker.min.js?ver=1.36.5' id='uael-fancytext-slidev-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
<script type='text/javascript' id='powerpack-frontend-js-extra'>
/* <![CDATA[ */
var ppLogin = {"empty_username":"Enter a username or email address.","empty_password":"Enter password.","empty_password_1":"Enter a password.","empty_password_2":"Re-enter password.","empty_recaptcha":"Please check the captcha to verify you are not a robot.","email_sent":"A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.","reset_success":"Your password has been reset successfully.","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
var ppRegistration = {"invalid_username":"This username is invalid because it uses illegal characters. Please enter a valid username.","username_exists":"This username is already registered. Please choose another one.","empty_email":"Please type your email address.","invalid_email":"The email address isn\u2019t correct!","email_exists":"The email is already registered, please choose another one.","password":"Password must not contain the character \"\\\\\"","password_length":"Your password should be at least 8 characters long.","password_mismatch":"Password does not match.","invalid_url":"URL seems to be invalid.","recaptcha_php_ver":"reCAPTCHA API requires PHP version 5.3 or above.","recaptcha_missing_key":"Your reCAPTCHA Site or Secret Key is missing!","show_password":"Show password","hide_password":"Hide password","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
<script type='text/javascript' id='bdt-uikit-js-extra'>
/* <![CDATA[ */
var element_pack_ajax_login_config = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","language":"en","loadingmessage":"Sending user info, please wait...","unknownerror":"Unknown error, make sure access is correct!"};
var ElementPackConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"268d485f92","data_table":{"language":{"lengthMenu":"Show _MENU_ Entries","info":"Showing _START_ to _END_ of _TOTAL_ entries","search":"Search :","paginate":{"previous":"Previous","next":"Next"}}},"contact_form":{"sending_msg":"Sending message please wait...","captcha_nd":"Invisible captcha not defined!","captcha_nr":"Could not get invisible captcha response!"},"mailchimp":{"subscribing":"Subscribing you please wait..."},"elements_data":{"sections":[],"columns":[],"widgets":[]}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.6.4","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"theme_builder_v2":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true,"form-submissions":true},"urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"viewport_tablet":1024,"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes"},"post":{"id":61892,"title":"Detail%20Platinum","excerpt":"","featuredImage":"https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/05\/Unity-Icon1-1.png"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"f02c3dc1b5","urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/","rest":"https:\/\/unityinvitation.com\/wp-json\/"},"i18n":{"toc_no_headings_found":"No headings were found on this page."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
<script type='text/javascript' id='jet-elements-js-extra'>
/* <![CDATA[ */
var jetElements = {"ajaxUrl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template","devMode":"false","messages":{"invalidMail":"Please specify a valid e-mail"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
<script type='text/javascript' id='jet-tabs-frontend-js-extra'>
/* <![CDATA[ */
var JetTabsSettings = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template","devMode":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
<script type='text/javascript' id='weddingpress-wdp-js-extra'>
/* <![CDATA[ */
var cevar = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","plugin_url":"https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
<div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div><div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>	</body>
</html>
