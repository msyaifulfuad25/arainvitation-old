<?php
	include "data-sap.php";
	include "data-client.php";

    $url_save_sap = $base_url.$uri_couple.'/save-sap.php';
    $url_get_sap = $base_url.$uri_couple.'/get-sap.php';
	// Untuk Development Product
	$asset_theme = $base_url.'product/platinum-detail/platinum1/preview/assets/';
	// Untuk Production
	// $asset_theme = $base_url.'satria-erika/assets/';
	$theme_name = 'Ara Platinum 1';
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      .wdp-comment-text img {
        max-width: 100% !important;
      }
    </style>
    <!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
    <title><?= $theme_name ?></title>
    <meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta property="og:url" content="<?= $asset ?>platinum-1/" />
    <meta property="og:site_name" content="<?= $title ?>" />
    <meta property="article:section" content="Undangan" />
    <meta property="og:updated_time" content="2022-06-09T18:09:49+07:00" />
    <meta property="og:image" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-1-cover-invitation.jpg" />
    <meta property="og:image:secure_url" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-1-cover-invitation.jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />
    <meta property="og:image:alt" content="<?= $title ?>" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?= $title ?>" />
    <meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="twitter:image" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-1-cover-invitation.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="admin" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="2 minutes" />
    <script type="application/ld+json" class="rank-math-schema">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": "BreadcrumbList",
          "@id": "<?= $asset ?>platinum-1/#breadcrumb",
          "itemListElement": [{
            "@type": "ListItem",
            "position": "1",
            "item": {
              "@id": "<?= $base_url ?>",
              "name": "Home"
            }
          }, {
            "@type": "ListItem",
            "position": "2",
            "item": {
              "@id": "<?= $asset ?>platinum-1/",
              <!-- "name": "Unity Platinum 1" -->
            }
          }]
        }]
      }
    </script>
    <!-- /Rank Math WordPress SEO plugin -->
    <link rel='dns-prefetch' href='//use.fontawesome.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="<?= $title ?> &raquo; Feed" href="<?= $asset ?>feed/" />
    <link rel="alternate" type="application/rss+xml" title="<?= $title ?> &raquo; Comments Feed" href="<?= $asset ?>comments/feed/" />
    <link rel="alternate" type="application/rss+xml" title="<?= $title ?> &raquo; <?= $title ?> Comments Feed" href="<?= $asset ?>platinum-1/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
        "svgExt": ".svg",
        "source": {
        //   "concatemoji": "<?= $base_url ?>wp-includes\/js\/wp-emoji-release.min.js?ver=6.0"
        }
      };
      /*! This file is auto-generated */
      ! function(e, a, t) {
        var n, r, o, i = a.createElement("canvas"),
          p = i.getContext && i.getContext("2d");

        function s(e, t) {
          var a = String.fromCharCode,
            e = (p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0), i.toDataURL());
          return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
        }

        function c(e) {
          var t = a.createElement("script");
          t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (o = Array("flag", "emoji"), t.supports = {
            everything: !0,
            everythingExceptFlag: !0
          }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
          if (!p || !p.fillText) return !1;
          switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
            case "flag":
              return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
            case "emoji":
              return !s([129777, 127995, 8205, 129778, 127999], [129777, 127995, 8203, 129778, 127999])
          }
          return !1
        }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
          t.DOMReady = !0
        }, t.supports.everything || (n = function() {
          t.readyCallback()
        }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
          "complete" === a.readyState && t.readyCallback()
        })), (e = t.source || {}).concatemoji ? c(e.concatemoji) : e.wpemoji && e.twemoji && (c(e.twemoji), c(e.wpemoji)))
      }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='bdt-uikit-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ep-helper-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css' href='<?= $asset ?>wp-includes/css/dist/block-library/style.min.css?ver=6.0' type='text/css' media='all' />
    <style id='global-styles-inline-css' type='text/css'>
      body {
        --wp--preset--color--black: #000000;
        --wp--preset--color--cyan-bluish-gray: #abb8c3;
        --wp--preset--color--white: #ffffff;
        --wp--preset--color--pale-pink: #f78da7;
        --wp--preset--color--vivid-red: #cf2e2e;
        --wp--preset--color--luminous-vivid-orange: #ff6900;
        --wp--preset--color--luminous-vivid-amber: #fcb900;
        --wp--preset--color--light-green-cyan: #7bdcb5;
        --wp--preset--color--vivid-green-cyan: #00d084;
        --wp--preset--color--pale-cyan-blue: #8ed1fc;
        --wp--preset--color--vivid-cyan-blue: #0693e3;
        --wp--preset--color--vivid-purple: #9b51e0;
        --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
        --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
        --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
        --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
        --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
        --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
        --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
        --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
        --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
        --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
        --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
        --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
        --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
        --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
        --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
        --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
        --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
        --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
        --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
        --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
        --wp--preset--font-size--small: 13px;
        --wp--preset--font-size--medium: 20px;
        --wp--preset--font-size--large: 36px;
        --wp--preset--font-size--x-large: 42px;
      }

      .has-black-color {
        color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-color {
        color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-color {
        color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-color {
        color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-color {
        color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-color {
        color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-color {
        color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-color {
        color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-color {
        color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-color {
        color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-color {
        color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-color {
        color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-background-color {
        background-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-background-color {
        background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-background-color {
        background-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-background-color {
        background-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-background-color {
        background-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-background-color {
        background-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-background-color {
        background-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-background-color {
        background-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-background-color {
        background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-background-color {
        background-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-border-color {
        border-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-border-color {
        border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-border-color {
        border-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-border-color {
        border-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-border-color {
        border-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-border-color {
        border-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-border-color {
        border-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-border-color {
        border-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-border-color {
        border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-border-color {
        border-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
        background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
      }

      .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
        background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
      }

      .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-orange-to-vivid-red-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
      }

      .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
        background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
      }

      .has-cool-to-warm-spectrum-gradient-background {
        background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
      }

      .has-blush-light-purple-gradient-background {
        background: var(--wp--preset--gradient--blush-light-purple) !important;
      }

      .has-blush-bordeaux-gradient-background {
        background: var(--wp--preset--gradient--blush-bordeaux) !important;
      }

      .has-luminous-dusk-gradient-background {
        background: var(--wp--preset--gradient--luminous-dusk) !important;
      }

      .has-pale-ocean-gradient-background {
        background: var(--wp--preset--gradient--pale-ocean) !important;
      }

      .has-electric-grass-gradient-background {
        background: var(--wp--preset--gradient--electric-grass) !important;
      }

      .has-midnight-gradient-background {
        background: var(--wp--preset--gradient--midnight) !important;
      }

      .has-small-font-size {
        font-size: var(--wp--preset--font-size--small) !important;
      }

      .has-medium-font-size {
        font-size: var(--wp--preset--font-size--medium) !important;
      }

      .has-large-font-size {
        font-size: var(--wp--preset--font-size--large) !important;
      }

      .has-x-large-font-size {
        font-size: var(--wp--preset--font-size--x-large) !important;
      }
    </style>
    <link rel='stylesheet' id='WEDKU_STYLE-css' href='<?= $asset ?>wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.18' type='text/css' media='all' />
    <link rel='stylesheet' id='pafe-extension-style-css' href='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp_style-css' href='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
    <style id='wdp_style-inline-css' type='text/css'>
      .wdp-wrapper {
        font-size: 14px
      }

      .wdp-wrapper {
        background: #ffffff;
      }

      .wdp-wrapper.wdp-border {
        border: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-comments a:link,
      .wdp-wrapper .wdp-wrap-comments a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {
        border-color: #64B6EC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        color: #FFFFFF;
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        background: #a3a3a3;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {
        color: #44525F;
      }

      .wdp-wrapper .wdp-media-btns a>span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-media-btns a>span:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        color: #319342;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {
        color: #C85951;
      }

      .wdp-wrapper .wdp-comment-status.wdp-loading>span {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-bottom: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #54595f !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #2a5782 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {
        color: #2a5782;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span:hover {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {
        color: #2C9E48;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {
        color: #D13D3D;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-counter-info {
        color: #9DA8B7;
      }

      .wdp-wrapper .wdp-holder span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a,
      .wdp-wrapper .wdp-holder a:link,
      .wdp-wrapper .wdp-holder a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a:hover,
      .wdp-wrapper .wdp-holder a:link:hover,
      .wdp-wrapper .wdp-holder a:visited:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {
        max-width: 28px;
        max-height: 28px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {
        margin-left: 38px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {
        max-width: 24px;
        max-height: 24px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {
        max-width: 21px;
        max-height: 21px;
      }
    </style>
    <link rel='stylesheet' id='wdp-centered-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-horizontal-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-fontello-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='exad-main-style-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-css' href='<?= $asset ?>wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-theme-style-css' href='<?= $asset ?>wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-css' href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-skin-css' href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1399-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-1399.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='powerpack-frontend-css' href='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='weddingpress-wdp-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css' href='<?= $asset ?>wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='uael-frontend-css' href='<?= $asset ?>wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-tabs-frontend-css' href='<?= $asset ?>wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
    <!-- <link rel='stylesheet' id='elementor-post-26593-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-26593.css?ver=1655076585' type='text/css' media='all' /> -->
	<style>
		.elementor-26593
		.elementor-element.elementor-element-12e74c9b:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-12e74c9b
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("<?= $asset_theme ?>photos/Cover3.jpg");
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-26593
		.elementor-element.elementor-element-12e74c9b
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(180deg, #1e1e1e94 0%, #1e1e1e 70%);
		opacity: 0.86;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-12e74c9b {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 40px 0px;
		z-index: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-54f0e56b
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 350px 30px 150px 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-54f0e56b
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-55c8fcd1 {
		text-align: center;
		z-index: 2;
		}
		.elementor-26593
		.elementor-element.elementor-element-55c8fcd1
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 28px;
		font-weight: 100;
		letter-spacing: 9px;
		}
		.elementor-26593 .elementor-element.elementor-element-18f2eb8e {
		text-align: center;
		z-index: 2;
		}
		.elementor-26593
		.elementor-element.elementor-element-18f2eb8e
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Soligant", Sans-serif;
		font-size: 82px;
		font-weight: normal;
		line-height: 0.8em;
		}
		.elementor-26593
		.elementor-element.elementor-element-18f2eb8e
		> .elementor-widget-container {
		margin: 10px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7bbf8daa {
		--divider-border-style: solid;
		--divider-color: #ffffff36;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7bbf8daa
		.elementor-divider-separator {
		width: 35%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-7bbf8daa
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-3aad3ac8 {
		text-align: center;
		z-index: 2;
		}
		.elementor-26593
		.elementor-element.elementor-element-3aad3ac8
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Soligant", Sans-serif;
		font-size: 40px;
		font-weight: 100;
		letter-spacing: 3px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3aad3ac8
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff57;
		}
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec.elementor-view-default
		.elementor-icon {
		fill: #ffffff57;
		color: #ffffff57;
		border-color: #ffffff57;
		}
		.elementor-26593 .elementor-element.elementor-element-7ea4e7ec {
		--icon-box-icon-margin: 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7ea4e7ec .elementor-icon {
		font-size: 25px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec
		.elementor-icon-box-title {
		color: #ffffff57;
		}
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec
		.elementor-icon-box-title,
		.elementor-26593
		.elementor-element.elementor-element-7ea4e7ec
		.elementor-icon-box-title
		a {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 400;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-83e0e69
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-83e0e69
		> .elementor-shape-bottom
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-40b529
		.elementor-spacer-inner {
		--spacer-size: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7b4400cf:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-7b4400cf
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff;
		background-image: url("<?= $asset_theme ?>backgrounds/bg1.jpg");
		background-position: center center;
		background-size: 50% auto;
		}
		.elementor-26593
		.elementor-element.elementor-element-7b4400cf
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: radial-gradient(
			at center center,
			#ffffffd1 32%,
			var(--e-global-color-secondary) 100%
		);
		opacity: 1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-7b4400cf {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 100px 0px 200px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6716efc4 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6716efc4
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 48px;
		font-weight: 500;
		}
		.elementor-26593 .elementor-element.elementor-element-50a4b2c7 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-50a4b2c7
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 84px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-50a4b2c7
		> .elementor-widget-container {
		margin: -20px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2f930b55 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-2f930b55
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2f930b55
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-50a70519
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6c5f118e
		> .elementor-container {
		max-width: 1500px;
		}
		.elementor-26593 .elementor-element.elementor-element-6c5f118e {
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-a3b8117.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-a3b8117.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-a3b8117
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a3b8117
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-312f9a8b {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-312f9a8b
		.elementor-heading-title {
		color: <?= $color_3 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		line-height: 1.2em;
		}
		.elementor-26593 .elementor-element.elementor-element-6bd6ab1a {
		--divider-border-style: solid;
		--divider-color: <?= $color_1 ?>4a;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6bd6ab1a
		.elementor-divider-separator {
		width: 35%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-6bd6ab1a
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-4f671a99 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-4f671a99
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 14px;
		font-weight: bold;
		}
		.elementor-26593
		.elementor-element.elementor-element-4f671a99
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-19d66ff6 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-19d66ff6
		.elementor-heading-title {
		color: <?= $color_4 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-19d66ff6
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-47514af6 {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 30px;
		--grid-column-gap: 15px;
		--grid-row-gap: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-47514af6
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-47514af6
		.elementor-social-icon {
		background-color: #02010100;
		}
		.elementor-26593
		.elementor-element.elementor-element-47514af6
		.elementor-social-icon
		i {
		color: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-47514af6
		.elementor-social-icon
		svg {
		fill: <?= $color_1 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-47514af6 .elementor-icon {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-6e1a4a71.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e1a4a71.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e1a4a71.elementor-column
		> .elementor-widget-wrap {
		justify-content: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e1a4a71
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593 .elementor-element.elementor-element-bc781e8 {
		text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-bc781e8 img {
		border-radius: 500px 500px 0px 0px;
		box-shadow: 10px 10px 15px -7px rgba(64, 47.99999999999997, 12, 0.49);
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-74479eb1.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-74479eb1.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-74479eb1.elementor-column
		> .elementor-widget-wrap {
		justify-content: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-74479eb1
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-74479eb1
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-4c123b15 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-4c123b15
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-4c123b15
		> .elementor-widget-container {
		margin: 20px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7f06c0be {
		text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-7f06c0be img {
		border-radius: 500px 500px 0px 0px;
		box-shadow: -10px 10px 15px -7px rgba(64, 47.99999999999997, 12, 0.49);
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-221c3ce7.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-221c3ce7.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-221c3ce7
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-221c3ce7
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-75355ed8 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-75355ed8
		.elementor-heading-title {
		color: <?= $color_3 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		line-height: 1.2em;
		}
		.elementor-26593 .elementor-element.elementor-element-43167651 {
		--divider-border-style: solid;
		--divider-color: <?= $color_1 ?>4a;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-43167651
		.elementor-divider-separator {
		width: 35%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-43167651
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-70115f1f {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-70115f1f
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 14px;
		font-weight: bold;
		}
		.elementor-26593
		.elementor-element.elementor-element-70115f1f
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-319fa485 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-319fa485
		.elementor-heading-title {
		color: <?= $color_4 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-319fa485
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-12f38211 {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 30px;
		--grid-column-gap: 15px;
		--grid-row-gap: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-12f38211
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-12f38211
		.elementor-social-icon {
		background-color: #02010100;
		}
		.elementor-26593
		.elementor-element.elementor-element-12f38211
		.elementor-social-icon
		i {
		color: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-12f38211
		.elementor-social-icon
		svg {
		fill: <?= $color_1 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-12f38211 .elementor-icon {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("<?= $asset_theme ?>photos/Galeri-15.jpg");
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-background-overlay {
		background-color: #3b3433;
		opacity: 0.95;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-310afb79 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 0px 200px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-shape-top
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-310afb79
		> .elementor-shape-bottom
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593 .elementor-element.elementor-element-34e30737 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-34e30737
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-34e30737
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-4636262a {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-4636262a
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Soligant", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-4636262a
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8.bdt-countdown--label-block
		.bdt-countdown-number {
		margin-bottom: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8.bdt-countdown--label-inline
		.bdt-countdown-number {
		margin-right: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8
		.bdt-countdown-wrapper {
		max-width: 50%;
		margin-left: auto;
		margin-right: auto;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8
		.bdt-countdown-item {
		backdrop-filter: blur(15px);
		-webkit-backdrop-filter: blur(15px);
		background-color: #ffffff26;
		border-style: solid;
		border-width: 2px 0px 0px 2px;
		border-color: #ffffff14;
		border-radius: 30px 30px 30px 30px;
		padding: 25px 25px 25px 25px;
		box-shadow: 5px 5px 20px -5px rgba(0, 0, 0, 0.31);
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8
		.bdt-countdown-number {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 42px;
		font-weight: 100;
		line-height: 1em;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8
		.bdt-countdown-label {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: 100;
		line-height: 1em;
		}
		.elementor-26593
		.elementor-element.elementor-element-5329bec8
		> .elementor-widget-container {
		margin: 30px 0px 30px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-580eaa66 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-580eaa66
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-580eaa66
		> .elementor-widget-container {
		margin: 0px 200px 0px 200px;
		}
		.elementor-26593 .elementor-element.elementor-element-43cea9c0 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-43cea9c0
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: bold;
		}
		.elementor-26593
		.elementor-element.elementor-element-43cea9c0
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-f1b747a:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-f1b747a
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff;
		background-image: url("<?= $asset_theme ?>backgrounds/bg1.jpg");
		background-position: center center;
		background-size: 50% auto;
		}
		.elementor-26593
		.elementor-element.elementor-element-f1b747a
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(
			180deg,
			var(--e-global-color-secondary) 0%,
			#ffffffe0 17%
		);
		opacity: 1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-f1b747a {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 150px 0px 250px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-f1b747a
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-f1b747a
		> .elementor-shape-bottom
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-14204cba
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-14204cba
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-14204cba
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-14204cba
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-14204cba
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-5b48a037 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-5b48a037
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 600;
		}
		.elementor-26593 .elementor-element.elementor-element-26929aec {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-26929aec
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-26929aec
		> .elementor-widget-container {
		margin: -20px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-4fb69753 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-4fb69753
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-4fb69753
		> .elementor-widget-container {
		margin: 0px 300px 0px 300px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-681780f8
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-54115b94
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-26593 .elementor-element.elementor-element-54115b94 {
		padding: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-1f92964b.elementor-column
		.elementor-widget-wrap {
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f92964b.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: flex-start;
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 3px 0px 0px;
		border-color: #ab9d9c4f;
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 50px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f92964b
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-27710c7b
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-27710c7b.elementor-view-stacked
		.elementor-icon {
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-27710c7b.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-27710c7b.elementor-view-default
		.elementor-icon {
		color: <?= $color_2 ?>;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-27710c7b.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-27710c7b.elementor-view-default
		.elementor-icon
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-27710c7b .elementor-icon {
		font-size: 100px;
		}
		.elementor-26593
		.elementor-element.elementor-element-27710c7b
		.elementor-icon
		i,
		.elementor-26593
		.elementor-element.elementor-element-27710c7b
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593
		.elementor-element.elementor-element-143de12e
		.elementor-spacer-inner {
		--spacer-size: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-5beb6017 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-5beb6017
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 48px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5beb6017
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-30681bb1 {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>5c;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-30681bb1
		.elementor-divider-separator {
		width: 75%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-30681bb1
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-16a3b19e {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-16a3b19e
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		}
		.elementor-26593
		.elementor-element.elementor-element-16a3b19e
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-39ffc1b8
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-26593 .elementor-element.elementor-element-39ffc1b8 {
		margin-top: 0px;
		margin-bottom: 20px;
		padding: 0px 75px 0px 75px;
		}
		.elementor-26593
		.elementor-element.elementor-element-22f55591
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7b7be058 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-7b7be058
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7b7be058
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-27217f0
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-27217f0
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-75cc5e4c
		.elementor-counter-number-wrapper {
		color: <?= $color_2 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 84px;
		font-weight: 100;
		}
		.elementor-26593
		.elementor-element.elementor-element-34424cfc
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-180d2b7f {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-180d2b7f
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-180d2b7f
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-10782627 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-10782627
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		}
		.elementor-26593
		.elementor-element.elementor-element-10782627
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-44595f0 {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>5c;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-44595f0
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-44595f0
		.elementor-divider {
		text-align: center;
		padding-top: 50px;
		padding-bottom: 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36.elementor-view-stacked
		.elementor-icon {
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36.elementor-view-default
		.elementor-icon {
		color: <?= $color_2 ?>;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36.elementor-view-default
		.elementor-icon
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-6e3ee36 .elementor-icon {
		font-size: 30px;
		}
		.elementor-26593 .elementor-element.elementor-element-6e3ee36 .elementor-icon i,
		.elementor-26593
		.elementor-element.elementor-element-6e3ee36
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593 .elementor-element.elementor-element-551fb491 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-551fb491
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		line-height: 1.3em;
		}
		.elementor-26593
		.elementor-element.elementor-element-551fb491
		> .elementor-widget-container {
		margin: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-565b7de1 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-565b7de1
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-26593
		.elementor-element.elementor-element-565b7de1
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-weight: normal;
		line-height: 1.3em;
		letter-spacing: 1px;
		background-color: <?= $color_2 ?>;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 15px -5px rgba(1.4912280701754448, 1.4912280701754448, 1.4912280701754448, 0.33);
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #127c9400;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-93fd04a
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-93fd04a {
		z-index: 2;
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7.elementor-column
		.elementor-widget-wrap {
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: flex-start;
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 0px 0px 3px;
		border-color: #ab9d9c4f;
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 50px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5037a4a7
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb.elementor-view-stacked
		.elementor-icon {
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb.elementor-view-default
		.elementor-icon {
		color: <?= $color_2 ?>;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb.elementor-view-default
		.elementor-icon
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-6efc2ffb .elementor-icon {
		font-size: 100px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb
		.elementor-icon
		i,
		.elementor-26593
		.elementor-element.elementor-element-6efc2ffb
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593
		.elementor-element.elementor-element-260cbace
		.elementor-spacer-inner {
		--spacer-size: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-2e037502 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-2e037502
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 48px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2e037502
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7fa0580d {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>5c;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7fa0580d
		.elementor-divider-separator {
		width: 75%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-7fa0580d
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-75429b1f {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-75429b1f
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		}
		.elementor-26593
		.elementor-element.elementor-element-75429b1f
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-33f517de
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-26593 .elementor-element.elementor-element-33f517de {
		margin-top: 0px;
		margin-bottom: 20px;
		padding: 0px 75px 0px 75px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3853853
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-d0ed149 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-d0ed149
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-d0ed149
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-12bd4b3a
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-12bd4b3a
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1272b3fa
		.elementor-counter-number-wrapper {
		color: <?= $color_2 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 84px;
		font-weight: 100;
		}
		.elementor-26593
		.elementor-element.elementor-element-22326808
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-adef9b7 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-adef9b7
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-adef9b7
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-353be9d9 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-353be9d9
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		}
		.elementor-26593
		.elementor-element.elementor-element-353be9d9
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2628856c {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>5c;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2628856c
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-2628856c
		.elementor-divider {
		text-align: center;
		padding-top: 50px;
		padding-bottom: 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f532b6
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f532b6.elementor-view-stacked
		.elementor-icon {
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f532b6.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6f532b6.elementor-view-default
		.elementor-icon {
		color: <?= $color_2 ?>;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f532b6.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-6f532b6.elementor-view-default
		.elementor-icon
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-6f532b6 .elementor-icon {
		font-size: 30px;
		}
		.elementor-26593 .elementor-element.elementor-element-6f532b6 .elementor-icon i,
		.elementor-26593
		.elementor-element.elementor-element-6f532b6
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593 .elementor-element.elementor-element-35e15138 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-35e15138
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		line-height: 1.3em;
		}
		.elementor-26593
		.elementor-element.elementor-element-35e15138
		> .elementor-widget-container {
		margin: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6caffb10 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6caffb10
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-26593
		.elementor-element.elementor-element-6caffb10
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-weight: normal;
		line-height: 1.3em;
		letter-spacing: 1px;
		background-color: <?= $color_2 ?>;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 15px -5px rgba(1.4912280701754448, 1.4912280701754448, 1.4912280701754448, 0.33);
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #127c9400;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-2d9f4d2e
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2d9f4d2e {
		z-index: 2;
		}
		.elementor-26593
		.elementor-element.elementor-element-68c6b0fd
		.elementor-spacer-inner {
		--spacer-size: 100px;
		}
		.elementor-26593 .elementor-element.elementor-element-32a2ddef {
		padding: 0px 250px 0px 250px;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("<?= $asset_theme ?>backgrounds/Asset-Platinum-Virtual.jpg");
		background-position: center center;
		background-size: cover;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-element-populated
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(180deg, #ffffff00 0%, #ffffff 76%);
		opacity: 1;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-background-slideshow {
		border-radius: 200px 200px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 0px 0px 30px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-4df13f9b
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-a568590
		.elementor-spacer-inner {
		--spacer-size: 350px;
		}
		.elementor-26593 .elementor-element.elementor-element-1005c20 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-1005c20
		.elementor-heading-title {
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 600;
		line-height: 1.3em;
		}
		.elementor-26593
		.elementor-element.elementor-element-1005c20
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-3daa5702 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-3daa5702
		.elementor-heading-title {
		font-family: "Soligant", Sans-serif;
		font-size: 52px;
		font-weight: normal;
		}
		.elementor-26593 .elementor-element.elementor-element-649efca4 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-649efca4
		.elementor-heading-title {
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: normal;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-649efca4
		> .elementor-widget-container {
		margin: 20px 150px 20px 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-weight: normal;
		line-height: 1.3em;
		letter-spacing: 1px;
		background-color: <?= $color_2 ?>;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 15px -5px rgba(1.4912280701754448, 1.4912280701754448, 1.4912280701754448, 0.33);
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #127c9400;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-2a486249
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2a486249 {
		z-index: 2;
		}
		.elementor-26593
		.elementor-element.elementor-element-7176eedd:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-7176eedd
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: <?= $color_1 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-7176eedd {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 0px 200px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7176eedd
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-35d9f07 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-35d9f07
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 48px;
		font-weight: 600;
		}
		.elementor-26593
		.elementor-element.elementor-element-35d9f07
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-36d58e2b {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-36d58e2b
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-36d58e2b
		> .elementor-widget-container {
		margin: -20px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-43d07d60 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-43d07d60
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-43d07d60
		> .elementor-widget-container {
		margin: 0px 330px 0px 330px;
		}
		.elementor-26593
		.elementor-element.elementor-element-36fdda1a
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7d755972
		> .elementor-element-populated {
		margin: 0px 200px 0px 200px;
		--e-column-margin-right: 200px;
		--e-column-margin-left: 200px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-vertical
		.pp-timeline-item:not(:last-child) {
		margin-bottom: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-horizontal
		.pp-timeline-item {
		padding-left: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-horizontal
		.slick-list {
		margin-left: -15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.pp-timeline-card {
		padding: 20px 50px 50px 50px;
		background-color: #ffffff70;
		border-radius: 50px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.pp-timeline-card-content {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-card {
		text-align: left;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.pp-timeline-arrow {
		color: #ffffff70;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-card-image {
		margin-bottom: 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-card-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 36px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-card-title-wrap {
		margin-bottom: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.pp-timeline-item-active
		.pp-timeline-card,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.slick-current
		.pp-timeline-card {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.pp-timeline-item-active
		.pp-timeline-arrow,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline
		.slick-current
		.pp-timeline-arrow {
		color: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-marker {
		font-size: 13px;
		width: 30px;
		height: 30px;
		color: #ffffff00;
		background-color: <?= $color_2 ?>03;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-marker
		img {
		width: 13px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-connector-wrap {
		width: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-navigation:before,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-navigation
		.pp-slider-arrow {
		bottom: calc(30px / 2);
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-marker
		svg {
		fill: #ffffff00;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-item-active
		.pp-timeline-marker,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.slick-current
		.pp-timeline-marker {
		color: var(--e-global-color-secondary);
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-item-active
		.pp-timeline-marker
		svg,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.slick-current
		.pp-timeline-marker
		svg {
		fill: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-card-date {
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 500;
		line-height: 2.5em;
		color: <?= $color_2 ?>75;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-vertical.pp-timeline-left
		.pp-timeline-marker-wrapper {
		margin-right: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-vertical.pp-timeline-right
		.pp-timeline-marker-wrapper {
		margin-left: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-vertical.pp-timeline-center
		.pp-timeline-marker-wrapper {
		margin-left: 15px;
		margin-right: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-horizontal {
		margin-top: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-navigation
		.pp-timeline-card-date-wrapper {
		margin-bottom: 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-vertical
		.pp-timeline-connector {
		width: 3px;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-navigation:before {
		height: 3px;
		transform: translateY(calc(3px / 2));
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-connector,
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-navigation:before {
		background-color: <?= $color_2 ?>00;
		}
		.elementor-26593
		.elementor-element.elementor-element-fe8dda1
		.pp-timeline-connector-inner {
		background-color: #ffffff7a;
		}
		.elementor-26593
		.elementor-element.elementor-element-256eb13a:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-256eb13a
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("<?= $asset_theme ?>backgrounds/bg2.jpg");
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-26593
		.elementor-element.elementor-element-256eb13a
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(
			180deg,
			#ffffff00 0%,
			var(--e-global-color-secondary) 100%
		);
		opacity: 0.9;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-256eb13a {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 250px 0px 250px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-256eb13a
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-256eb13a
		> .elementor-shape-top
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593 .elementor-element.elementor-element-50614b85 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-50614b85
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 48px;
		font-weight: 600;
		}
		.elementor-26593 .elementor-element.elementor-element-51676832 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-51676832
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-51676832
		> .elementor-widget-container {
		margin: -20px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1600bf1
		> .elementor-widget-container {
		margin: -15px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-322f613b
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-353417a5 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-353417a5
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-353417a5
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7f7ec1b
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-36efaaa2 {
		--image-transition-duration: 800ms;
		--overlay-transition-duration: 800ms;
		--content-text-align: center;
		--content-padding: 20px;
		--content-transition-duration: 800ms;
		--content-transition-delay: 800ms;
		}
		.elementor-26593
		.elementor-element.elementor-element-36efaaa2
		.e-gallery-item:hover
		.elementor-gallery-item__overlay {
		background-color: rgba(0, 0, 0, 0.5);
		}
		.elementor-26593
		.elementor-element.elementor-element-efe2d53
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-64a7b8b6 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-64a7b8b6
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 20px;
		line-height: 1.2em;
		}
		.elementor-26593
		.elementor-element.elementor-element-64a7b8b6
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6785a56d img {
		width: 20%;
		}
		.elementor-26593 .elementor-element.elementor-element-3d0d2424 {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3d0d2424
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-3d0d2424
		.elementor-social-icon {
		background-color: rgba(129, 134, 213, 0);
		}
		.elementor-26593
		.elementor-element.elementor-element-3d0d2424
		.elementor-social-icon
		i {
		color: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-3d0d2424
		.elementor-social-icon
		svg {
		fill: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-background-overlay {
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-7a198c20 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 250px 0px 250px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-shape-top
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-7a198c20
		> .elementor-shape-bottom
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593 .elementor-element.elementor-element-aa11137 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-aa11137
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 56px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-aa11137
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-1f6345c2 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f6345c2
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1f6345c2
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-38583d82 {
		--divider-border-style: solid;
		--divider-color: #ffffff73;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-38583d82
		.elementor-divider-separator {
		width: 55%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-38583d82
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-15b996e7 {
		margin-top: 0px;
		margin-bottom: 30px;
		padding: 0px 250px 0px 250px;
		}
		.elementor-26593
		.elementor-element.elementor-element-c7a6987
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-26593
		.elementor-element.elementor-element-c7a6987
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-c7a6987
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-c7a6987
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-26593
		.elementor-element.elementor-element-edff565
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-26593
		.elementor-element.elementor-element-edff565
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-edff565
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-edff565
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-26593 .elementor-element.elementor-element-680dc18c {
		margin-top: 30px;
		margin-bottom: 0px;
		padding: 0px 250px 0px 250px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2fce9d1f
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-26593
		.elementor-element.elementor-element-2fce9d1f
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-2fce9d1f
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-2fce9d1f
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f88bba4
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f88bba4
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f88bba4
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f88bba4
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2eb241c3:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-2eb241c3
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		background-image: url("<?= $asset_theme ?>backgrounds/bg1.jpg");
		background-position: center center;
		background-size: 50% auto;
		}
		.elementor-26593
		.elementor-element.elementor-element-2eb241c3
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: radial-gradient(
			at center center,
			#ffffffde 49%,
			var(--e-global-color-secondary) 81%
		);
		opacity: 1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-2eb241c3 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 250px 0px 250px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-63860774 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-63860774
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 600;
		}
		.elementor-26593 .elementor-element.elementor-element-670b6a3 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-670b6a3
		.elementor-heading-title {
		color: <?= $color_2 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-670b6a3
		> .elementor-widget-container {
		margin: -15px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2f770391 {
		--divider-border-style: solid;
		--divider-color: <?= $color_1 ?>4a;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-2f770391
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-2f770391
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-786700a0 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-786700a0
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-786700a0
		> .elementor-widget-container {
		margin: 0px 300px 0px 300px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7cab197e
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-26593
		.elementor-element.elementor-element-55d3263:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: <?= $color_5 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-background-slideshow {
		border-radius: 50px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 150px 0px 150px;
		--e-column-margin-right: 150px;
		--e-column-margin-left: 150px;
		padding: 35px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-55d3263
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-70abb54 {
		margin-top: 20px;
		margin-bottom: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-cf89f20:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-background-slideshow {
		border-radius: 50px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 70px 30px 40px 30px;
		}
		.elementor-26593
		.elementor-element.elementor-element-cf89f20
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-9cbd728 .wdp-wrapper {
		background-color: #ffffff00;
		border-radius: 40px 40px 40px 40px;
		padding: 0px 0px 20px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-komentar-cswd
		input[type="text"],
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-komentar-cswd
		select,
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-komentar-cswd
		textarea {
		font-family: "Lora", Sans-serif;
		font-weight: 500;
		line-height: 1em;
		border-radius: 13px 13px 13px 13px;
		padding: 8px 8px 8px 8px;
		}
		.elementor-26593 .elementor-element.elementor-element-9cbd728 .wdp-wrap-link a {
		font-family: "Averia Serif Libre", Sans-serif;
		font-weight: normal;
		line-height: 1em;
		color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-submit
		input[type="submit"],
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-submit
		button {
		font-family: var(--e-global-typography-text-font-family), Sans-serif;
		font-weight: var(--e-global-typography-text-font-weight);
		line-height: var(--e-global-typography-text-line-height);
		background-color: <?= $color_2 ?>;
		border-style: solid;
		border-color: <?= $color_2 ?>;
		border-radius: 12px 12px 12px 12px;
		padding: 7px 25px 7px 25px;
		}
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-submit
		input[type="submit"]:hover,
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		.form-submit
		button:hover {
		color: <?= $color_2 ?>;
		background-color: <?= $color_2 ?>00;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-9cbd728
		> .elementor-widget-container {
		margin: -90px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: <?= $color_1 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-background-overlay {
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-a0053f1 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 250px 0px 250px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-shape-top
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-a0053f1
		> .elementor-shape-bottom
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593 .elementor-element.elementor-element-0f9d978 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-0f9d978
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-0f9d978
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-cc42991 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-cc42991
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 25px;
		font-weight: 600;
		line-height: 1.3em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-cc42991
		> .elementor-widget-container {
		margin: 0px 150px 0px 150px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-dbb1c5e:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-dbb1c5e
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		background-image: url("<?= $asset_theme ?>backgrounds/bg1.jpg");
		background-position: center center;
		background-size: 50% auto;
		}
		.elementor-26593
		.elementor-element.elementor-element-dbb1c5e
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: radial-gradient(
			at center center,
			#ffffffde 49%,
			var(--e-global-color-secondary) 81%
		);
		opacity: 1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-dbb1c5e {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 250px 0px 250px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-f3f5424 {
		padding: 0px 200px 0px 200px;
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-01d29e9.elementor-column
		.elementor-widget-wrap {
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-01d29e9.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: flex-start;
		align-items: flex-start;
		}
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-element-populated {
		border-style: solid;
		border-width: 5px 5px 5px 5px;
		border-color: #ab9d9c4f;
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 100px 50px 50px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-element-populated,
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-background-slideshow {
		border-radius: 200px 200px 200px 200px;
		}
		.elementor-26593
		.elementor-element.elementor-element-01d29e9
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-8498600
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-8498600.elementor-view-stacked
		.elementor-icon {
		background-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-8498600.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-8498600.elementor-view-default
		.elementor-icon {
		color: <?= $color_2 ?>;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-8498600.elementor-view-framed
		.elementor-icon,
		.elementor-26593
		.elementor-element.elementor-element-8498600.elementor-view-default
		.elementor-icon
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593 .elementor-element.elementor-element-8498600 .elementor-icon {
		font-size: 75px;
		}
		.elementor-26593 .elementor-element.elementor-element-8498600 .elementor-icon i,
		.elementor-26593
		.elementor-element.elementor-element-8498600
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593 .elementor-element.elementor-element-b2ae688 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-b2ae688
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-b2ae688
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6f00f7b {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f00f7b
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6f00f7b
		> .elementor-widget-container {
		margin: 0px 50px 0px 50px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 600;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: <?= $color_2 ?>;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #af8e4f00;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-a2eca50
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-bcc0bd3
		.elementor-spacer-inner {
		--spacer-size: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-2f74342 {
		margin-top: 20px;
		margin-bottom: 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-30baf7e
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-30baf7e
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 70px 30px 40px 30px;
		}
		.elementor-26593 .elementor-element.elementor-element-f066c95 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-f066c95
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 48px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-f066c95
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-f676723 {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>63;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-f676723
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-f676723
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-8eb8230 img {
		width: 25%;
		}
		.elementor-26593
		.elementor-element.elementor-element-8eb8230
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593 .elementor-element.elementor-element-5284c3d .copy-content {
		color: <?= $color_2 ?>;
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 36px;
		font-weight: 400;
		line-height: 1.5em;
		}
		.elementor-26593 .elementor-element.elementor-element-5284c3d .head-title {
		color: #5d3c38;
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 24px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		a.elementor-button,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button {
		font-family: "Montserrat", Sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		a.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		a.elementor-button:focus,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #af8e4f00;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		a.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		a.elementor-button:focus
		svg,
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-5284c3d
		.elementor-button {
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		}
		.elementor-26593
		.elementor-element.elementor-element-be897a6
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-26593 .elementor-element.elementor-element-8262a89 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-8262a89
		.elementor-heading-title {
		color: <?= $color_1 ?>;
		font-family: "Soligant", Sans-serif;
		font-size: 48px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-8262a89
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6bd5cc7 {
		--divider-border-style: solid;
		--divider-color: <?= $color_2 ?>63;
		--divider-border-width: 2px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6bd5cc7
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-26593
		.elementor-element.elementor-element-6bd5cc7
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-26593 .elementor-element.elementor-element-879131a .copy-content {
		color: <?= $color_2 ?>;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: normal;
		font-style: italic;
		line-height: 1.3em;
		letter-spacing: 0.5px;
		}
		.elementor-26593 .elementor-element.elementor-element-879131a .head-title {
		color: #5d3c38;
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 24px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		a.elementor-button,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button {
		font-family: "Montserrat", Sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: <?= $color_2 ?>;
		border-radius: 13px 13px 13px 13px;
		padding: 10px 20px 10px 20px;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		a.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		a.elementor-button:focus,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button:focus {
		color: <?= $color_2 ?>;
		background-color: #af8e4f00;
		border-color: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		a.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button:hover
		svg,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		a.elementor-button:focus
		svg,
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button:focus
		svg {
		fill: <?= $color_2 ?>;
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		.elementor-button {
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: <?= $color_2 ?>;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		}
		.elementor-26593
		.elementor-element.elementor-element-879131a
		> .elementor-widget-container {
		margin: 0px 100px 0px 100px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e81adb2:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
		.elementor-element.elementor-element-6e81adb2
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		background-image: url("<?= $asset_theme ?>photos/Galeri-6.jpg");
		background-position: center center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e81adb2
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(180deg, #615856 50%, #020202db 75%);
		opacity: 1;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-6e81adb2 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 250px 0px 150px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e81adb2
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: #ffffff;
		}
		.elementor-26593
		.elementor-element.elementor-element-6e81adb2
		> .elementor-shape-top
		svg {
		width: calc(100% + 1.3px);
		height: 150px;
		}
		.elementor-26593 .elementor-element.elementor-element-3720215f {
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6d7f848b {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-6d7f848b
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 600;
		}
		.elementor-26593 .elementor-element.elementor-element-5c7c24a2 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-5c7c24a2
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-26593
		.elementor-element.elementor-element-5c7c24a2
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-53ca8e70 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-53ca8e70
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-26593 .elementor-element.elementor-element-7174f094 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-7174f094
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: 100;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-26593
		.elementor-element.elementor-element-7174f094
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-749e59fa
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-26593 .elementor-element.elementor-element-2158efcc {
		padding: 0px 0px 0px 0px;
		}
		.elementor-26593
		.elementor-element.elementor-element-1469ed45
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 1px 0px 0px;
		border-color: var(--e-global-color-secondary);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-1469ed45
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-420b0a95 img {
		width: 60%;
		}
		.elementor-26593
		.elementor-element.elementor-element-420b0a95
		> .elementor-widget-container {
		margin: 0px 0px -15px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-76eb38 {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 18px;
		}
		.elementor-26593
		.elementor-element.elementor-element-76eb38
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-76eb38
		.elementor-social-icon {
		background-color: rgba(129, 134, 213, 0);
		--icon-padding: 0.3em;
		}
		.elementor-26593
		.elementor-element.elementor-element-76eb38
		.elementor-social-icon
		i {
		color: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-76eb38
		.elementor-social-icon
		svg {
		fill: var(--e-global-color-secondary);
		}
		.elementor-bc-flex-widget
		.elementor-26593
		.elementor-element.elementor-element-72db2ad4.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-72db2ad4.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-72db2ad4
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 7px;
		}
		.elementor-26593
		.elementor-element.elementor-element-72db2ad4
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 0px 0px 1px;
		border-color: var(--e-global-color-secondary);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-26593
		.elementor-element.elementor-element-72db2ad4
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-26593 .elementor-element.elementor-element-42e268c9 img {
		width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-eacbda6 {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-eacbda6
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 7px;
		font-weight: normal;
		line-height: 1.3em;
		}
		.elementor-26593
		.elementor-element.elementor-element-3268dc4a
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-26593
		.elementor-element.elementor-element-3268dc4a
		.elementor-icon
		i,
		.elementor-26593
		.elementor-element.elementor-element-3268dc4a
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-26593 .elementor-element.elementor-element-3268dc4a {
		width: initial;
		max-width: initial;
		bottom: 150px;
		z-index: 999;
		}
		body:not(.rtl) .elementor-26593 .elementor-element.elementor-element-3268dc4a {
		left: 50px;
		}
		body.rtl .elementor-26593 .elementor-element.elementor-element-3268dc4a {
		right: 50px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-dear {
		text-align: center;
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 500;
		letter-spacing: 0.5px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-text {
		text-align: center;
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-style: italic;
		letter-spacing: 1px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .overlayy {
		background-color: transparent;
		background-image: linear-gradient(180deg, #0101019e 0%, #000000 100%);
		opacity: 0.95;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-name {
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .dwp-sesi {
		margin-top: 20px;
		font-family: "Poppins", Sans-serif;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .text_tambahan {
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 500;
		letter-spacing: 9px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.wdp-button-wrapper {
		margin-top: 40px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.elementor-image
		img {
		width: 10%;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-mempelai {
		font-family: "Soligant", Sans-serif;
		font-size: 72px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		a.elementor-button,
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.elementor-button {
		fill: var(--e-global-color-1ac7aadb);
		color: var(--e-global-color-1ac7aadb);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		font-weight: 500;
		line-height: 1.3em;
		background-color: var(--e-global-color-secondary);
		border-radius: 13px 13px 13px 13px;
		padding: 7px 15px 7px 15px;
		}
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.elementor-button {
		border-style: solid;
		border-width: 1px 1px 1px 1px;
		border-color: var(--e-global-color-secondary);
		}
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		a.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.elementor-button:hover,
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		a.elementor-button:focus,
		.elementor-26593
		.elementor-element.elementor-element-3328422a
		.elementor-button:focus {
		color: var(--e-global-color-secondary);
		background-color: #ffffff00;
		border-color: var(--e-global-color-secondary);
		}
		.elementor-widget .tippy-tooltip .tippy-content {
		text-align: center;
		}
		@media (max-width: 1024px) {
		.elementor-26593 .elementor-element.elementor-element-7b4400cf {
			margin-top: 0px;
			margin-bottom: 70px;
			padding: 0px 50px 0px 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2f930b55
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6c5f118e {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-f1b747a {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4fb69753
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-54115b94 {
			padding: 0px 10px 0px 10px;
		}
		.elementor-26593 .elementor-element.elementor-element-39ffc1b8 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-33f517de {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7176eedd {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-tablet-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-tablet-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-tablet-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
			margin-right: 5px;
		}
		.elementor-26593 .elementor-element.elementor-element-256eb13a {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-353417a5
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-64a7b8b6
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43a30275
			> .elementor-element-populated {
			padding: 75px 50px 70px 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f6345c2
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-15b996e7 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 250px 0px 250px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-26593 .elementor-element.elementor-element-680dc18c {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 250px 0px 250px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-26593
			.elementor-element.elementor-element-786700a0
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-submit
			input[type="submit"],
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-submit
			button {
			line-height: var(--e-global-typography-text-line-height);
		}
		.elementor-26593
			.elementor-element.elementor-element-0f9d978
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-cc42991
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7174f094
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		}
		@media (max-width: 767px) {
		.elementor-26593
			.elementor-element.elementor-element-12e74c9b:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-12e74c9b
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-position: center center;
			background-repeat: no-repeat;
			background-size: cover;
		}
		.elementor-26593 .elementor-element.elementor-element-12e74c9b {
			padding: 0px 0px 0px 0px;
			z-index: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-54f0e56b
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-54f0e56b
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 500px 0px 150px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-55c8fcd1
			.elementor-heading-title {
			font-size: 14px;
			letter-spacing: 3px;
		}
		.elementor-26593
			.elementor-element.elementor-element-18f2eb8e
			.elementor-heading-title {
			font-size: 36px;
			line-height: 45px;
		}
		.elementor-26593
			.elementor-element.elementor-element-18f2eb8e
			> .elementor-widget-container {
			margin: 0px 0px -10px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7bbf8daa
			.elementor-divider-separator {
			width: 50%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-7bbf8daa
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3aad3ac8
			.elementor-heading-title {
			font-size: 24px;
			letter-spacing: 2px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3aad3ac8
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7ea4e7ec {
			--icon-box-icon-margin: -5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7ea4e7ec
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7ea4e7ec
			.elementor-icon-box-title,
		.elementor-26593
			.elementor-element.elementor-element-7ea4e7ec
			.elementor-icon-box-title
			a {
			font-size: 8px;
			letter-spacing: 0.5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7ea4e7ec
			> .elementor-widget-container {
			margin: 10px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-83e0e69
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-40b529
			.elementor-spacer-inner {
			--spacer-size: 80px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7b4400cf:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-7b4400cf
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-size: 300px auto;
		}
		.elementor-26593 .elementor-element.elementor-element-7b4400cf {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 70px 20px 100px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4de0ec86
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6716efc4
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-50a4b2c7
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-26593
			.elementor-element.elementor-element-50a4b2c7
			> .elementor-widget-container {
			margin: -20px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2f930b55
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-2f930b55
			> .elementor-widget-container {
			margin: 0px 35px 0px 35px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-50a70519
			.elementor-spacer-inner {
			--spacer-size: 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-6c5f118e {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-a3b8117 {
			width: 50%;
			z-index: 2;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-a3b8117.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-a3b8117.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-a3b8117.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-a3b8117
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a3b8117
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-312f9a8b {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-312f9a8b
			.elementor-heading-title {
			font-size: 16px;
			line-height: 1.3em;
		}
		.elementor-26593
			.elementor-element.elementor-element-312f9a8b
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd6ab1a
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd6ab1a
			.elementor-divider {
			text-align: center;
			padding-top: 2px;
			padding-bottom: 2px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd6ab1a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-4f671a99 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-4f671a99
			.elementor-heading-title {
			font-size: 9px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4f671a99
			> .elementor-widget-container {
			margin: 0px 0px -5px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-19d66ff6 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-19d66ff6
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-19d66ff6
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-47514af6
			.elementor-widget-container {
			text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-47514af6 {
			--icon-size: 25px;
			--grid-column-gap: 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-47514af6
			.elementor-social-icon {
			--icon-padding: 0em;
		}
		.elementor-26593
			.elementor-element.elementor-element-47514af6
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6e1a4a71 {
			width: 50%;
			z-index: 1;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-6e1a4a71.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e1a4a71.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e1a4a71
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e1a4a71
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-bc781e8 {
			text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-bc781e8 img {
			width: 75%;
			border-radius: 110px 110px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-74479eb1 {
			width: 100%;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-74479eb1.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-26593
			.elementor-element.elementor-element-74479eb1.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-26593
			.elementor-element.elementor-element-74479eb1
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-74479eb1
			> .elementor-element-populated {
			margin: 10px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-4c123b15 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-4c123b15
			.elementor-heading-title {
			font-size: 72px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4c123b15
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-2db4090d {
			width: 50%;
			z-index: 1;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-2db4090d.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-2db4090d.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-2db4090d.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-2db4090d
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2db4090d
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7f06c0be {
			text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-7f06c0be img {
			width: 75%;
			border-radius: 110px 110px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-221c3ce7 {
			width: 50%;
			z-index: 2;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-221c3ce7.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-221c3ce7.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-221c3ce7
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-221c3ce7
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 20px 0px 20px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-75355ed8 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-75355ed8
			.elementor-heading-title {
			font-size: 16px;
			line-height: 1.3em;
		}
		.elementor-26593
			.elementor-element.elementor-element-75355ed8
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43167651
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-43167651
			.elementor-divider {
			text-align: center;
			padding-top: 2px;
			padding-bottom: 2px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43167651
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-70115f1f {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-70115f1f
			.elementor-heading-title {
			font-size: 9px;
		}
		.elementor-26593
			.elementor-element.elementor-element-70115f1f
			> .elementor-widget-container {
			margin: 0px 0px -5px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-319fa485 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-319fa485
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-319fa485
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-12f38211
			.elementor-widget-container {
			text-align: center;
		}
		.elementor-26593 .elementor-element.elementor-element-12f38211 {
			--icon-size: 25px;
			--grid-column-gap: 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-12f38211
			.elementor-social-icon {
			--icon-padding: 0em;
		}
		.elementor-26593
			.elementor-element.elementor-element-12f38211
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-310afb79
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-310afb79
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-310afb79 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 150px 20px 150px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-33f43cc5
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-33f43cc5
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-34e30737
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4636262a
			.elementor-heading-title {
			font-size: 34px;
			line-height: 1em;
		}
		.elementor-26593
			.elementor-element.elementor-element-4636262a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8.bdt-countdown--label-block
			.bdt-countdown-number {
			margin-bottom: 7px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8.bdt-countdown--label-inline
			.bdt-countdown-number {
			margin-right: 7px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8
			.bdt-countdown-wrapper {
			max-width: 150%;
			margin-left: auto;
			margin-right: auto;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8
			.bdt-countdown-item {
			border-width: 1px 0px 0px 1px;
			border-radius: 15px 15px 15px 15px;
			padding: 15px 15px 15px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8
			.bdt-countdown-number {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8
			.bdt-countdown-label {
			font-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5329bec8
			> .elementor-widget-container {
			margin: 20px 0px 20px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-580eaa66
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-580eaa66
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43cea9c0
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-f1b747a:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-f1b747a
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-size: 300px auto;
		}
		.elementor-26593
			.elementor-element.elementor-element-f1b747a
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-f1b747a {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 30px 150px 30px;
		}
		.elementor-26593
			.elementor-element.elementor-element-14204cba
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-14204cba
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5b48a037
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-26929aec
			.elementor-heading-title {
			font-size: 42px;
		}
		.elementor-26593
			.elementor-element.elementor-element-26929aec
			> .elementor-widget-container {
			margin: -5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4fb69753
			.elementor-heading-title {
			font-size: 12px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-4fb69753
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-681780f8
			.elementor-spacer-inner {
			--spacer-size: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-54115b94 {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f92964b
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f92964b
			> .elementor-element-populated {
			border-width: 5px 5px 5px 5px;
			margin: 0px 0px 50px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 60px 0px 50px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f92964b
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-1f92964b
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-1f92964b
			> .elementor-background-slideshow {
			border-radius: 75px 75px 75px 75px;
		}
		.elementor-26593
			.elementor-element.elementor-element-27710c7b
			.elementor-icon {
			font-size: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-143de12e
			.elementor-spacer-inner {
			--spacer-size: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5beb6017
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5beb6017
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-30681bb1
			.elementor-divider-separator {
			width: 50%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-30681bb1
			.elementor-divider {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-16a3b19e
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-16a3b19e
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-39ffc1b8 {
			margin-top: 0px;
			margin-bottom: 10px;
			padding: 0px 40px 0px 40px;
		}
		.elementor-26593 .elementor-element.elementor-element-22f55591 {
			width: 35%;
		}
		.elementor-26593
			.elementor-element.elementor-element-22f55591
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7b7be058 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-7b7be058
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7b7be058
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-27217f0 {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-27217f0
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-75cc5e4c
			.elementor-counter-number-wrapper {
			font-size: 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-75cc5e4c
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-34424cfc {
			width: 35%;
		}
		.elementor-26593
			.elementor-element.elementor-element-34424cfc
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-180d2b7f {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-180d2b7f
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-180d2b7f
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-10782627
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-10782627
			> .elementor-widget-container {
			margin: 0px 0px 5px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-44595f0
			.elementor-divider-separator {
			width: 75%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-44595f0
			.elementor-divider {
			text-align: center;
			padding-top: 25px;
			padding-bottom: 25px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e3ee36
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-551fb491
			.elementor-heading-title {
			font-size: 16px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-551fb491
			> .elementor-widget-container {
			margin: 5px 40px 5px 40px;
		}
		.elementor-26593
			.elementor-element.elementor-element-565b7de1
			.elementor-heading-title {
			font-size: 12px;
			line-height: 1.3em;
			letter-spacing: 0.5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-565b7de1
			> .elementor-widget-container {
			margin: 0px 50px 0px 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-93fd04a
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-93fd04a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5037a4a7
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5037a4a7
			> .elementor-element-populated {
			border-width: 5px 5px 5px 5px;
			margin: 50px 0px 50px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 60px 0px 50px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5037a4a7
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-5037a4a7
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-5037a4a7
			> .elementor-background-slideshow {
			border-radius: 75px 75px 75px 75px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6efc2ffb
			.elementor-icon {
			font-size: 75px;
		}
		.elementor-26593
			.elementor-element.elementor-element-260cbace
			.elementor-spacer-inner {
			--spacer-size: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2e037502
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2e037502
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7fa0580d
			.elementor-divider-separator {
			width: 50%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-7fa0580d
			.elementor-divider {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-75429b1f
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-75429b1f
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-33f517de {
			margin-top: 0px;
			margin-bottom: 10px;
			padding: 0px 40px 0px 40px;
		}
		.elementor-26593 .elementor-element.elementor-element-3853853 {
			width: 35%;
		}
		.elementor-26593
			.elementor-element.elementor-element-3853853
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-d0ed149 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-d0ed149
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-d0ed149
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-12bd4b3a {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-12bd4b3a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1272b3fa
			.elementor-counter-number-wrapper {
			font-size: 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1272b3fa
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-22326808 {
			width: 35%;
		}
		.elementor-26593
			.elementor-element.elementor-element-22326808
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-adef9b7 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-adef9b7
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-adef9b7
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-353be9d9
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-353be9d9
			> .elementor-widget-container {
			margin: 0px 0px 5px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2628856c
			.elementor-divider-separator {
			width: 75%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-2628856c
			.elementor-divider {
			text-align: center;
			padding-top: 25px;
			padding-bottom: 25px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f532b6
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-35e15138
			.elementor-heading-title {
			font-size: 16px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-35e15138
			> .elementor-widget-container {
			margin: 5px 40px 5px 40px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6caffb10
			.elementor-heading-title {
			font-size: 12px;
			line-height: 1.3em;
			letter-spacing: 0.5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6caffb10
			> .elementor-widget-container {
			margin: 0px 50px 0px 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2d9f4d2e
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2d9f4d2e
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-68c6b0fd
			.elementor-spacer-inner {
			--spacer-size: 50px;
		}
		.elementor-26593 .elementor-element.elementor-element-32a2ddef,
		.elementor-26593
			.elementor-element.elementor-element-32a2ddef
			> .elementor-background-overlay {
			border-radius: 50px 50px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-32a2ddef {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b:not(.elementor-motion-effects-element-type-background)
			> .elementor-widget-wrap,
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-widget-wrap
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-image: url("<?= $asset_theme ?>backgrounds/Asset-Platinum-Virtual.jpg");
			background-position: center center;
			background-size: cover;
		}
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-background-slideshow {
			border-radius: 100px 100px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-4df13f9b
			> .elementor-element-populated {
			padding: 0px 0px 50px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a568590
			.elementor-spacer-inner {
			--spacer-size: 300px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1005c20
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1005c20
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3daa5702
			.elementor-heading-title {
			font-size: 30px;
			line-height: 0.8em;
		}
		.elementor-26593
			.elementor-element.elementor-element-649efca4
			.elementor-heading-title {
			font-size: 11px;
		}
		.elementor-26593
			.elementor-element.elementor-element-649efca4
			> .elementor-widget-container {
			margin: 10px 50px 10px 50px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2a486249
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2a486249
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-7176eedd {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 20px 100px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-45cf3b03
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-45cf3b03
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-35d9f07
			.elementor-heading-title {
			font-size: 24px;
		}
		.elementor-26593
			.elementor-element.elementor-element-36d58e2b
			.elementor-heading-title {
			font-size: 36px;
			line-height: 0.8em;
		}
		.elementor-26593
			.elementor-element.elementor-element-36d58e2b
			> .elementor-widget-container {
			margin: -5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43d07d60
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43d07d60
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
		}
		.elementor-26593
			.elementor-element.elementor-element-36fdda1a
			.elementor-spacer-inner {
			--spacer-size: 25px;
		}
		.elementor-26593 .elementor-element.elementor-element-6958710f {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7d755972
			> .elementor-element-populated {
			margin: 0px 5px 0px 0px;
			--e-column-margin-right: 5px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline
			.pp-timeline-card {
			padding: 30px 25px 25px 25px;
			border-radius: 35px 35px 35px 35px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-card {
			text-align: left;
			font-size: 11px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-card-title {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-card-title-wrap {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-marker {
			font-size: 8px;
			width: 20px;
			height: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-marker
			img {
			width: 8px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-connector-wrap {
			width: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-navigation:before,
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-navigation
			.pp-slider-arrow {
			bottom: calc(20px / 2);
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-card-date {
			font-size: 12px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-mobile-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px !important;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-mobile-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px !important;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-mobile-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px !important;
			margin-right: 5px !important;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-vertical.pp-timeline-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-horizontal {
			margin-top: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-fe8dda1
			.pp-timeline-navigation
			.pp-timeline-card-date-wrapper {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-256eb13a
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-256eb13a {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 120px 20px 100px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-13dbe0fa
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-13dbe0fa
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-50614b85
			.elementor-heading-title {
			font-size: 24px;
		}
		.elementor-26593
			.elementor-element.elementor-element-51676832
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-26593
			.elementor-element.elementor-element-51676832
			> .elementor-widget-container {
			margin: -5px 0px 10px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1600bf1
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-322f613b
			.elementor-spacer-inner {
			--spacer-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-353417a5
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.3em;
		}
		.elementor-26593
			.elementor-element.elementor-element-353417a5
			> .elementor-widget-container {
			margin: 0px 20px 0px 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7f7ec1b
			.elementor-spacer-inner {
			--spacer-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-64a7b8b6
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.2em;
		}
		.elementor-26593
			.elementor-element.elementor-element-64a7b8b6
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6785a56d img {
			width: 35%;
		}
		.elementor-26593 .elementor-element.elementor-element-3d0d2424 {
			--icon-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7a198c20
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7a198c20
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-7a198c20 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 150px 20px 150px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43a30275
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43a30275
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-aa11137
			.elementor-heading-title {
			font-size: 42px;
			letter-spacing: 3px;
		}
		.elementor-26593
			.elementor-element.elementor-element-aa11137
			> .elementor-widget-container {
			margin: 5px 0px 5px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f6345c2
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.3em;
		}
		.elementor-26593
			.elementor-element.elementor-element-1f6345c2
			> .elementor-widget-container {
			margin: 0px 20px 0px 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-38583d82
			.elementor-divider-separator {
			width: 75%;
		}
		.elementor-26593
			.elementor-element.elementor-element-38583d82
			.elementor-divider {
			padding-top: 25px;
			padding-bottom: 25px;
		}
		.elementor-26593
			.elementor-element.elementor-element-38583d82
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-15b996e7 {
			margin-top: 0px;
			margin-bottom: 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-d44538f {
			width: 50%;
		}
		.elementor-26593
			.elementor-element.elementor-element-d44538f
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-26593
			.elementor-element.elementor-element-c7a6987
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-65e198e7 {
			width: 50%;
		}
		.elementor-26593
			.elementor-element.elementor-element-65e198e7
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-26593
			.elementor-element.elementor-element-edff565
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-680dc18c {
			margin-top: 20px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-5fc38f52 {
			width: 50%;
		}
		.elementor-26593
			.elementor-element.elementor-element-5fc38f52
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-26593
			.elementor-element.elementor-element-2fce9d1f
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-cffda27 {
			width: 50%;
		}
		.elementor-26593
			.elementor-element.elementor-element-cffda27
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f88bba4
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2eb241c3:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-2eb241c3
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-size: 300px auto;
		}
		.elementor-26593 .elementor-element.elementor-element-2eb241c3 {
			padding: 100px 20px 100px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-47f180e2
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-47f180e2
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-63860774
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-26593
			.elementor-element.elementor-element-670b6a3
			.elementor-heading-title {
			font-size: 30px;
		}
		.elementor-26593
			.elementor-element.elementor-element-670b6a3
			> .elementor-widget-container {
			margin: -5px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-2f770391
			.elementor-divider-separator {
			width: 75%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-2f770391
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-786700a0
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-26593
			.elementor-element.elementor-element-786700a0
			> .elementor-widget-container {
			margin: 0px 20px 0px 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7cab197e
			.elementor-spacer-inner {
			--spacer-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-55d3263
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-55d3263
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-55d3263
			> .elementor-background-slideshow {
			border-radius: 45px 45px 45px 45px;
		}
		.elementor-26593
			.elementor-element.elementor-element-55d3263
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 15px 15px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-cf89f20
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-cf89f20
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-cf89f20
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-cf89f20
			> .elementor-background-slideshow {
			border-radius: 40px 40px 40px 40px;
		}
		.elementor-26593
			.elementor-element.elementor-element-cf89f20
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-komentar-cswd
			input[type="text"],
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-komentar-cswd
			select,
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-komentar-cswd
			textarea {
			font-size: 12px;
			line-height: 1.3em;
		}
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.wdp-wrap-link
			a {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-submit
			input[type="submit"],
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			.form-submit
			button {
			line-height: var(--e-global-typography-text-line-height);
			border-width: 2px 2px 2px 2px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9cbd728
			> .elementor-widget-container {
			margin: -30px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a0053f1
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a0053f1
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-a0053f1 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 30px 100px 30px;
		}
		.elementor-26593
			.elementor-element.elementor-element-d3595ad
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-d3595ad
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-0f9d978
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-26593
			.elementor-element.elementor-element-0f9d978
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-cc42991
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-cc42991
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-dbb1c5e:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-dbb1c5e
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-size: 300px auto;
		}
		.elementor-26593 .elementor-element.elementor-element-dbb1c5e {
			padding: 100px 30px 100px 30px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9c8873a
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-9c8873a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-f3f5424 {
			padding: 0px 10px 0px 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-01d29e9
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-01d29e9
			> .elementor-element-populated {
			border-width: 5px 5px 5px 5px;
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 50px 20px 20px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-01d29e9
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-01d29e9
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-01d29e9
			> .elementor-background-slideshow {
			border-radius: 75px 75px 75px 75px;
		}
		.elementor-26593
			.elementor-element.elementor-element-8498600
			.elementor-icon {
			font-size: 45px;
		}
		.elementor-26593
			.elementor-element.elementor-element-b2ae688
			.elementor-heading-title {
			font-size: 28px;
			line-height: 0.8em;
		}
		.elementor-26593
			.elementor-element.elementor-element-b2ae688
			> .elementor-widget-container {
			margin: 10px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-6f00f7b {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f00f7b
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6f00f7b
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a2eca50
			.elementor-button {
			font-size: 12px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-a2eca50
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-bcc0bd3
			.elementor-spacer-inner {
			--spacer-size: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-30baf7e
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-30baf7e
			> .elementor-element-populated,
		.elementor-26593
			.elementor-element.elementor-element-30baf7e
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-26593
			.elementor-element.elementor-element-30baf7e
			> .elementor-background-slideshow {
			border-radius: 40px 40px 40px 40px;
		}
		.elementor-26593
			.elementor-element.elementor-element-30baf7e
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 30px 20px 20px 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-f066c95
			.elementor-heading-title {
			font-size: 24px;
			line-height: 0.8em;
		}
		.elementor-26593
			.elementor-element.elementor-element-f066c95
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-f676723
			.elementor-divider-separator {
			width: 75%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-f676723
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-f676723
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-8eb8230 img {
			width: 50%;
		}
		.elementor-26593
			.elementor-element.elementor-element-8eb8230
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-5284c3d .copy-content {
			font-size: 20px;
			line-height: 1em;
		}
		.elementor-26593 .elementor-element.elementor-element-5284c3d .head-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5284c3d
			a.elementor-button,
		.elementor-26593
			.elementor-element.elementor-element-5284c3d
			.elementor-button {
			font-size: 12px;
			padding: 5px 15px 5px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-8262a89
			.elementor-heading-title {
			font-size: 24px;
			line-height: 0.8em;
		}
		.elementor-26593
			.elementor-element.elementor-element-8262a89
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd5cc7
			.elementor-divider-separator {
			width: 75%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd5cc7
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6bd5cc7
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-879131a .copy-content {
			font-size: 10px;
			line-height: 1.5em;
			letter-spacing: 0.5px;
		}
		.elementor-26593 .elementor-element.elementor-element-879131a .head-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-879131a
			a.elementor-button,
		.elementor-26593
			.elementor-element.elementor-element-879131a
			.elementor-button {
			font-size: 12px;
			padding: 5px 15px 5px 15px;
		}
		.elementor-26593
			.elementor-element.elementor-element-879131a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e81adb2
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 70px;
		}
		.elementor-26593 .elementor-element.elementor-element-6e81adb2 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 200px 30px 50px 30px;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-3672441a.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-3672441a.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-3672441a.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-3672441a
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3672441a
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-43eaec95
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-26593
			.elementor-element.elementor-element-6d7f848b
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5c7c24a2
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-26593
			.elementor-element.elementor-element-5c7c24a2
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593
			.elementor-element.elementor-element-53ca8e70
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7174f094
			.elementor-heading-title {
			font-size: 11px;
		}
		.elementor-26593
			.elementor-element.elementor-element-7174f094
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-40f9b29b {
			width: 15%;
		}
		.elementor-26593 .elementor-element.elementor-element-1469ed45 {
			width: 35%;
		}
		.elementor-bc-flex-widget
			.elementor-26593
			.elementor-element.elementor-element-1469ed45.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-1469ed45.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-1469ed45
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 7px;
		}
		.elementor-26593
			.elementor-element.elementor-element-1469ed45
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 10px 0px 10px 15px;
		}
		.elementor-26593 .elementor-element.elementor-element-420b0a95 img {
			width: 60%;
		}
		.elementor-26593
			.elementor-element.elementor-element-420b0a95
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-76eb38 {
			--icon-size: 18px;
		}
		.elementor-26593 .elementor-element.elementor-element-72db2ad4 {
			width: 35%;
		}
		.elementor-26593
			.elementor-element.elementor-element-72db2ad4
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-26593
			.elementor-element.elementor-element-72db2ad4
			> .elementor-element-populated {
			padding: 10px 15px 10px 0px;
		}
		.elementor-26593 .elementor-element.elementor-element-42e268c9 img {
			width: 60%;
		}
		.elementor-26593 .elementor-element.elementor-element-eacbda6 {
			text-align: center;
		}
		.elementor-26593
			.elementor-element.elementor-element-eacbda6
			.elementor-heading-title {
			font-size: 5px;
			line-height: 1.4em;
		}
		.elementor-26593 .elementor-element.elementor-element-34515251 {
			width: 15%;
		}
		.elementor-26593
			.elementor-element.elementor-element-3268dc4a
			.elementor-icon {
			font-size: 1px;
		}
		.elementor-26593 .elementor-element.elementor-element-3268dc4a {
			width: 35px;
			max-width: 35px;
			bottom: 70px;
		}
		body:not(.rtl)
			.elementor-26593
			.elementor-element.elementor-element-3268dc4a {
			left: 5px;
		}
		body.rtl .elementor-26593 .elementor-element.elementor-element-3268dc4a {
			right: 5px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-text {
			margin-top: 10px;
			font-size: 11px;
			line-height: 1.5em;
			letter-spacing: 0.5px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-dear {
			margin-top: 10px;
			font-size: 12px;
			letter-spacing: 0.5px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-name {
			margin-top: 10px;
			font-size: 16px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .dwp-sesi {
			margin-top: 10px;
			font-size: 14px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3328422a
			.text_tambahan {
			margin-top: 25px;
			font-size: 14px;
			letter-spacing: 4px;
		}
		.elementor-26593 .elementor-element.elementor-element-3328422a .wdp-mempelai {
			margin-top: 10px;
			margin-bottom: 10px;
			font-size: 36px;
			line-height: 0.5em;
		}
		.elementor-26593
			.elementor-element.elementor-element-3328422a
			.wdp-button-wrapper {
			margin-top: 20px;
		}
		.elementor-26593
			.elementor-element.elementor-element-3328422a
			.elementor-image
			img {
			width: 35%;
		}
		}
		@media (min-width: 768px) {
		.elementor-26593 .elementor-element.elementor-element-a3b8117 {
			width: 20%;
		}
		.elementor-26593 .elementor-element.elementor-element-6e1a4a71 {
			width: 25%;
		}
		.elementor-26593 .elementor-element.elementor-element-74479eb1 {
			width: 10%;
		}
		.elementor-26593 .elementor-element.elementor-element-2db4090d {
			width: 25%;
		}
		.elementor-26593 .elementor-element.elementor-element-221c3ce7 {
			width: 20%;
		}
		.elementor-26593 .elementor-element.elementor-element-22f55591 {
			width: 35%;
		}
		.elementor-26593 .elementor-element.elementor-element-27217f0 {
			width: 29.332%;
		}
		.elementor-26593 .elementor-element.elementor-element-34424cfc {
			width: 35%;
		}
		.elementor-26593 .elementor-element.elementor-element-3853853 {
			width: 35%;
		}
		.elementor-26593 .elementor-element.elementor-element-12bd4b3a {
			width: 29.332%;
		}
		.elementor-26593 .elementor-element.elementor-element-22326808 {
			width: 35%;
		}
		}
		@media (max-width: 1024px) and (min-width: 768px) {
		.elementor-26593 .elementor-element.elementor-element-a3b8117 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-6e1a4a71 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-74479eb1 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-2db4090d {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-221c3ce7 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-22f55591 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-27217f0 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-34424cfc {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-3853853 {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-12bd4b3a {
			width: 50%;
		}
		.elementor-26593 .elementor-element.elementor-element-22326808 {
			width: 50%;
		}
		}
		@media (min-width: 1025px) {
		.elementor-26593
			.elementor-element.elementor-element-12e74c9b:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-12e74c9b
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-attachment: scroll;
		}
		.elementor-26593
			.elementor-element.elementor-element-310afb79:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-310afb79
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-attachment: fixed;
		}
		.elementor-26593
			.elementor-element.elementor-element-6e81adb2:not(.elementor-motion-effects-element-type-background),
		.elementor-26593
			.elementor-element.elementor-element-6e81adb2
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-attachment: fixed;
		}
		} /* Start Custom Fonts CSS */
		@font-face {
		font-family: "Soligant";
		font-style: normal;
		font-weight: normal;
		font-display: auto;
		src: url("<?= $asset_theme ?>fonts/Soligant.eot");
		src: url("<?= $asset_theme ?>fonts/Soligant.eot?#iefix")
			format("embedded-opentype"),
			url("<?= $asset_theme ?>fonts/Soligant.woff2")
			format("woff2"),
			url("<?= $asset_theme ?>fonts/Soligant.woff")
			format("woff"),
			url("<?= $asset_theme ?>fonts/Soligant.ttf")
			format("truetype"),
			url("<?= $asset_theme ?>fonts/Soligant.svg#Soligant")
			format("svg");
		}
		/* End Custom Fonts CSS */
	</style>

    <link rel='stylesheet' id='elementor-post-51207-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-51207.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-31584-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-31584.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-css' href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <link rel='stylesheet' id='loftloader-lite-animation-css' href='<?= $asset ?>wp-content/plugins/loftloader/assets/css/loftloader.min.css?ver=2022022501' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-v4shim-css' href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
    <style id='font-awesome-official-v4shim-inline-css' type='text/css'>
      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
        unicode-range: U+F004-F005, U+F007, U+F017, U+F022, U+F024, U+F02E, U+F03E, U+F044, U+F057-F059, U+F06E, U+F070, U+F075, U+F07B-F07C, U+F080, U+F086, U+F089, U+F094, U+F09D, U+F0A0, U+F0A4-F0A7, U+F0C5, U+F0C7-F0C8, U+F0E0, U+F0EB, U+F0F3, U+F0F8, U+F0FE, U+F111, U+F118-F11A, U+F11C, U+F133, U+F144, U+F146, U+F14A, U+F14D-F14E, U+F150-F152, U+F15B-F15C, U+F164-F165, U+F185-F186, U+F191-F192, U+F1AD, U+F1C1-F1C9, U+F1CD, U+F1D8, U+F1E3, U+F1EA, U+F1F6, U+F1F9, U+F20A, U+F247-F249, U+F24D, U+F254-F25B, U+F25D, U+F267, U+F271-F274, U+F279, U+F28B, U+F28D, U+F2B5-F2B6, U+F2B9, U+F2BB, U+F2BD, U+F2C1-F2C2, U+F2D0, U+F2D2, U+F2DC, U+F2ED, U+F328, U+F358-F35B, U+F3A5, U+F3D1, U+F410, U+F4AD;
      }
    </style>
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLora%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-wedding-css' href='<?= $asset ?>wp-content/uploads/elementor/custom-icons/wedding/css/wedding.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-unityinv-css' href='<?= $asset ?>wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-regular-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
    <script type='text/javascript' id='jquery-core-js-extra'>
      /* 
					<![CDATA[ */
      var pp = {
        // "ajax_url": "<?= $base_url ?>wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
	<script type='text/javascript' src='<?= $asset ?>plugins/jquery/js/jquery-3.3.1.js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
    <link rel="https://api.w.org/" href="<?= $asset ?>wp-json/" />
    <link rel="alternate" type="application/json" href="<?= $asset ?>wp-json/wp/v2/posts/26593" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?= $asset ?>xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?= $asset ?>wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0" />
    <link rel='shortlink' href='<?= $asset ?>?p=26593' />
    <link rel="alternate" type="application/json+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fplatinum-1%2F" />
    <link rel="alternate" type="text/xml+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fplatinum-1%2F&#038;format=xml" />
    <link rel="icon" href="<?= $asset_theme ?>icons/icon1.png" sizes="32x32" />
    <link rel="icon" href="<?= $asset_theme ?>icons/icon1.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?= $asset_theme ?>icons/icon1.png" />
    <meta name="msapplication-TileImage" content="<?= $asset_theme ?>icons/icon1.png" />
    <style>
      .pswp.pafe-lightbox-modal {
        display: none;
      }
    </style>
    <style id="loftloader-lite-custom-bg-color">
      #loftloader-wrapper .loader-section {
        background: #ffffff;
      }
    </style>
    <style id="loftloader-lite-custom-bg-opacity">
      #loftloader-wrapper .loader-section {
        opacity: 1;
      }
    </style>
    <style id="loftloader-lite-custom-loader">
      #loftloader-wrapper.pl-imgloading #loader {
        width: 70px;
      }

      #loftloader-wrapper.pl-imgloading #loader span {
        background-size: cover;
        background-image: url(<?= $asset_theme ?>logos/logo1.png);
      }
    </style>
    <style type="text/css" id="wp-custom-css">
      .elementor-section {
        overflow: hidden;
      }

      .justify-cstm .pp-timeline-card {
        text-align: justify !important;
      }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
  </head>
  <body onload="onLoad()" data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-26593 single-format-standard loftloader-lite-enabled elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-26593">
    <div id="loftloader-wrapper" class="pl-imgloading" data-show-close-time="15000" data-max-load-time="2000">
      <div class="loader-inner">
        <div id="loader">
          <div class="imgloading-container">
            <span style="background-image: url(<?= $asset_theme ?>logos/logo1.png);"></span>
          </div>
          <img width="70" height="43" data-no-lazy="1" class="skip-lazy" alt="loader image" src="<?= $asset_theme ?>logos/logo1.png">
        </div>
      </div>
      <div class="loader-section section-fade"></div>
      <div class="loader-close-button" style="display: none;">
        <span class="screen-reader-text">Close</span>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-dark-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0.49803921568627" />
            <feFuncG type="table" tableValues="0 0.49803921568627" />
            <feFuncB type="table" tableValues="0 0.49803921568627" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-red">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 0.27843137254902" />
            <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-midnight">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0" />
            <feFuncG type="table" tableValues="0 0.64705882352941" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-magenta-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.78039215686275 1" />
            <feFuncG type="table" tableValues="0 0.94901960784314" />
            <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-green">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.44705882352941 0.4" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-orange">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.098039215686275 1" />
            <feFuncG type="table" tableValues="0 0.66274509803922" />
            <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <div data-elementor-type="wp-post" data-elementor-id="26593" class="elementor elementor-26593">
      <section class="pp-bg-effects pp-bg-effects-12e74c9b elementor-section elementor-top-section elementor-element elementor-element-12e74c9b pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="12e74c9b" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="" data-line-color="" data-line-h-color="" data-part-opacity="0.5" data-rand-opacity="1" data-quantity="25" data-part-size="10" data-part-speed="2" data-part-direction="none" data-hover-effect="grab" data-hover-size="" data-id="12e74c9b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;part_rand_opacity&quot;:&quot;true&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-54f0e56b wdp-sticky-section-no" data-id="54f0e56b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-55c8fcd1 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="55c8fcd1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default">PERNIKAHAN</h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-18f2eb8e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="18f2eb8e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h1 class="elementor-heading-title elementor-size-default"><?= $groom ?> & <?= $bride ?></h1>
                </div>
              </div>
              <div class="elementor-element elementor-element-7bbf8daa elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="7bbf8daa" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-3aad3ac8 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3aad3ac8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default"><?= $wedding_fullday ?></h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-7ea4e7ec elementor-view-default elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="7ea4e7ec" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon-box.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-box-wrapper">
                    <div class="elementor-icon-box-icon">
                      <span class="elementor-icon elementor-animation-">
                        <i aria-hidden="true" class="fas fa-angle-double-up"></i>
                      </span>
                    </div>
                    <div class="elementor-icon-box-content">
                      <h3 class="elementor-icon-box-title">
                        <span> Swipe Up </span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-83e0e69 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="83e0e69" data-element_type="section" data-settings="{&quot;shape_divider_bottom&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div style="height: 143px" class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2ccfb83a wdp-sticky-section-no" data-id="2ccfb83a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-40b529 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="40b529" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7b4400cf animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7b4400cf" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4de0ec86 wdp-sticky-section-no" data-id="4de0ec86" data-element_type="column" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:55}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-6716efc4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6716efc4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-50a4b2c7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="50a4b2c7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-2f930b55 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2f930b55" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Maha Suci Allah Subhanahu wa Ta'ala yang telah menciptakan makhluk-Nya berpasang-pasangan. Ya Allah, perkenankanlah dan Ridhoilah Pernikahan Kami.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-50a70519 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="50a70519" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6c5f118e elementor-reverse-mobile animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="6c5f118e" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-a3b8117 animated-slow wdp-sticky-section-no elementor-invisible" data-id="a3b8117" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;,&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-312f9a8b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="312f9a8b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default"><?= $groom_firstname ?> <br> <?= $groom_middlename ?> <?= $groom_lastname ?> </h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6bd6ab1a elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6bd6ab1a" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4f671a99 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="4f671a99" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $groom_child_position ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-19d66ff6 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="19d66ff6" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak <?= $groom_father ?> <br> & <br> Ibu <?= $groom_mother ?> </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-47514af6 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="47514af6" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="<?= $groom_ig ?>" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-6e1a4a71 animated-slow wdp-sticky-section-no elementor-invisible" data-id="6e1a4a71" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-bc781e8 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="bc781e8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img src="<?= $asset_theme ?>photos/CPP.png" title="CPP" alt="CPP" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-74479eb1 animated-slow wdp-sticky-section-no elementor-invisible" data-id="74479eb1" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation_delay&quot;:500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-4c123b15 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4c123b15" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:51}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">&</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-2db4090d animated-slow wdp-sticky-section-no elementor-invisible" data-id="2db4090d" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-7f06c0be animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="7f06c0be" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img src="<?= $asset_theme ?>photos/CPW.png" title="CPW" alt="CPW" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-221c3ce7 animated-slow wdp-sticky-section-no elementor-invisible" data-id="221c3ce7" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;,&quot;animation_mobile&quot;:&quot;fadeInRight&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-75355ed8 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="75355ed8" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default"><?= $bride_firstname ?> <br> <?= $bride_middlename ?> <?= $bride_lastname ?> </h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-43167651 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="43167651" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-70115f1f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="70115f1f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $bride_child_position ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-319fa485 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="319fa485" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak <?= $bride_father ?> <br> & <br> Ibu <?= $bride_mother ?> </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-12f38211 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="12f38211" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="<?= $bride_ig ?>" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-310afb79 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="310afb79" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-fan&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-33f43cc5 wdp-sticky-section-no" data-id="33f43cc5" data-element_type="column" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:56}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-34e30737 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="34e30737" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hitung Mundur</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-4636262a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4636262a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-5329bec8 bdt-countdown--align-center bdt-countdown--label-block wdp-sticky-section-no elementor-widget elementor-widget-bdt-countdown" data-id="5329bec8" data-element_type_="widget" data-widget_type="bdt-countdown.default">
                <div class="elementor-widget-container">
                  <div class="bdt-countdown-wrapper bdt-countdown-skin-default" data-settings="{&quot;id&quot;:&quot;#bdt-countdown-5329bec8&quot;,&quot;msgId&quot;:&quot;#bdt-countdown-msg-5329bec8&quot;,&quot;adminAjaxUrl&quot;:&quot;<?= $base_url ?>wp-admin\/admin-ajax.php&quot;,&quot;endActionType&quot;:&quot;none&quot;,&quot;redirectUrl&quot;:null,&quot;redirectDelay&quot;:1000,&quot;finalTime&quot;:&quot;2022-12-30T17:00:00+00:00&quot;,&quot;wpCurrentTime&quot;:1655271780,&quot;endTime&quot;:1672419600,&quot;loopHours&quot;:false,&quot;isLogged&quot;:false,&quot;couponTrickyId&quot;:&quot;bdt-sf-5329bec8&quot;,&quot;triggerId&quot;:false}">
                    <div id="bdt-countdown-5329bec8-timer" class="bdt-grid bdt-grid-small bdt-child-width-1-4 bdt-child-width-1-2@s bdt-child-width-1-4@m countdown" data-bdt-countdown_="date: 2022-12-30T17:00:00+00:00" data-bdt-grid="" style="x">
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-days-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-days bdt-text-center">
							<span class="days">00</span>
						  </span>
                          <span class="bdt-countdown-label bdt-text-center">Hari</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-hours-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-hours bdt-text-center">
							<span class="hours">00</span>
						  </span>
                          <span class="bdt-countdown-label bdt-text-center">Jam</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-minutes-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-minutes bdt-text-center">
							<span class="minutes">00</span>
						  </span>
                          <span class="bdt-countdown-label bdt-text-center">Menit</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-seconds-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-seconds bdt-text-center">
							<span class="seconds">00</span>
						  </span>
                          <span class="bdt-countdown-label bdt-text-center">Detik</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-580eaa66 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="580eaa66" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default"><?= $verse_value ?></p>
                </div>
              </div>
              <div class="elementor-element elementor-element-43cea9c0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="43cea9c0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default"><?= $verse_name ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-f1b747a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f1b747a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-14204cba wdp-sticky-section-no" data-id="14204cba" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-5b48a037 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5b48a037" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Waktu & Tempat</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-26929aec wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="26929aec" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pernikahan</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-4fb69753 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4fb69753" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">“Cinta tidak mengenal hambatan. Ia melompati rintangan, melompati pagar, menembus tembok untuk sampai pada tujuannya dengan penuh harapan.”</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-681780f8 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="681780f8" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-54115b94 elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="54115b94" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-1f92964b animated-slow wdp-sticky-section-no" data-id="1f92964b" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-27710c7b elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="27710c7b" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconwedding-photography"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-143de12e wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="143de12e" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5beb6017 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5beb6017" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Akad</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-30681bb1 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="30681bb1" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-16a3b19e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="16a3b19e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_month ?></p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-39ffc1b8 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="39ffc1b8" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-22f55591 wdp-sticky-section-no" data-id="22f55591" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-7b7be058 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7b7be058" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default"><?= $akad_day ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-27217f0 wdp-sticky-section-no" data-id="27217f0" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-75cc5e4c wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="75cc5e4c" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="<?= $akad_date_show_duration ?>" data-to-value="<?= $akad_date ?>" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-34424cfc wdp-sticky-section-no" data-id="34424cfc" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-180d2b7f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="180d2b7f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default"><?= $akad_year ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-10782627 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="10782627" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_hour ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-44595f0 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="44595f0" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6e3ee36 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6e3ee36" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-551fb491 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="551fb491" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_venue ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-565b7de1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="565b7de1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_venue_address ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-93fd04a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="93fd04a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $akad_venue_map ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5037a4a7 animated-slow wdp-sticky-section-no" data-id="5037a4a7" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200,&quot;animation_tablet&quot;:&quot;none&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6efc2ffb elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6efc2ffb" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconwedding-dinner"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-260cbace wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="260cbace" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2e037502 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2e037502" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Resepsi</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7fa0580d elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="7fa0580d" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-75429b1f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="75429b1f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_month ?></p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-33f517de elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="33f517de" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3853853 wdp-sticky-section-no" data-id="3853853" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-d0ed149 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d0ed149" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default"><?= $wedding_day ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-12bd4b3a wdp-sticky-section-no" data-id="12bd4b3a" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-1272b3fa wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="1272b3fa" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="<?= $wedding_date_show_duration ?>" data-to-value="<?= $wedding_date ?>" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-22326808 wdp-sticky-section-no" data-id="22326808" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-adef9b7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="adef9b7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default"><?= $wedding_year ?></p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-353be9d9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="353be9d9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_hour ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2628856c elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="2628856c" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6f532b6 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6f532b6" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-35e15138 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="35e15138" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_venue ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6caffb10 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6caffb10" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_venue_address ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2d9f4d2e elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="2d9f4d2e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $wedding_venue_map ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-68c6b0fd wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="68c6b0fd" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-32a2ddef elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="32a2ddef" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4df13f9b wdp-sticky-section-no" data-id="4df13f9b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-background-overlay"></div>
                      <div class="elementor-element elementor-element-a568590 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="a568590" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1005c20 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1005c20" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kami Akan <br> Melangsungkan </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3daa5702 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3daa5702" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Live Streaming </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-649efca4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="649efca4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live.</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2a486249 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="2a486249" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $youtube_streaming ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fab fa-youtube"></i>
                                </span>
                                <span class="elementor-button-text">Join Streaming</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7176eedd elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7176eedd" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-45cf3b03 wdp-sticky-section-no" data-id="45cf3b03" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-35d9f07 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="35d9f07" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Sebuah Kisah </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-36d58e2b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="36d58e2b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Perjalanan Kami</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-43d07d60 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="43d07d60" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Aku tidak tahu dimana ujung perjalanan ini, aku tidak bisa menjanjikan apapun. Tapi, selama aku mampu, mimpi-mimpi kita adalah prioritas."</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-36fdda1a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="36fdda1a" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6958710f animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6958710f" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-7d755972 animated-slow wdp-sticky-section-no" data-id="7d755972" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-fe8dda1 animated-slow justify-cstm wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="fe8dda1" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:200,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
                        <div class="elementor-widget-container">
                          <div class="pp-timeline-wrapper">
                            <div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
                              <div class="pp-timeline-connector-wrap">
                                <div class="pp-timeline-connector">
                                  <div class="pp-timeline-connector-inner"></div>
                                </div>
                              </div>
                              <div class="pp-timeline-items">
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-3ddf251">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $first_meet_date ?></div>
                                        <h2 class="pp-timeline-card-title"> Pertama Bertemu </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $first_meet_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $first_meet_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-48bb742">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $having_a_relationship_date ?></div>
                                        <h2 class="pp-timeline-card-title"> Menjalin Hubungan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $having_a_relationship_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $having_a_relationship_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-9e7595d">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $engagement_date ?></div>
                                        <h2 class="pp-timeline-card-title"> Bertunangan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $engagement_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $engagement_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-255189e">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $married_date ?></div>
                                        <h2 class="pp-timeline-card-title"> Menikah </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $married_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $married_date ?></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-256eb13a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="256eb13a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-13dbe0fa wdp-sticky-section-no" data-id="13dbe0fa" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-50614b85 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="50614b85" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Momen</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-51676832 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="51676832" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Bahagia Kami </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-1600bf1 animated-fast elementor-aspect-ratio-169 wdp-sticky-section-no elementor-widget elementor-widget-video" data-id="1600bf1" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;<?= $youtube_prewedding ?>&quot;,&quot;yt_privacy&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;play_on_mobile&quot;:&quot;yes&quot;,&quot;mute&quot;:&quot;yes&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default">
                <div class="elementor-widget-container">
                  <div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                    <div class="elementor-video"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-322f613b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="322f613b" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-353417a5 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="353417a5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Kamu adalah sahabat dan kekasihku, dan aku tidak tahu sisi mana darimu yang paling aku nikmati. Aku menghargai setiap sisi, sama seperti aku telah menghargai hidup kita bersama."</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-7f7ec1b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7f7ec1b" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
			  <div class="elementor-element elementor-element-36efaaa2 wdp-sticky-section-no elementor-widget elementor-widget-gallery" data-id="36efaaa2" data-element_type="widget" data-settings="{&quot;gallery_layout&quot;:&quot;justified&quot;,&quot;ideal_row_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:300,&quot;sizes&quot;:[]},&quot;ideal_row_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:200,&quot;sizes&quot;:[]},&quot;gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;gap_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;ideal_row_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;gap_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;link_to&quot;:&quot;file&quot;,&quot;overlay_background&quot;:&quot;yes&quot;,&quot;content_hover_animation&quot;:&quot;fade-in&quot;}" data-widget_type="gallery.default">
				<div class="elementor-widget-container">
					<div class="elementor-gallery__container e-gallery-container e-gallery-justified e-gallery--ltr" style="--hgap:5px; --vgap:5px; --animation-duration:350ms; --container-aspect-ratio:0.976581;">
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content e-gallery-first-row-item" href="<?= $asset_theme ?>photos/Galeri-1.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.160256; --gap-count:3; --item-height:24.2902%; --item-start:0; --item-row-index:0; --item-top:0%; --row:0;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-1.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-1.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-2.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0yLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.339744; --gap-count:3; --item-height:24.2902%; --item-start:0.160256; --item-row-index:1; --item-top:0%; --row:0;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-2.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-2.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-3-scaled.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0zLXNjYWxlZC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.339744; --gap-count:3; --item-height:24.2902%; --item-start:0.5; --item-row-index:2; --item-top:0%; --row:0;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-3.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-3.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS00LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.160256; --gap-count:3; --item-height:24.2902%; --item-start:0.839744; --item-row-index:3; --item-top:0%; --row:0;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-4.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-4.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content e-gallery-first-row-item" href="<?= $asset_theme ?>photos/Galeri-5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS01LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.160256; --gap-count:3; --item-height:24.2902%; --item-start:0; --item-row-index:0; --item-top:24.2902%; --row:1;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-5.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-5.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-6.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS02LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.339744; --gap-count:3; --item-height:24.2902%; --item-start:0.160256; --item-row-index:1; --item-top:24.2902%; --row:1;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-6.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-6.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS03LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.339744; --gap-count:3; --item-height:24.2902%; --item-start:0.5; --item-row-index:2; --item-top:24.2902%; --row:1;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-7.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-7.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS04LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.160256; --gap-count:3; --item-height:24.2902%; --item-start:0.839744; --item-row-index:3; --item-top:24.2902%; --row:1;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-8.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-8.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content e-gallery-first-row-item" href="<?= $asset_theme ?>photos/Galeri-9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NjksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS05LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0zNmVmYWFhMiJ9" style="--item-width:0.339744; --gap-count:3; --item-height:24.3178%; --item-start:0; --item-row-index:0; --item-top:48.5803%; --row:2;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-9.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-9.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<!-- <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xMC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.339744; --gap-count:3; --item-height:24.3178%; --item-start:0.339744; --item-row-index:1; --item-top:48.5803%; --row:2;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-10.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-10.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xMS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.160256; --gap-count:3; --item-height:24.3178%; --item-start:0.679487; --item-row-index:2; --item-top:48.5803%; --row:2;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-11.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-11.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xMi5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.160256; --gap-count:3; --item-height:24.3178%; --item-start:0.839744; --item-row-index:3; --item-top:48.5803%; --row:2;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-12.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-12.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content e-gallery-first-row-item" href="<?= $asset_theme ?>photos/Galeri-13.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xMy5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.169492; --gap-count:2; --item-height:25.8002%; --item-start:0; --item-row-index:0; --item-top:72.8981%; --row:3;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-13.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-13.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-14.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xNC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.169492; --gap-count:2; --item-height:25.8002%; --item-start:0.169492; --item-row-index:1; --item-top:72.8981%; --row:3;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-14.jpg" data-width="683" data-height="1024" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-14.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a>
					<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Galeri-15.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-36efaaa2" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzI0NzYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktUGxhdGludW0tMS1Gb3RvLUdhbGVyaS0xNS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMzZlZmFhYTIifQ%3D%3D" style="--item-width:0.359322; --gap-count:2; --item-height:25.8002%; --item-start:0.338983; --item-row-index:2; --item-top:72.8981%; --row:3;">
						<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Galeri-15.jpg" data-width="800" data-height="566" alt="" style="background-image: url(&quot;<?= $asset_theme ?>photos/Galeri-15.jpg&quot;);"></div>
						<div class="elementor-gallery-item__overlay"></div>
					</a> -->
					</div>
				</div>
				</div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-5aa734ff elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5aa734ff" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4bd181e4 wdp-sticky-section-no" data-id="4bd181e4" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-efe2d53 wdp-sticky-section-no" data-id="efe2d53" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-64a7b8b6 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="64a7b8b6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Photo by</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6785a56d wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="6785a56d" data-element_type="widget" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="800" height="280" src="<?= $asset_theme ?>vendors/vendor1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>vendors/vendor1.png 1024w, <?= $asset_theme ?>vendors/vendor1.png 300w, <?= $asset_theme ?>vendors/vendor1.png 768w, <?= $asset_theme ?>vendors/vendor1.png 1516w" sizes="(max-width: 800px) 100vw, 800px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3d0d2424 elementor-shape-circle elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="3d0d2424" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="<?= $ig_photo_by ?>" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-70a19035 wdp-sticky-section-no" data-id="70a19035" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7a198c20 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7a198c20" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-fan&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-43a30275 wdp-sticky-section-no" data-id="43a30275" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-aa11137 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="aa11137" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-1f6345c2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1f6345c2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat, <br>acara ini menerapkan Protokol Kesehatan, sesuai dengan peraturan & rekomendasi pemerintah. </h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-38583d82 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="38583d82" data-element_type="widget" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-15b996e7 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="15b996e7" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d44538f animated-slow wdp-sticky-section-no elementor-invisible" data-id="d44538f" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-c7a6987 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="c7a6987" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Gunakan Masker</h3>
                              <p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-65e198e7 animated-slow wdp-sticky-section-no elementor-invisible" data-id="65e198e7" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-edff565 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="edff565" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Cuci Tangan</h3>
                              <p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-680dc18c elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="680dc18c" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-5fc38f52 animated-slow wdp-sticky-section-no elementor-invisible" data-id="5fc38f52" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-2fce9d1f elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="2fce9d1f" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Jaga Jarak </h3>
                              <p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-cffda27 animated-slow wdp-sticky-section-no elementor-invisible" data-id="cffda27" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6f88bba4 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="6f88bba4" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Tidak Berjabat Tangan</h3>
                              <p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>


      <section class="elementor-section elementor-top-section elementor-element elementor-element-2eb241c3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2eb241c3" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-47f180e2 wdp-sticky-section-no" data-id="47f180e2" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-63860774 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="63860774" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Doa & Ucapan untuk</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-670b6a3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="670b6a3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan Mempelai </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-2f770391 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="2f770391" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-786700a0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="786700a0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Merupakan suatu kehormatan dan kebahagiaan bagi Kami apabila Bapak/ Ibu/ Saudara/ i berkenan hadir untuk memberikan doa restu kepada kedua mempelai.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-7cab197e wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7cab197e" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-373d0322 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="373d0322" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-55d3263 animated-slow wdp-sticky-section-no elementor-invisible" data-id="55d3263" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500,&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-70abb54 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="70abb54" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-cf89f20 wdp-sticky-section-no" data-id="cf89f20" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-9cbd728 wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="9cbd728" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
                                <div class="elementor-widget-container">
                                  <style>
                                    .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],
                                    .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,
                                    .wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,
                                    .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit] {
                                      height: auto !important;
                                    }

                                    .halooo {
                                      background-color: red !important;
                                      color: #fff !important;
                                    }

                                    .wdp-wrapper .wdp-wrap-link {
                                      text-align: center;
                                      padding: 18px;
                                      margin-top: 15px;
                                    }

                                    .wdp-wrapper .wdp-wrap-form {
                                      border-top: 0px;
                                    }

                                    .konfirmasi-kehadiran {
                                      display: flex;
                                      justify-content: center;
                                      flex-direction: row;
                                      padding: 10px;
                                      margin-top: 10px;
                                    }

                                    .jenis-konfirmasi>i {
                                      margin: 0px 5px;
                                    }

                                    .jenis-konfirmasi {
                                      padding: 5px 10px;
                                      background: red;
                                      border-radius: 0.5em;
                                      color: #fff;
                                      margin: 7px;
                                    }
                                  </style>
                                  <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                                    <div class='wdp-wrap-link'>
                                      <a id='wdp-link-26593' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=26593&amp;comments=2&amp;get=0&amp;order=DESC' title='2 Ucapan'></a>
                                      <div class='konfirmasi-kehadiran elementor-screen-only'>
                                        <div class='jenis-konfirmasi cswd-hadir'>
                                          <i class='fas fa-check'></i> 1 Hadir
                                        </div>
                                        <div class='jenis-konfirmasi cswd-tidak-hadir'>
                                          <i class='fas fa-times'></i> 1 Tidak Hadir
                                        </div>
                                      </div>
                                    </div>
                                    <!-- <div id='wdp-wrap-commnent-26593' class='wdp-wrap-comments' style='display:none;'>
                                      <div id='wdp-wrap-form-26593' class='wdp-wrap-form wdp-clearfix'>
                                        <div id='wdp-container-form-26593' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                          <div id='respond-26593' class='respond wdp-clearfix'>
                                            <form action='<?= $asset ?>wp-comments-post.php' method='post' id='commentform-26593'>
                                              <p class="comment-form-author wdp-field-1">
                                                <input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                                <span class="wdp-required">*</span>
                                                <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                              </p>
                                              <div class="wdp-wrap-textarea">
                                                <textarea id="wdp-textarea-26593" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                                <span class="wdp-required">*</span>
                                                <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                              </div>
                                              <div class="wdp-wrap-select">
                                                <select class="waci_comment wdp-select" name="konfirmasi">
                                                  <option value="" disabled selected>Konfirmasi Kehadiran</option>> </option>
                                                  <option value="Hadir">Hadir</option>
                                                  <option value="Tidak Hadir">Tidak Hadir</option>
                                                </select>
                                                <span class="wdp-required">*</span>
                                                <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                              </div>
                                              <div class='wdp-wrap-submit wdp-clearfix'>
                                                <p class='form-submit'>
                                                  <span class="wdp-hide">Do not change these fields following</span>
                                                  <input type="text" class="wdp-hide" name="name" value="username">
                                                  <input type="text" class="wdp-hide" name="nombre" value="">
                                                  <input type="text" class="wdp-hide" name="form-wdp" value="">
                                                  <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                                  <input name='submit' id='submit-26593' value='Kirim' type='submit' />
                                                  <input type='hidden' name='commentpress' value='true' />
                                                  <input type='hidden' name='comment_post_ID' value='26593' id='comment_post_ID' />
                                                  <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                </p>
                                              </div>
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                      <div id='wdp-comment-status-26593' class='wdp-comment-status'></div>
                                      <ul id='wdp-container-comment-26593' class='wdp-container-comments wdp-order-DESC  wdp-has-2-comments wdp-multiple-comments' data-order='DESC'></ul>
                                      <div class='wdp-holder-26593 wdp-holder'></div>
                                    </div> -->
                                    <div id='wdp-wrap-commnent-26593_' class='wdp-wrap-comments' style='display:block;'>
                                        <div id='wdp-wrap-form-26593' class='wdp-wrap-form wdp-clearfix'>
                                            <div id='wdp-container-form-26593' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                                <div id='respond-26593' class='respond wdp-clearfix'>
                                                  <form action='https://unityinvitation.com/save-sap.php' method='post' id='commentform-26593'>
                                                    <p class="comment-form-author wdp-field-1">
                                                      <input id="name" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                                      <span class="wdp-required">*</span>
                                                      <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                                    </p>
                                                    <div class="wdp-wrap-textarea">
                                                      <textarea id="description" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                                      <span class="wdp-required">*</span>
                                                      <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                                    </div>
                                                    <div class="wdp-wrap-select">
                                                      <select class="waci_comment wdp-select" id="confirmation_of_attendance" name="konfirmasi">
                                                        <option value="" disabled selected>Konfirmasi Kehadiran</option>></option>
                                                        <option value="Hadir">Hadir</option>
                                                        <option value="Tidak Hadir">Tidak Hadir</option>
                                                      </select>
                                                      <span class="wdp-required">*</span>
                                                      <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                                    </div>
                                                    <div class='wdp-wrap-submit wdp-clearfix'>
                                                      <p class='form-submit'>
                                                        <span class="wdp-hide">Do not change these fields following</span>
                                                        <input type="text" class="wdp-hide" name="name" value="username">
                                                        <input type="text" class="wdp-hide" name="nombre" value="">
                                                        <input type="text" class="wdp-hide" name="form-wdp" value="">
                                                        <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                                        <input name='submit' value="Kirim" id='submit-26593' type='button' onclick="saveSAP()" style="background-color: #A99F9E; color: #ffffff; border-color: #A99F9E; border-radius: 12px; padding: 5px 25px 5px 25px; font-size: 14px;" onMouseOver="this.style.background='#ffffff';this.style.color='#A99F9E';" onMouseOut="this.style.background='#A99F9E';this.style.color='#ffffff';">
                                                        <input type='hidden' name='commentpress' value='true' />
                                                        <input type='hidden' name='comment_post_ID' value='26593' id='comment_post_ID' />
                                                        <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                      </p>
                                                    </div>
                                                  </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div id='wdp-comment-status-26593'  class='wdp-comment-status' style="display: none">
                                          <p class="wdp-ajax-success">Terima kasih atas ucapan &amp; doanya!</p>
                                        </div>
                                        <ul id="wdp-container-comment-26593" class="wdp-container-comments wdp-order-DESC  wdp-has-6-comments wdp-multiple-comments" data-order="DESC" style="display: block;">
                                          <?php foreach ($data_sap as $k => $v) { ?>
                                            <li class="comment even thread-even depth-1 wdp-item-comment animated fadeIn" id="wdp-item-comment-14904" data-likes="0">
                                            <div id="wdp-comment-14904" class="wdp-comment wdp-clearfix">
                                              <div class="wdp-comment-avatar">
                                                <img src="https://unityinvitation.com/wp-content/uploads/2022/03/Icon-Unity-Comment.png">
                                              </div><!--.wdp-comment-avatar-->
                                              <div class="wdp-comment-content">
                                                <div class="wdp-comment-info">
                                                  <a class="wdp-commenter-name" title="<?= $v['name'] ?>"><?= $v['name'] ?></a>
                                                  <span class="wdp-post-author"><i class="fas fa-check-circle"></i> <?= $v['confirmation_of_attendance'] ?></span>
                                                  <br>
                                                  <span class="wdp-comment-time">
                                                  <i class="far fa-clock"></i>
                                                <?= $v['human_created_at'] ?>
                                              </span>
                                                </div><!--.wdp-comment-info-->
                                                <div class="wdp-comment-text">
                                                  <p><?= $v['description'] ?></p>
                                                </div><!--.wdp-comment-text-->
                                                <div class="wdp-comment-actions" style="display: block;">
                                                </div><!--.wdp-comment-actions-->
                                              </div><!--.wdp-comment-content-->
                                            </div><!--.wdp-comment-->
                                            <!--</li>-->
                                            </li><!-- #comment-## -->
                                          <?php } ?>
                                        </ul>
                                        <div id="page" class="wdp-holder-26593 wdp-holder" style="display: block;">
                                          <a class="jp-previous">← Sebelumnya</a>
                                          <a class="">1</a>
                                          <!-- <span class="jp-hidden">...</span> -->
                                          <a class="jp-current">2</a>
                                          <a class="jp-next jp-disabled">Selanjutnya →</a>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>

	  
      <section class="elementor-section elementor-top-section elementor-element elementor-element-a0053f1 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="a0053f1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-fan&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-d3595ad wdp-sticky-section-no" data-id="d3595ad" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-0f9d978 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="0f9d978" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Semoga Allah memberkahimu dan memberkahi apa yang menjadi tanggung jawabmu, serta menyatukan kalian berdua dalam kebaikan." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-cc42991 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cc42991" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">(HR. Ahmad, at-Tirmidzi, an-Nasa'i, <br> Abu Dawud, dan Ibnu Majah) </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-dbb1c5e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dbb1c5e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9c8873a wdp-sticky-section-no" data-id="9c8873a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-f3f5424 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f3f5424" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-01d29e9 animated-slow wdp-sticky-section-no" data-id="01d29e9" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_tablet&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-8498600 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8498600" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-b2ae688 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b2ae688" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hadiah Pernikahan</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6f00f7b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6f00f7b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Doa restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah. Tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentu semakin melengkapi kebahagiaan kami.</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4b02472 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="4b02472" data-element_type="widget" data-widget_type="html.default">
                        <div class="elementor-widget-container">
                          <script>
                            document.addEventListener('DOMContentLoaded', function() {
                              jQuery(function($) {
                                $('.clicktoshow').each(function(i) {
                                  $(this).click(function() {
                                    $('.showclick').eq(i).toggle();
                                    $('.clicktoshow');
                                    $('.showclick2').eq(i).hide();
                                  });
                                });
                              });
                            });
                          </script>
                          <style>
                            .clicktoshow {
                              cursor: pointer;
                            }

                            .showclick {
                              display: none;
                            }
                          </style>
                          <script>
                            document.addEventListener('DOMContentLoaded', function() {
                              jQuery(function($) {
                                $('.clicktoshow2').each(function(i) {
                                  $(this).click(function() {
                                    $('.showclick2').eq(i).toggle();
                                    $('.clicktoshow2');
                                  });
                                });
                              });
                            });
                          </script>
                          <style>
                            .clicktoshow2 {
                              cursor: pointer;
                            }

                            .showclick2 {
                              display: none;
                            }
                          </style>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-a2eca50 elementor-align-center clicktoshow wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="a2eca50" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                                </span>
                                <span class="elementor-button-text">Kirim Hadiah</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-bcc0bd3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="bcc0bd3" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-2f74342 showclick animated-slow elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="2f74342" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-30baf7e wdp-sticky-section-no" data-id="30baf7e" data-element_type="column">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-f066c95 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f066c95" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Amplop</p>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-f676723 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="f676723" data-element_type="widget" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-divider">
                                    <span class="elementor-divider-separator"></span>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-8eb8230 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="8eb8230" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="768" height="243" src="<?= $asset_theme ?>banks/bank-mandiri.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>banks/bank-mandiri.png 768w, <?= $asset_theme ?>banks/bank-mandiri.png 300w, <?= $asset_theme ?>banks/bank-mandiri.png 1024w, <?= $asset_theme ?>banks/bank-mandiri.png 1536w, <?= $asset_theme ?>banks/bank-mandiri.png 1572w" sizes="(max-width: 768px) 100vw, 768px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-5284c3d elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="5284c3d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-image img"></div>
                                  <div class="head-title">a/n <?= $transfer_gift_user ?></div>
                                  <div class="elementor-button-wrapper">
                                    <div class="copy-content spancontent"><?= $transfer_gift_rekening ?></div>
                                    <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                      <div class="elementor-button-content-wrapper">
                                        <span class="elementor-button-icon elementor-align-icon-left">
                                          <i aria-hidden="true" class="far fa-copy"></i>
                                        </span>
                                        <span class="elementor-button-text">Salin</span>
                                      </div>
                                    </a>
                                  </div>
                                  <style type="text/css">
                                    .spancontent {
                                      padding-bottom: 20px;
                                    }

                                    .copy-content {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }

                                    .head-title {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }
                                  </style>
                                  <script>
                                    function copyText(el) {
                                      var content = jQuery(el).siblings('div.copy-content').html()
                                      var temp = jQuery(" < textarea > ");
                                        jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                        var text = jQuery(el).html()
                                        jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                        var interval = setInterval(function() {
                                          counter++;
                                          if (counter == 2) {
                                            jQuery(el).html(text)
                                            Interval(interval);
                                          }
                                        }, 5000);
                                      }
                                  </script>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-be897a6 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="be897a6" data-element_type="widget" data-widget_type="spacer.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-spacer">
                                    <div class="elementor-spacer-inner"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-8262a89 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8262a89" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Kado</p>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-6bd5cc7 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6bd5cc7" data-element_type="widget" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-divider">
                                    <span class="elementor-divider-separator"></span>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-879131a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="879131a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-image img"></div>
                                  <div class="head-title">a/n <?= $send_gift_user ?></div>
                                  <div class="elementor-button-wrapper">
                                    <div class="copy-content spancontent"><?= $send_gift_address ?></div>
                                    <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                      <div class="elementor-button-content-wrapper">
                                        <span class="elementor-button-icon elementor-align-icon-left">
                                          <i aria-hidden="true" class="far fa-copy"></i>
                                        </span>
                                        <span class="elementor-button-text">Salin</span>
                                      </div>
                                    </a>
                                  </div>
                                  <style type="text/css">
                                    .spancontent {
                                      padding-bottom: 20px;
                                    }

                                    .copy-content {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }

                                    .head-title {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }
                                  </style>
                                  <script>
                                    function copyText(el) {
                                      var content = jQuery(el).siblings('div.copy-content').html()
                                      var temp = jQuery(" < textarea > ");
                                        jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                        var text = jQuery(el).html()
                                        jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                        var interval = setInterval(function() {
                                          counter++;
                                          if (counter == 2) {
                                            jQuery(el).html(text)
                                            Interval(interval);
                                          }
                                        }, 5000);
                                      }
                                  </script>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-6e81adb2 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6e81adb2" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-fan&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 19.6" preserveAspectRatio="none">
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 18.8 141.8 4.1 283.5 18.8 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 12.6 141.8 4 283.5 12.6 283.5 0z" />
            <path class="elementor-shape-fill" style="opacity:0.33" d="M0 0L0 6.4 141.8 4 283.5 6.4 283.5 0z" />
            <path class="elementor-shape-fill" d="M0 0L0 1.2 141.8 4 283.5 1.2 283.5 0z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3672441a wdp-sticky-section-no" data-id="3672441a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-3720215f elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="3720215f" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-43eaec95 animated-fast wdp-sticky-section-no elementor-invisible" data-id="43eaec95" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6d7f848b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6d7f848b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Terima Kasih</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5c7c24a2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5c7c24a2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h1 class="elementor-heading-title elementor-size-default"><?= $groom ?> & <?= $bride ?></h1>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-53ca8e70 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="53ca8e70" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Keluarga Besar</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7174f094 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7174f094" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Bapak <?= $groom_father ?> & Ibu <?= $groom_mother ?> <br> Bapak <?= $bride_father ?> & Ibu <?= $bride_mother ?> </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-749e59fa wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="749e59fa" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>


				<br><br><br><br><br><br>
				<div class="elementor-element elementor-element-7174f094 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="adb4295" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
					<div class="elementor-widget-container">
						<p class="elementor-heading-title elementor-size-default">
							Made With ♥ by <a href="<?= $base_url ?>">Ara Invitation</a>
						</p>
					</div>
				</div>


              <!-- <section class="elementor-section elementor-inner-section elementor-element elementor-element-2158efcc animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="2158efcc" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-1191ef64 wdp-sticky-section-no" data-id="1191ef64" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-3c83db5a wdp-sticky-section-no" data-id="3c83db5a" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-40f9b29b wdp-sticky-section-no" data-id="40f9b29b" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-1469ed45 animated-slow wdp-sticky-section-no elementor-invisible" data-id="1469ed45" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-420b0a95 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="420b0a95" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="<?= $asset ?>" target="_blank">
                            <img width="768" height="589" src="<?= $asset ?>wp-content/uploads/2021/05/LogosWhite-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/05/LogosWhite-768x589.png 768w, <?= $asset ?>wp-content/uploads/2021/05/LogosWhite-300x230.png 300w, <?= $asset ?>wp-content/uploads/2021/05/LogosWhite.png 779w" sizes="(max-width: 768px) 100vw, 768px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-76eb38 elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="76eb38" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20Digital%20Unity%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Whatsapp</span>
                                <i class="fab fa-whatsapp"></i>
                              </a>
                            </span>
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/unity.invitation/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-72db2ad4 animated-slow wdp-sticky-section-no elementor-invisible" data-id="72db2ad4" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-42e268c9 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="42e268c9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="<?= $asset ?>" target="_blank">
                            <img width="1001" height="1001" src="<?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png" class="attachment-full size-full" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png 1001w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-150x150.png 150w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-768x768.png 768w" sizes="(max-width: 1001px) 100vw, 1001px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-eacbda6 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="eacbda6" data-element_type="widget" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">© 2021 Unity Invitation, <br>All Rights Reserved </h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-34515251 wdp-sticky-section-no" data-id="34515251" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-1993dcaa wdp-sticky-section-no" data-id="1993dcaa" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-47d70b26 wdp-sticky-section-no" data-id="47d70b26" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section> -->


              <div class="elementor-element elementor-element-3268dc4a elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="3268dc4a" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
                <div class="elementor-widget-container">
                  <script>
                    var settingAutoplay = 'yes';
                    window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
                  </script>
                  <div id="audio-container" class="audio-box">
                    <style>
                      .play_cswd_audio {
                        animation-name: putar;
                        animation-duration: 5000ms;
                        animation-iteration-count: infinite;
                        animation-timing-function: linear;
                      }

                      @keyframes putar {
                        from {
                          transform: rotate(0deg);
                        }

                        to {
                          transform: rotate(360deg);
                        }
                      }
                    </style>
                    <audio id="song_" loop>
                      <source src="<?= $asset_theme ?>backsound/<?= $backsound ?>" type="audio/mp3">
                    </audio>
                    <div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="<?= $asset_theme ?>icons/icon-music-1.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
                      </div>
                    </div>
                    <div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="<?= $asset_theme ?>icons/icon-music-1.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
                      </div>
                    </div>
                  </div>
                  <script>
						// jQuery("document").ready(function (n) {
						// 	var e = window.settingAutoplay;
						// 	e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
						// 		n("#audio-container").click(function (u) {
						// 			e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
						// 		});
						// });

						function onLoad() {
							// Disable Button
							// document.getElementById("open-invitation").disabled = false;
							// setTimeout(function() {
							// 	document.getElementById("open-invitation").innerHTML = '3';
							// 	setTimeout(function() {
							// 		document.getElementById("open-invitation").innerHTML = '2';
							// 		setTimeout(function() {
							// 			document.getElementById("open-invitation").innerHTML = '1';
							// 			setTimeout(function() {
							// 				document.getElementById("open-invitation").innerHTML = 'Buka Undangan';
							// 			}, 500);
							// 		}, 500);
							// 	}, 500);
							// }, 500);

							// Setting Click
							document.getElementById("unmute-sound").onclick = function() {
							document.getElementById("unmute-sound").style.display = 'none';
							document.getElementById("mute-sound").style.display = 'block';
							document.getElementById("song_").play();
							};
							document.getElementById("mute-sound").onclick = function() {
							document.getElementById("mute-sound").style.display = 'none';
							document.getElementById("unmute-sound").style.display = 'block';
							document.getElementById("song_").pause();
							};
							
						}
					</script>
                </div>
              </div>
              <div class="elementor-element elementor-element-3328422a wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="3328422a" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
                <div class="elementor-widget-container">
                  <style>
                    .elementor-image>img {
                      display: initial !important;
                    }
                  </style>
                  <div class="modalx animated " data-sampul='<?= $asset_theme ?>photos/Cover1.jpg'>
                    <div class="overlayy"></div>
                    <div class="content-modalx">
                      <div class="info_modalx">
                        <div class="elementor-image img">
                          <img src="<?= $asset_theme ?>photos/Cover2.png" title="Cover2" alt="Cover2" />
                        </div>
                        <div class="text_tambahan">PERNIKAHAN</div>
                        <div class="wdp-mempelai"><?= $groom ?> & <?= $bride ?></div>
                        <div class="wdp-dear">Yth. Bapak/Ibu/Saudara/i</div>
                        <div class="wdp-name"><?= $to ?></div>
                        <div class="wdp-text">Tanpa mengurangi rasa hormat, <br> Kami mengundang Bapak/Ibu/Saudara/i <br>untuk hadir di acara pernikahan kami. </div>
                        <div class="wdp-button-wrapper">
                          <button class="elementor-button"> Buka Undangan </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <script>
                    const sampul = jQuery('.modalx').data('sampul');
                    jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
                    jQuery('body').css('overflow', 'hidden');
                    jQuery('.wdp-button-wrapper button').on('click', function() {
                      jQuery('.modalx').removeClass('');
                      jQuery('.modalx').addClass('fadeIn reverse');
                      setTimeout(function() {
                        jQuery('.modalx').addClass('removeModals');
                      }, 1600);
                      jQuery('body').css('overflow', 'auto');
                      document.getElementById("song_").play();
                    });
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- <div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-widget-wrap elementor-element-populated">
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-spacer">
                              <div class="elementor-spacer-inner"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                  <div class="elementor-widget-container">
                    <p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami. </p>
                  </div>
                </div>
                <div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
                  <div class="elementor-widget-container">
                    <div class="elementor-spacer">
                      <div class="elementor-spacer-inner"></div>
                    </div>
                  </div>
                </div>
                <div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
                  <div class="elementor-widget-container">
                    <div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
                      <div class="jet-tabs__control-wrapper">
                        <div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/08/Logo-BCA.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/08/Logo-Mandiri.png" alt="">
                          </div>
                        </div>
                      </div>
                      <div class="jet-tabs__content-wrapper">
                        <div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                          <div class="elementor-widget-container">
                            <h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-icon-wrapper">
                              <div class="elementor-icon">
                                <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-image img"></div>
                            <div class="head-title">a/n Bimo Bramantyo</div>
                            <div class="elementor-button-wrapper">
                              <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                              <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                <div class="elementor-button-content-wrapper">
                                  <span class="elementor-button-icon elementor-align-icon-left">
                                    <i aria-hidden="true" class="far fa-copy"></i>
                                  </span>
                                  <span class="elementor-button-text">Salin</span>
                                </div>
                              </a>
                            </div>
                            <style type="text/css">
                              .spancontent {
                                padding-bottom: 20px;
                              }

                              .copy-content {
                                color: #6EC1E4;
                                text-align: center;
                              }

                              .head-title {
                                color: #6EC1E4;
                                text-align: center;
                              }
                            </style>
                            <script>
                              function copyText(el) {
                                var content = jQuery(el).siblings('div.copy-content').html()
                                var temp = jQuery(" < textarea > ");
                                  jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                  var text = jQuery(el).html()
                                  jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                  var interval = setInterval(function() {
                                    counter++;
                                    if (counter == 2) {
                                      jQuery(el).html(text)
                                      Interval(interval);
                                    }
                                  }, 5000);
                                }
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
          <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
              <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
                  <div class="elementor-widget-container">
                    <div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
                      <nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                      <div class="pp-menu-toggle pp-menu-toggle-on-tablet">
                        <div class="pp-hamburger">
                          <div class="pp-hamburger-box">
                            <div class="pp-hamburger-inner"></div>
                          </div>
                        </div>
                      </div>
                      <nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div> -->
    <link rel='stylesheet' id='ep-countdown-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-countdown.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-gallery-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/e-gallery/css/e-gallery.min.css?ver=1.2.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-51208-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-51208.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='e-animations-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
    <script type='text/javascript' id='WEDKU_SCRP-js-extra'>
      /* 
																																										<![CDATA[ */
      var ceper = {
        // "ajax_url": "<?= $base_url ?>wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.18' id='WEDKU_SCRP-js'></script>
    <script type='text/javascript' id='wdp_js_script-js-extra'>
      /* 
																																										<![CDATA[ */
      var WDP_WP = {
        // "ajaxurl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "wdpNonce": "ce8ea137e7",
        "jpages": "true",
        "jPagesNum": "5",
        "textCounter": "true",
        "textCounterNum": "500",
        "widthWrap": "",
        "autoLoad": "true",
        "thanksComment": "Terima kasih atas ucapan & doanya!",
        "thanksReplyComment": "Terima kasih atas balasannya!",
        "duplicateComment": "You might have left one of the fields blank, or duplicate comments",
        "insertImage": "Insert image",
        "insertVideo": "Insert video",
        "insertLink": "Insert link",
        "checkVideo": "Check video",
        "accept": "Accept",
        "cancel": "Cancel",
        "reply": "Balas",
        "textWriteComment": "Tulis Ucapan & Doa",
        "classPopularComment": "wdp-popular-comment",
        "textUrlImage": "Url image",
        "textUrlVideo": "Url video youtube or vimeo",
        "textUrlLink": "Url link",
        "textToDisplay": "Text to display",
        "textCharacteresMin": "Minimal 2 karakter",
        "textNavNext": "Selanjutnya",
        "textNavPrev": "Sebelumnya",
        "textMsgDeleteComment": "Do you want delete this comment?",
        "textLoadMore": "Load more",
        "textLikes": "Likes"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/loftloader/assets/js/loftloader.min.js?ver=2022022501' id='loftloader-lite-front-main-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/pp-bg-effects.min.js?ver=1.0.0' id='pp-bg-effects-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/particles/particles.min.js?ver=2.0.0' id='particles-js'></script>
    <script type='text/javascript' id='bdt-uikit-js-extra'>
      /* 
																																										<![CDATA[ */
      var element_pack_ajax_login_config = {
        // "ajaxurl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "language": "en",
        "loadingmessage": "Sending user info, please wait...",
        "unknownerror": "Unknown error, make sure access is correct!"
      };
      var ElementPackConfig = {
        // "ajaxurl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "nonce": "33fe9462c8",
        "data_table": {
          "language": {
            "lengthMenu": "Show _MENU_ Entries",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "search": "Search :",
            "paginate": {
              "previous": "Previous",
              "next": "Next"
            }
          }
        },
        "contact_form": {
          "sending_msg": "Sending message please wait...",
          "captcha_nd": "Invisible captcha not defined!",
          "captcha_nr": "Could not get invisible captcha response!"
        },
        "mailchimp": {
          "subscribing": "Subscribing you please wait..."
        },
        "elements_data": {
          "sections": [],
          "columns": [],
          "widgets": []
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
    <script type='text/javascript' id='elementor-frontend-js-before'>
      var elementorFrontendConfig = {
        "environmentMode": {
          "edit": false,
          "wpPreview": false,
          "isScriptDebug": false
        },
        "i18n": {
          "shareOnFacebook": "Share on Facebook",
          "shareOnTwitter": "Share on Twitter",
          "pinIt": "Pin it",
          "download": "Download",
          "downloadImage": "Download image",
          "fullscreen": "Fullscreen",
          "zoom": "Zoom",
          "share": "Share",
          "playVideo": "Play Video",
          "previous": "Previous",
          "next": "Next",
          "close": "Close"
        },
        "is_rtl": false,
        "breakpoints": {
          "xs": 0,
          "sm": 480,
          "md": 768,
          "lg": 1025,
          "xl": 1440,
          "xxl": 1600
        },
        "responsive": {
          "breakpoints": {
            "mobile": {
              "label": "Mobile",
              "value": 767,
              "default_value": 767,
              "direction": "max",
              "is_enabled": true
            },
            "mobile_extra": {
              "label": "Mobile Extra",
              "value": 880,
              "default_value": 880,
              "direction": "max",
              "is_enabled": false
            },
            "tablet": {
              "label": "Tablet",
              "value": 1024,
              "default_value": 1024,
              "direction": "max",
              "is_enabled": true
            },
            "tablet_extra": {
              "label": "Tablet Extra",
              "value": 1200,
              "default_value": 1200,
              "direction": "max",
              "is_enabled": false
            },
            "laptop": {
              "label": "Laptop",
              "value": 1366,
              "default_value": 1366,
              "direction": "max",
              "is_enabled": false
            },
            "widescreen": {
              "label": "Widescreen",
              "value": 2400,
              "default_value": 2400,
              "direction": "min",
              "is_enabled": false
            }
          }
        },
        "version": "3.6.4",
        "is_static": false,
        "experimentalFeatures": {
          "e_dom_optimization": true,
          "a11y_improvements": true,
          "e_import_export": true,
          "e_hidden_wordpress_widgets": true,
          "theme_builder_v2": true,
          "landing-pages": true,
          "elements-color-picker": true,
          "favorite-widgets": true,
          "admin-top-bar": true,
          "form-submissions": true
        },
        "urls": {
        //   "assets": "<?= $base_url ?>wp-content\/plugins\/elementor\/assets\/"
        },
        "settings": {
          "page": [],
          "editorPreferences": []
        },
        "kit": {
          "viewport_tablet": 1024,
          "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
          "global_image_lightbox": "yes"
        },
        "post": {
          "id": 26593,
        //   "title": "Unity%20Platinum%201",
          "excerpt": "",
        //   "featuredImage": "<?= $base_url ?>wp-content\/uploads\/2021\/10\/Platinum-1-cover-invitation.jpg"
        }
      };
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-countdown.min.js?ver=6.0.10' id='ep-countdown-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
    <script type='text/javascript' id='powerpack-frontend-js-extra'>
      /* 
																																										<![CDATA[ */
      var ppLogin = {
        "empty_username": "Enter a username or email address.",
        "empty_password": "Enter password.",
        "empty_password_1": "Enter a password.",
        "empty_password_2": "Re-enter password.",
        "empty_recaptcha": "Please check the captcha to verify you are not a robot.",
        "email_sent": "A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.",
        "reset_success": "Your password has been reset successfully.",
        // "ajax_url": "<?= $base_url ?>wp-admin\/admin-ajax.php"
      };
      var ppRegistration = {
        "invalid_username": "This username is invalid because it uses illegal characters. Please enter a valid username.",
        "username_exists": "This username is already registered. Please choose another one.",
        "empty_email": "Please type your email address.",
        "invalid_email": "The email address isn\u2019t correct!",
        "email_exists": "The email is already registered, please choose another one.",
        "password": "Password must not contain the character \"\\\\\"",
        "password_length": "Your password should be at least 8 characters long.",
        "password_mismatch": "Password does not match.",
        "invalid_url": "URL seems to be invalid.",
        "recaptcha_php_ver": "reCAPTCHA API requires PHP version 5.3 or above.",
        "recaptcha_missing_key": "Your reCAPTCHA Site or Secret Key is missing!",
        "show_password": "Show password",
        "hide_password": "Hide password",
        // "ajax_url": "<?= $base_url ?>wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min.js?ver=1.2.0' id='elementor-gallery-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
    <script type='text/javascript' id='elementor-pro-frontend-js-before'>
      var ElementorProFrontendConfig = {
        // "ajaxurl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "nonce": "f0e2eed9ea",
        "urls": {
        //   "assets": "<?= $base_url ?>wp-content\/plugins\/elementor-pro\/assets\/",
        //   "rest": "<?= $base_url ?>wp-json\/"
        },
        "i18n": {
          "toc_no_headings_found": "No headings were found on this page."
        },
        "shareButtonsNetworks": {
          "facebook": {
            "title": "Facebook",
            "has_counter": true
          },
          "twitter": {
            "title": "Twitter"
          },
          "linkedin": {
            "title": "LinkedIn",
            "has_counter": true
          },
          "pinterest": {
            "title": "Pinterest",
            "has_counter": true
          },
          "reddit": {
            "title": "Reddit",
            "has_counter": true
          },
          "vk": {
            "title": "VK",
            "has_counter": true
          },
          "odnoklassniki": {
            "title": "OK",
            "has_counter": true
          },
          "tumblr": {
            "title": "Tumblr"
          },
          "digg": {
            "title": "Digg"
          },
          "skype": {
            "title": "Skype"
          },
          "stumbleupon": {
            "title": "StumbleUpon",
            "has_counter": true
          },
          "mix": {
            "title": "Mix"
          },
          "telegram": {
            "title": "Telegram"
          },
          "pocket": {
            "title": "Pocket",
            "has_counter": true
          },
          "xing": {
            "title": "XING",
            "has_counter": true
          },
          "whatsapp": {
            "title": "WhatsApp"
          },
          "email": {
            "title": "Email"
          },
          "print": {
            "title": "Print"
          }
        },
        "facebook_sdk": {
          "lang": "en_US",
          "app_id": ""
        },
        "lottie": {
        //   "defaultAnimationUrl": "<?= $base_url ?>wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"
        }
      };
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
    <script type='text/javascript' id='jet-elements-js-extra'>
      /* 
																																										<![CDATA[ */
      var jetElements = {
        // "ajaxUrl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "isMobile": "false",
        // "templateApiUrl": "<?= $base_url ?>wp-json\/jet-elements-api\/v1\/elementor-template",
        "devMode": "false",
        "messages": {
          "invalidMail": "Please specify a valid e-mail"
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
    <script type='text/javascript' id='jet-tabs-frontend-js-extra'>
      /* 
																																										<![CDATA[ */
      var JetTabsSettings = {
        // "ajaxurl": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        "isMobile": "false",
        // "templateApiUrl": "<?= $base_url ?>wp-json\/jet-tabs-api\/v1\/elementor-template",
        "devMode": "false"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
    <script type='text/javascript' id='weddingpress-wdp-js-extra'>
      /* 
																																										<![CDATA[ */
      var cevar = {
        // "ajax_url": "<?= $base_url ?>wp-admin\/admin-ajax.php",
        // "plugin_url": "<?= $base_url ?>wp-content\/plugins\/weddingpress\/"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>


	<!-- COUNTDOWN -->
	<script type="text/javascript" src="<?= $asset ?>custom/plugins/downCount/jquery.downCount.js"></script> 
	<script class="source" type="text/javascript">
		$('.countdown').downCount({
			// Bulan/Tanggal/Tahun Jam/Menit/Detik
			date: '12/31/2023 08:00:00',
			offset: +7
		});
	</script>
    <script>
      function openInvitation() {
        document.getElementById("unmute-sound").style.display = 'none';
        document.getElementById("mute-sound").style.display = 'block';
      }

      function saveSAP() {
        let name = $('#name').val();
        let description = $('#description').val();
        let confirmation_of_attendance = $('#confirmation_of_attendance').val();

        if (name == '') { return false }
        if (description == '') { return false }
        
            $.ajax({
                type: 'POST',
                url: '<?= $url_save_sap ?>',
                data: {
                    'uri_couple': '<?= $uri_couple ?>',
                    'name': name,
                    'description': description,
                    'confirmation_of_attendance': confirmation_of_attendance,
                },
                success: function(response) {
            $('#name').val('');
            $('#description').val('');
            $('#confirmation_of_attendance').val('').change();
            
            $('#wdp-comment-status-26593').css('display', 'block');
            setTimeout(() => {
              $('#wdp-comment-status-26593').css('display', 'none');
            }, 3000);

            getSAP({page: 1});
          }
        });
      }

      function getSAP(params) {
            $.ajax({
                type: 'POST',
                url: '<?= $url_get_sap ?>',
                data: {
                    'uri_couple': '<?= $uri_couple ?>',
            'page': params.page
                },
                success: function(response) {
            let res = JSON.parse(response);
            let sap = '';
            let page = res.page.length > 1 ? `<a class="jp-previous ${params.page == 1 ? 'jp-disabled' : ''}" ${params.page == 1 ? '' : `onclick="getSAP({page: ${params.page-1}})"`}>← Sebelumnya</a>` : '';

            $.each(res.sap, function(k, v) {
              if (v.page == params.page) {
                sap += `<li class="comment even thread-even depth-1 wdp-item-comment animated fadeIn" id="wdp-item-comment-14904" data-likes="0">
                  <div id="wdp-comment-14904" class="wdp-comment wdp-clearfix">
                    <div class="wdp-comment-avatar">
                      <img src="<?=$asset_theme?>/icons/icon-comment-1.png">
                    </div><!--.wdp-comment-avatar-->
                    <div class="wdp-comment-content">
                      <div class="wdp-comment-info">
                        <a class="wdp-commenter-name" title="${v.name}">${v.name}</a>
                        <span class="wdp-post-author"><i class="fas fa-check-circle"></i> ${v.confirmation_of_attendance}</span>

						<div style="text-align: right; float: right; display: block;">
							<a href="<?=$base_url?><?=$uri_couple?>/repost.php?bg=<?=rawurlencode($bg)?>&img=<?=rawurlencode($img)?>&saying=${encodeURIComponent(v.description)}&by=${encodeURIComponent(v.name)}"><i class="fas fa-download"></i></a>
						</div>

                        <br>
                        <span class="wdp-comment-time">
                        <i class="far fa-clock"></i>
                      ${v.human_created_at}
                    </span>
                      </div><!--.wdp-comment-info-->
                      <div class="wdp-comment-text">
                        <p>${v.description}</p>
                      </div><!--.wdp-comment-text-->
                      <div class="wdp-comment-actions" style="display: block;">
                      </div><!--.wdp-comment-actions-->
                    </div><!--.wdp-comment-content-->
                  </div><!--.wdp-comment-->
                  <!--</li>-->
                  </li><!-- #comment-## -->`
              }
            });

            if (res.page.length > 1) {
              $.each(res.page, function(k, v) {
                page += `<a class="" ${params.page == v ? '' : `onclick="getSAP({page: ${v}})"`}>${params.page == v ? `<b><u>${v}</u></b>` : v}</a>`;
              })
            }
            page += res.page.length > 1 ? `<a class="jp-next ${params.page == res.page.length ? 'jp-disabled' : ''}" ${params.page == res.page.length ? '' : `onclick="getSAP({page: ${params.page+1}})"`}>Selanjutnya →</a>` : '';
            
            $('#wdp-container-comment-26593').html(sap);
            $('#page').html(page);
          }
        });
      }

      $(document).ready(function() {
        getSAP({page: 1});
      })
    </script>


    <div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="<?= $asset ?>wp-admin/admin-ajax.php"></div>
    <div data-pafe-form-builder-tinymce-upload="<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>
  </body>
</html>