<?php
	// Pengantin Wanita
    $bride = 'Alina';
	$bride_firstname = 'Alina';
	$bride_middlename = 'Baraz';
	$bride_lastname = '';
	$bride_father = 'Ibnu Baraz';
	$bride_mother = 'Risma';
	$bride_child_position = 'Putri Ketiga';
	$bride_ig = 'https://instagram.com/instagram';

	// Pengantin Pria
	$groom = 'Fahri';
	$groom_firstname = 'Fahri';
	$groom_middlename = 'Kurniawan';
	$groom_lastname = '';
	$groom_father = 'Tommy Kurniawan';
	$groom_mother = 'Fahrana';
	$groom_child_position = 'Putra Pertama';
	$groom_ig = 'https://instagram.com/instagram';

	// Akad
	$akad_fullday = 'Desember 31, 2022';
	$akad_day = 'Minggu';
	$akad_date = '31';
	$akad_date_show_duration = '1000';
	$akad_month = 'Desember';
	$akad_year = '2022';
	$akad_hour = '08:00 WIB';
	$akad_venue = 'Kediaman Rumah Bapak Suratman';
	$akad_venue_address = 'Jl. Pisang Agung II no 7 rt4 rw5, Kel. Pisang Candi, Kec Sukun, Kota Malang';
	$akad_venue_map = 'https://goo.gl/maps/Pysrzo6uJu1NDZ4FA';
	
	// Resepsi
	$wedding_fullday = '31 . 12 . 2022';
	$wedding_day = 'Minggu';
	$wedding_date = '31';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'Desember';
	$wedding_year = '2022';
	$wedding_hour = 'Jam Bebas';
	$wedding_venue = 'Kediaman Rumah Bapak Suratman';
	$wedding_venue_address = 'Jl. Pisang Agung II no 7 rt4 rw5, Kel. Pisang Candi, Kec Sukun, Kota Malang';
	$wedding_venue_map = 'https://goo.gl/maps/Pysrzo6uJu1NDZ4FA';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '07/3/2022 08:00:00';
	
	// Gift
	$transfer_gift_user = 'Fahri Kurniawan';
	$transfer_gift_rekening = '3850684331';
	$send_gift_user = 'Fahri Kurniawan';
	$send_gift_address = 'Jl. Pisang Agung II no 7 rt4 rw5, Kel. Pisang Candi, Kec Sukun, Kota Malang';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '6287866767417';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/instagram';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/MT71X613n08';

	// Love Story
	$first_meet_date = '11 Januari 2016';
	$first_meet_story = 'Pertama kali kita bertemu di bangku SMA tanpa mengenal satu sama lain. Kita hanya saling sapa.';
	$having_a_relationship_date = '08 Juni 2019';
	$having_a_relationship_story = 'Setelah pertemuan di bangku SMA dan masing-masing dari kita sudah lulus, barulah kita mulai menjalin hubungan yang berawal dari DM Instagram. Hingga kita memutuskan untuk sama-sama dan LDR.';
	$engagement_date = '17 Agustus 2021';
	$engagement_story = 'Komunikasi jarak jauh dan kesibukan kita masing-masing bukan penghalang. Pada Agustus 2020 kedua keluarga saling bertemu dan melaksanakan acara tunangan kami';
	$married_date = '03 Juli 2022';
	$married_story = 'Akhirnya momen spesial pernikahan kami dilaksanakan pada 01 Desember 2022. Ini menjadi tanggal yang kami pilih untuk saling mengikat janji menjadi sebuah keluarga yang setia & saling menyayangi.';

	// Backsound
	$backsound = 'Ysabelle-Cueva-I-Like-You-So-Much-Youll-Know-It-Piano-Cover-WEB.mp3';

	// Title
	$title = "$groom & $bride";

	// Colors
	$color_1 = '#736864';
	$color_2 = '#a99f9e';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';
?>