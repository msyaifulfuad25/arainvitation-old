<?php
	include "../data.php";

    $uri_couple = $_POST['uri_couple'];
    $page = $_POST['page'];

    $sql = "SELECT * FROM sayings_and_prayers WHERE uri_couple='$uri_couple' ORDER BY created_at DESC, name DESC";
    $result = mysqli_query($conn, $sql);
    $data = [
        'sap' => [],
        'page' => []
    ];
    if (mysqli_num_rows($result) > 0) {
        $page = 1;
        while($row = mysqli_fetch_assoc($result)) {
            $start = new DateTime($row['created_at']);
            $end = new DateTime(date('Y-m-d H:i:s'));
            
            // echo convert_seconds(9000000);die;
            $human_created_at = convert_seconds($end->getTimestamp() - $start->getTimestamp());
            $row['human_created_at'] = $human_created_at;
            $row['page'] = $page;

            $data['sap'][] = $row;
            
            if (!in_array($page, $data['page'])) {
                $data['page'][] = $page;
            }
            
            if (count($data['sap'])%5 == 0) {
                $page++;
            }

        }
    }
    echo json_encode($data);
?>