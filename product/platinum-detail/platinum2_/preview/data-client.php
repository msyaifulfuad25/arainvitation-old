<?php
	// Pengantin Wanita
    $bride = 'Kania';
	$bride_firstname = 'Kania';
	$bride_middlename = 'Adriani';
	$bride_lastname = '';
	$bride_father = 'Adriano';
	$bride_mother = 'Citra';
	$bride_child_position = 'Putri Keempat';
	$bride_ig = 'ar.rtr';

	// Pengantin Pria
	$groom = 'Haikal';
	$groom_firstname = 'Haikal';
	$groom_middlename = 'Abimana';
	$groom_lastname = '';
	$groom_father = 'Abimana';
	$groom_mother = 'Amanda';
	$groom_child_position = 'Putra Pertama';
	$groom_ig = 'ar.rtr';

	// Akad
	$akad_fullday = 'July 31, 2023';
	$akad_day = 'Min';
	$akad_date = '31';
	$akad_month = 'Juli';
	$akad_year = '2023';
	$akad_hour = '08:00';
	$akad_venue = 'Auditorium Graha Widyatama';
	$akad_venue_address = 'Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122';
	$akad_venue_map = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.2670159584995!2d111.90832811414634!3d-8.074225882938402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78e303563502d3%3A0xdca3b3ad7c0e3e7e!2sJepun%20View%20Resto!5e0!3m2!1sen!2sid!4v1646114865092!5m2!1sen!2sid';
	
	// Resepsi
	$wedding_fullday = '31 . 07 . 2023';
	$wedding_day = 'Min';
	$wedding_date = '31';
	$wedding_month = 'Juli';
	$wedding_year = '2023';
	$wedding_hour = '13:00';
	$wedding_venue = 'Auditorium Graha Widyatama';
	$wedding_venue_address = 'Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122';
	$wedding_venue_map = 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.2670159584995!2d111.90832811414634!3d-8.074225882938402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78e303563502d3%3A0xdca3b3ad7c0e3e7e!2sJepun%20View%20Resto!5e0!3m2!1sen!2sid!4v1646114865092!5m2!1sen!2sid';
	
	// Countdown
	$countdown_to_akad = '2023-07-31 08:00';
	
	// Gift
	$transfer_gift_user = 'Kania Adriani';
	$transfer_gift_rekening = '1234567891011';
	$send_gift_user = 'Kania Adriani';
	$send_gift_address = 'Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '62895367255770';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$photo_by = 'https://www.instagram.com/loisenses/';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/4efVf1pta0c';

	// Love Story
	$first_meet_date = '1 Mei 2018';
	$first_meet_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$having_a_relationship_date = '1 Juni 2019';
	$having_a_relationship_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$engagement_date = '1 Juli 2020';
	$engagement_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$married_date = '1 Desember 2022';
	$married_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';

	// Backsound
	$backsound = 'music/piano/Ysabelle-Cuevas-I-Like-You-So-Much-Youll-Know-It-Piano-by-Riyandi-Kusuma.mp3';
?>