<?php
	include "../../../../data.php";
	include "data-client.php";

	$product_name = 'Ara Platinum 2';
	$asset_theme = $base_url.'product/platinum-detail/platinum2/preview/assets/';
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
		
        <style type="text/css">

            .wdp-comment-text img {

                max-width: 100% !important;

            }

        </style>

        
<!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
<title><?= $product_name ?></title>
<meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami."/>
<meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet"/>
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?= $product_name ?>" />
<meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
<meta property="og:url" content="<?= $asset ?>unity-platinum-2/" />
<meta property="og:site_name" content="Ara Invitation" />
<meta property="article:section" content="Undangan" />
<meta property="og:updated_time" content="2022-04-29T06:49:05+07:00" />
<meta property="og:image" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-2-cover-invitation.jpg" />
<meta property="og:image:secure_url" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-2-cover-invitation.jpg" />
<meta property="og:image:width" content="500" />
<meta property="og:image:height" content="500" />
<meta property="og:image:alt" content="<?= $product_name ?>" />
<meta property="og:image:type" content="image/jpeg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="<?= $product_name ?>" />
<meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
<meta name="twitter:image" content="<?= $asset ?>wp-content/uploads/2021/10/Platinum-2-cover-invitation.jpg" />
<meta name="twitter:label1" content="Written by" />
<meta name="twitter:data1" content="admin" />
<meta name="twitter:label2" content="Time to read" />
<meta name="twitter:data2" content="3 minutes" />
<script type="application/ld+json" class="rank-math-schema">{"@context":"https://schema.org","@graph":[{"@type":"BreadcrumbList","@id":"https://unityinvitation.com/unity-platinum-2/#breadcrumb","itemListElement":[{"@type":"ListItem","position":"1","item":{"@id":"https://unityinvitation.com","name":"Home"}},{"@type":"ListItem","position":"2","item":{"@id":"https://unityinvitation.com/unity-platinum-2/","name":"<?= $product_name ?>"}}]}]}</script>
<!-- /Rank Math WordPress SEO plugin -->

<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Ara Invitation &raquo; Feed" href="<?= $asset ?>feed/" />
<link rel="alternate" type="application/rss+xml" title="Ara Invitation &raquo; Comments Feed" href="<?= $asset ?>comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Ara Invitation &raquo; <?= $product_name ?> Comments Feed" href="<?= $asset ?>unity-platinum-2/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.9.3"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='bdt-uikit-css'  href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
<link rel='stylesheet' id='ep-helper-css'  href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='<?= $asset ?>wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
</style>
<link rel='stylesheet' id='WEDKU_STYLE-css'  href='<?= $asset ?>wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.16' type='text/css' media='all' />
<link rel='stylesheet' id='pafe-extension-style-css'  href='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
<link rel='stylesheet' id='wdp_style-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
<style id='wdp_style-inline-css' type='text/css'>


        .wdp-wrapper {

          font-size: 14px

        }

    

        .wdp-wrapper {

          background: #ffffff;

        }

        .wdp-wrapper.wdp-border {

          border: 1px solid #d5deea;

        }



        .wdp-wrapper .wdp-wrap-comments a:link,

        .wdp-wrapper .wdp-wrap-comments a:visited {

          color: #54595f;

        }



        .wdp-wrapper .wdp-wrap-link a.wdp-link {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-wrap-form {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {

          border-color: #64B6EC;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {

          color: #FFFFFF;

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {

          background: #a3a3a3;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {

          color: #44525F;

        }



        .wdp-wrapper .wdp-media-btns a > span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-media-btns a > span:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-comment-status {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {

          color: #319342;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {

          color: #C85951;

        }

        .wdp-wrapper .wdp-comment-status.wdp-loading > span {

          color: #54595f;

        }



        .wdp-wrapper ul.wdp-container-comments {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {

          border-bottom: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {

          color: #54595f !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {

          color: #2a5782 !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {

          color: #54595f;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {

          color: #2a5782;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span:hover {

          color: #3D7DBC;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {

          color: #2C9E48;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {

          color: #D13D3D;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-counter-info {

          color: #9DA8B7;

        }



        .wdp-wrapper .wdp-holder span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a,

        .wdp-wrapper .wdp-holder a:link,

        .wdp-wrapper .wdp-holder a:visited {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a:hover,

        .wdp-wrapper .wdp-holder a:link:hover,

        .wdp-wrapper .wdp-holder a:visited:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled, .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {

          color: #9DA8B7;

        }

        

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {

        max-width: 28px;

        max-height: 28px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {

        margin-left: 38px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {

        max-width: 24px;

        max-height: 24px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {

        max-width: 21px;

        max-height: 21px;

    }

    
</style>
<link rel='stylesheet' id='wdp-centered-css-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-horizontal-css-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-fontello-css-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='exad-main-style-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='a33f19b0d-css'  href='<?= $asset ?>wp-content/uploads/essential-addons-elementor/734e5f942.min.css?ver=1652086788' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-css'  href='<?= $asset ?>wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-theme-style-css'  href='<?= $asset ?>wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-css'  href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-skin-css'  href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
<style id='elementor-icons-inline-css' type='text/css'>

		.elementor-add-new-section .elementor-add-templately-promo-button{
            background-color: #5d4fff;
            background-image: url(https://unityinvitation.com/wp-content/plugins/essential-addons-for-elementor-lite/assets/admin/images/templately/logo-icon.svg);
            background-repeat: no-repeat;
            background-position: center center;
            margin-left: 5px;
            position: relative;
            bottom: 5px;
        }
</style>
<link rel='stylesheet' id='elementor-frontend-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-1399-css'  href='<?= $asset ?>wp-content/uploads/elementor/css/post-1399.css?ver=1652056007' type='text/css' media='all' />
<link rel='stylesheet' id='powerpack-frontend-css'  href='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='weddingpress-wdp-css'  href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-pro-css'  href='<?= $asset ?>wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='uael-frontend-css'  href='<?= $asset ?>wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
<link rel='stylesheet' id='jet-tabs-frontend-css'  href='<?= $asset ?>wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-27002-css'  href='<?= $asset ?>wp-content/uploads/elementor/css/post-27002.css?ver=1652058561' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51207-css'  href='<?= $asset ?>wp-content/uploads/elementor/css/post-51207.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-31584-css'  href='<?= $asset ?>wp-content/uploads/elementor/css/post-31584.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-official-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
<link rel='stylesheet' id='font-awesome-official-v4shim-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
<style id='font-awesome-official-v4shim-inline-css' type='text/css'>
@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
unicode-range: U+F004-F005,U+F007,U+F017,U+F022,U+F024,U+F02E,U+F03E,U+F044,U+F057-F059,U+F06E,U+F070,U+F075,U+F07B-F07C,U+F080,U+F086,U+F089,U+F094,U+F09D,U+F0A0,U+F0A4-F0A7,U+F0C5,U+F0C7-F0C8,U+F0E0,U+F0EB,U+F0F3,U+F0F8,U+F0FE,U+F111,U+F118-F11A,U+F11C,U+F133,U+F144,U+F146,U+F14A,U+F14D-F14E,U+F150-F152,U+F15B-F15C,U+F164-F165,U+F185-F186,U+F191-F192,U+F1AD,U+F1C1-F1C9,U+F1CD,U+F1D8,U+F1E3,U+F1EA,U+F1F6,U+F1F9,U+F20A,U+F247-F249,U+F24D,U+F254-F25B,U+F25D,U+F267,U+F271-F274,U+F279,U+F28B,U+F28D,U+F2B5-F2B6,U+F2B9,U+F2BB,U+F2BD,U+F2C1-F2C2,U+F2D0,U+F2D2,U+F2DC,U+F2ED,U+F328,U+F358-F35B,U+F3A5,U+F3D1,U+F410,U+F4AD;
}
</style>
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLora%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-wedding-css'  href='<?= $asset ?>wp-content/uploads/elementor/custom-icons/wedding/css/wedding.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-unityinv-css'  href='<?= $asset ?>wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
<script type='text/javascript' id='jquery-core-js-extra'>
/* <![CDATA[ */
var pp = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
<link rel="https://api.w.org/" href="<?= $asset ?>wp-json/" /><link rel="alternate" type="application/json" href="<?= $asset ?>wp-json/wp/v2/posts/27002" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?= $asset ?>xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?= $asset ?>wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.9.3" />
<link rel='shortlink' href='<?= $asset ?>?p=27002' />
<link rel="alternate" type="application/json+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-platinum-2%2F" />
<link rel="alternate" type="text/xml+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-platinum-2%2F&#038;format=xml" />
<link rel="icon" href="<?= $asset ?>images/favicon.png" sizes="32x32" />
<link rel="icon" href="<?= $asset ?>images/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon" href="<?= $asset ?>images/favicon.png" />
<meta name="msapplication-TileImage" content="<?= $asset ?>images/favicon.png" />
<style>.pswp.pafe-lightbox-modal {display: none;}</style>		<style type="text/css" id="wp-custom-css">
			.elementor-section{
	overflow:hidden;
}

.justify-cstm .pp-timeline-card{
	text-align:justify !important;
}		</style>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /></head>
<body data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-27002 single-format-standard elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-27002">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>		<div data-elementor-type="wp-post" data-elementor-id="27002" class="elementor elementor-27002">
									<section class="pp-bg-effects pp-bg-effects-3a81fe5 elementor-section elementor-top-section elementor-element elementor-element-3a81fe5 elementor-section-stretched pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="3a81fe5" data-effect-enable="yes" data-animation-type="particles" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="5" data-part-speed="" data-part-direction="none" data-hover-effect="grab" data-hover-size="" data-id="3a81fe5" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0127491 wdp-sticky-section-no" data-id="0127491" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e0460e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4e0460e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;bounceIn&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">PERNIKAHAN</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-70a8387 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="70a8387" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default"><?= $groom ?> & <?= $bride ?></h1>		</div>
				</div>
				<div class="elementor-element elementor-element-f7a45c6 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="f7a45c6" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-55a13b0 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="55a13b0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;bounceIn&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default"> <?= $wedding_fullday ?></h3>		</div>
				</div>
				<div class="elementor-element elementor-element-b905764 elementor-view-default elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="b905764" data-element_type="widget" data-widget_type="icon-box.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-box-wrapper">
						<div class="elementor-icon-box-icon">
				<span class="elementor-icon elementor-animation-" >
				<i aria-hidden="true" class="fas fa-angle-double-up"></i>				</span>
			</div>
						<div class="elementor-icon-box-content">
				<h3 class="elementor-icon-box-title">
					<span  >
						Swipe Up					</span>
				</h3>
							</div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-2be281b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2be281b" data-element_type="section" data-settings="{&quot;shape_divider_bottom&quot;:&quot;wave-brush&quot;}">
					<div class="elementor-shape elementor-shape-bottom" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-453cbef wdp-sticky-section-no" data-id="453cbef" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c795ec7 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="c795ec7" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-233deb9 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="233deb9" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3061c9c wdp-sticky-section-no" data-id="3061c9c" data-element_type="column" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:58}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-9b7962a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9b7962a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pasangan </p>		</div>
				</div>
				<div class="elementor-element elementor-element-02553d4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="02553d4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Mempelai</p>		</div>
				</div>
				<div class="elementor-element elementor-element-38898b4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="38898b4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Maha Suci Allah Subhanahu wa Ta'ala yang telah menciptakan makhluk-Nya berpasang-pasangan. Ya Allah, perkenankanlah dan Ridhoilah Pernikahan Kami.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-cd15abe wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="cd15abe" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-0822a3f elementor-reverse-mobile animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="0822a3f" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-f61f36b animated-slow wdp-sticky-section-no elementor-invisible" data-id="f61f36b" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c2fb9e3 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c2fb9e3" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default"><?= $groom_firstname ?> <br> <?= $groom_middlename ?> <?= $groom_lastname ?></h3>		</div>
				</div>
				<div class="elementor-element elementor-element-92bc714 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="92bc714" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-036536a wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="036536a" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><?= $groom_child_position ?></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-85a91a2 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="85a91a2" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bapak <?= $groom_father ?> <br> & <br> Ibu <?= $groom_mother ?></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-8328da0 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="8328da0" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="https://instagram.com/<?= $groom_ig ?>" target="_blank">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-867a648 animated-slow wdp-sticky-section-no elementor-invisible" data-id="867a648" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2986274 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="2986274" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:700}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="624" height="1024" src="<?= $asset_theme ?>photos/Platinum-2-CPP-624x1024.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>photos/Platinum-2-CPP-624x1024.png 624w, <?= $asset_theme ?>photos/Platinum-2-CPP-183x300.png 183w, <?= $asset_theme ?>photos/Platinum-2-CPP-768x1260.png 768w, <?= $asset_theme ?>photos/Platinum-2-CPP-937x1536.png 937w, <?= $asset_theme ?>photos/Platinum-2-CPP.png 1000w" sizes="(max-width: 624px) 100vw, 624px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-63729ab animated-slow wdp-sticky-section-no elementor-invisible" data-id="63729ab" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-11db282 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="11db282" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">&</h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-091585d animated-slow wdp-sticky-section-no elementor-invisible" data-id="091585d" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-7c42770 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="7c42770" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:700}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="624" height="1024" src="<?= $asset_theme ?>photos/Platinum-2-CPW-624x1024.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>photos/Platinum-2-CPW-624x1024.png 624w, <?= $asset_theme ?>photos/Platinum-2-CPW-183x300.png 183w, <?= $asset_theme ?>photos/Platinum-2-CPW-768x1260.png 768w, <?= $asset_theme ?>photos/Platinum-2-CPW-937x1536.png 937w, <?= $asset_theme ?>photos/Platinum-2-CPW.png 1000w" sizes="(max-width: 624px) 100vw, 624px" />															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-20 elementor-inner-column elementor-element elementor-element-75134e0 animated-slow wdp-sticky-section-no elementor-invisible" data-id="75134e0" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-b5b5a57 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="b5b5a57" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default"><?= $bride_firstname ?> <br><?= $bride_middlename ?> <?= $bride_lastname ?></h3>		</div>
				</div>
				<div class="elementor-element elementor-element-ac62e0d elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="ac62e0d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-ae9b56e wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ae9b56e" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><?= $bride_child_position ?></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-0b75306 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="0b75306" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bapak <?= $bride_father ?> 
<br> & <br> 
Ibu <?= $bride_mother ?></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-1632d56 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="1632d56" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="https://instagram.com/<?= $bride_ig ?>" target="_blank">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-c8a5e7b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c8a5e7b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;wave-brush&quot;,&quot;shape_divider_bottom&quot;:&quot;wave-brush&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
				<div class="elementor-shape elementor-shape-bottom" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4d53f8c wdp-sticky-section-no" data-id="4d53f8c" data-element_type="column" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:56}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-11dde72 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="11dde72" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Hitung Mundur</p>		</div>
				</div>
				<div class="elementor-element elementor-element-07cb38b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="07cb38b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami</p>		</div>
				</div>
				<div class="elementor-element elementor-element-5838191 bdt-countdown--align-center bdt-countdown--label-block wdp-sticky-section-no elementor-widget elementor-widget-bdt-countdown" data-id="5838191" data-element_type="widget" data-widget_type="bdt-countdown.default">
				<div class="elementor-widget-container">
			

		<div class="bdt-countdown-wrapper bdt-countdown-skin-default" data-settings="{&quot;id&quot;:&quot;#bdt-countdown-5838191&quot;,&quot;msgId&quot;:&quot;#bdt-countdown-msg-5838191&quot;,&quot;adminAjaxUrl&quot;:&quot;https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php&quot;,&quot;endActionType&quot;:&quot;none&quot;,&quot;redirectUrl&quot;:null,&quot;redirectDelay&quot;:1000,&quot;finalTime&quot;:&quot;2022-12-30T17:00:00+00:00&quot;,&quot;wpCurrentTime&quot;:1652086740,&quot;endTime&quot;:1672419600,&quot;loopHours&quot;:false,&quot;isLogged&quot;:false,&quot;couponTrickyId&quot;:&quot;bdt-sf-5838191&quot;,&quot;triggerId&quot;:false}">
			<div id="bdt-countdown-5838191-timer" class="bdt-grid bdt-grid-small bdt-child-width-1-4 bdt-child-width-1-2@s bdt-child-width-1-4@m" data-bdt-countdown="date: <?= $countdown_to_akad ?>" data-bdt-grid="" style="x">
				<div class="bdt-countdown-item-wrapper"><div class="bdt-countdown-item bdt-days-wrapper"><span class="bdt-countdown-number bdt-countdown-days bdt-text-center"></span> <span class="bdt-countdown-label bdt-text-center">Hari</span></div></div><div class="bdt-countdown-item-wrapper"><div class="bdt-countdown-item bdt-hours-wrapper"><span class="bdt-countdown-number bdt-countdown-hours bdt-text-center"></span> <span class="bdt-countdown-label bdt-text-center">Jam</span></div></div><div class="bdt-countdown-item-wrapper"><div class="bdt-countdown-item bdt-minutes-wrapper"><span class="bdt-countdown-number bdt-countdown-minutes bdt-text-center"></span> <span class="bdt-countdown-label bdt-text-center">Menit</span></div></div><div class="bdt-countdown-item-wrapper"><div class="bdt-countdown-item bdt-seconds-wrapper"><span class="bdt-countdown-number bdt-countdown-seconds bdt-text-center"></span> <span class="bdt-countdown-label bdt-text-center">Detik</span></div></div>			</div>

			
		</div>

				</div>
				</div>
				<div class="elementor-element elementor-element-e86170c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e86170c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $verse_value ?>
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-ef3d8b1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ef3d8b1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $verse_name ?>

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-dc09311 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dc09311" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;wave-brush&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-bottom" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-157772a wdp-sticky-section-no" data-id="157772a" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-7409cd1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7409cd1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Waktu & Tempat</p>		</div>
				</div>
				<div class="elementor-element elementor-element-ecc34dd wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ecc34dd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pernikahan</p>		</div>
				</div>
				<div class="elementor-element elementor-element-e606b7d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e606b7d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pertemuan adalah permulaan, tetap bersama adalah perkembangan, bekerja sama adalah keberhasilan.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b9c65dd wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b9c65dd" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-9e7d7ad elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="9e7d7ad" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2d37161 animated-slow wdp-sticky-section-no" data-id="2d37161" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-a95b35b elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="a95b35b" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fontelloicon fontello-iconwedding-photography"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9df0d71 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9df0d71" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Akad Nikah</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0f18f2e elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="0f18f2e" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-f970f6a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f970f6a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_month ?></p>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ebf9e2e elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ebf9e2e" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-267e211 wdp-sticky-section-no" data-id="267e211" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-f36a93a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f36a93a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_day ?></p>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-77c8cf9 wdp-sticky-section-no" data-id="77c8cf9" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-9e9f626 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="9e9f626" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="2000" data-to-value="<?= $akad_date ?>" data-from-value="0">0</span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-451496b wdp-sticky-section-no" data-id="451496b" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-aab1556 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="aab1556" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_year ?>

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-e3dec5c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e3dec5c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_hour ?></p>		</div>
				</div>
				<div class="elementor-element elementor-element-be1ce9d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="be1ce9d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">s/d Selesai</p>		</div>
				</div>
				<div class="elementor-element elementor-element-e37e096 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="e37e096" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-00deec8 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="00deec8" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fas fa-map-marker-alt"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2ab2e06 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2ab2e06" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_venue ?>

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b1e8a0b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b1e8a0b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $akad_venue_address ?></p>		</div>
				</div>
				<div class="elementor-element elementor-element-7c80910 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="7c80910" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://goo.gl/maps/ovLwb7KeHqux7XRNA" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-location-arrow"></i>			</span>
						<span class="elementor-button-text">Lihat Lokasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-10cdbeb animated-slow wdp-sticky-section-no" data-id="10cdbeb" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200,&quot;animation_tablet&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-10d2660 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="10d2660" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fontelloicon fontello-iconwedding-dinner"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-d31c2ab wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d31c2ab" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Resepsi Nikah</p>		</div>
				</div>
				<div class="elementor-element elementor-element-94ad306 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="94ad306" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-7b8c77f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7b8c77f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_month ?></p>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-a198103 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="a198103" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-a92d6de wdp-sticky-section-no" data-id="a92d6de" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c9f532d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c9f532d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_day ?></p>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-7b02e8e wdp-sticky-section-no" data-id="7b02e8e" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0a3ba47 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="0a3ba47" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="2000" data-to-value="<?= $wedding_date ?>" data-from-value="0">0</span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2cc741b wdp-sticky-section-no" data-id="2cc741b" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-d2e03ef wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d2e03ef" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_year ?>

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-8fcbc7f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8fcbc7f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_hour ?> WIB</p>		</div>
				</div>
				<div class="elementor-element elementor-element-c6fce68 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c6fce68" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">s/d Selesai</p>		</div>
				</div>
				<div class="elementor-element elementor-element-010f671 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="010f671" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-db2368f elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="db2368f" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fas fa-map-marker-alt"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-a02eabc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="a02eabc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_venue ?>

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b452276 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b452276" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default"><?= $wedding_venue_address ?></p>		</div>
				</div>
				<div class="elementor-element elementor-element-7d31f9a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="7d31f9a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://goo.gl/maps/ovLwb7KeHqux7XRNA" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-location-arrow"></i>			</span>
						<span class="elementor-button-text">Lihat Lokasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-6e4bf11 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="6e4bf11" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-0dd3da5 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="0dd3da5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3911ed0 wdp-sticky-section-no" data-id="3911ed0" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<div class="elementor-element elementor-element-9ddf615 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="9ddf615" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-50ef5dd wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="50ef5dd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Kami Akan <br> Melangsungkan</p>		</div>
				</div>
				<div class="elementor-element elementor-element-6bea21b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6bea21b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Live Streaming

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-37a95e0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="37a95e0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Akad & Resepsi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-d65b15d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="d65b15d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="768" height="172" src="<?= $asset_theme ?>youtube/Logo-Youtube.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>youtube/Logo-Youtube-768x172.png 768w, <?= $asset_theme ?>youtube/Logo-Youtube-300x67.png 300w, <?= $asset_theme ?>youtube/Logo-Youtube-1024x229.png 1024w, <?= $asset_theme ?>youtube/Logo-Youtube.png 1348w" sizes="(max-width: 768px) 100vw, 768px" />															</div>
				</div>
				<div class="elementor-element elementor-element-6264405 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="6264405" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="<?= $youtube_streaming ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fab fa-youtube"></i>			</span>
						<span class="elementor-button-text">Join Streaming</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-639d803 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="639d803" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2a26beb wdp-sticky-section-no" data-id="2a26beb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ad63bbc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ad63bbc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Sebuah Kisah

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0422f50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="0422f50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Perjalanan Kami</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-27484d1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="27484d1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Perjalanan yang tak terlupakan, dan perjalanan yang mengubah tujuan hidup kami</p>		</div>
				</div>
				<div class="elementor-element elementor-element-a94c2d1 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="a94c2d1" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-2cd7794 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2cd7794" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-db1806b animated-slow wdp-sticky-section-no" data-id="db1806b" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1384873 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="1384873" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:200,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
				<div class="elementor-widget-container">
					<div class="pp-timeline-wrapper">
			
			<div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
									<div class="pp-timeline-connector-wrap">
						<div class="pp-timeline-connector">
							<div class="pp-timeline-connector-inner">
							</div>
						</div>
					</div>
								<div class="pp-timeline-items">
							<div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-3ddf251">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												<?= $first_meet_date ?>											</div>
																																					<h2 class="pp-timeline-card-title">
										Pertama Bertemu									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								<p>     <?= $first_meet_story ?></p>							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
									<?= $first_meet_date ?>					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-48bb742">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												<?= $having_a_relationship_date ?>											</div>
																																					<h2 class="pp-timeline-card-title">
										Menjalin Hubungan									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								<p>     <?= $having_a_relationship_story ?></p>							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						<?= $having_a_relationship_date ?>					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-9e7595d">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												<?= $engagement_date ?>											</div>
																																					<h2 class="pp-timeline-card-title">
										Bertunangan									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								<p>     <?= $engagement_story ?></p>							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						<?= $engagement_date ?>					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-255189e">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												<?= $married_date ?>											</div>
																																					<h2 class="pp-timeline-card-title">
										Menikah									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								<p>     <?= $married_story ?></p>							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						<?= $married_date ?>					</div>
							</div>
											</div>
							</div>
			</div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-cba7d47 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="cba7d47" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;wave-brush&quot;,&quot;shape_divider_bottom&quot;:&quot;wave-brush&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
				<div class="elementor-shape elementor-shape-bottom" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2ada9e0 wdp-sticky-section-no" data-id="2ada9e0" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-236e6d8 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="236e6d8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Momen</p>		</div>
				</div>
				<div class="elementor-element elementor-element-9a51c32 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9a51c32" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Bahagia Kami </p>		</div>
				</div>
				<div class="elementor-element elementor-element-49046fe wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="49046fe" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">"Mencintai bukan untuk menyamai, tetapi keikhlasan menerima perbedaan."</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b2103ec wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b2103ec" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0babed1 wdp-sticky-section-no elementor-widget elementor-widget-gallery" data-id="0babed1" data-element_type="widget" data-settings="{&quot;gallery_layout&quot;:&quot;justified&quot;,&quot;ideal_row_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:400,&quot;sizes&quot;:[]},&quot;gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;gap_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;lazyload&quot;:&quot;yes&quot;,&quot;ideal_row_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;ideal_row_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;gap_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;link_to&quot;:&quot;file&quot;,&quot;overlay_background&quot;:&quot;yes&quot;,&quot;content_hover_animation&quot;:&quot;fade-in&quot;}" data-widget_type="gallery.default">
				<div class="elementor-widget-container">
					<div class="elementor-gallery__container">
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-1.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1NzYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-1.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMTEuanBnIiwic2xpZGVzaG93IjoiYWxsLTBiYWJlZDEifQ%3D%3D">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-11.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMTAuanBnIiwic2xpZGVzaG93IjoiYWxsLTBiYWJlZDEifQ%3D%3D">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-10.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktOS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-9.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktOC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-8.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktNy5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-7.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-6.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktNi5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-6.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktNS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-5.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1NzksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktNC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-4.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-3.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1NzgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMy5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-3.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-2.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1NzcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMi5qcGciLCJzbGlkZXNob3ciOiJhbGwtMGJhYmVkMSJ9">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-2.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
							<a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="<?= $asset_theme ?>photos/Platinum-2-Galeri-12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0babed1" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NTA1ODcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wMVwvUGxhdGludW0tMi1HYWxlcmktMTIuanBnIiwic2xpZGVzaG93IjoiYWxsLTBiYWJlZDEifQ%3D%3D">
					<div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="<?= $asset_theme ?>photos/Platinum-2-Galeri-12.jpg" data-width="683" data-height="1024" alt="" ></div>
											<div class="elementor-gallery-item__overlay"></div>
														</a>
					</div>
			</div>
				</div>
				<div class="elementor-element elementor-element-0b37877 animated-fast elementor-aspect-ratio-169 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-video" data-id="0b37877" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;<?= $youtube_prewedding ?>&quot;,&quot;yt_privacy&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;controls&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default">
				<div class="elementor-widget-container">
					<div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
			<div class="elementor-video"></div>		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-de85ddc elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="de85ddc" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-35ff2ff wdp-sticky-section-no" data-id="35ff2ff" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-cdc3d44 wdp-sticky-section-no" data-id="cdc3d44" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-bc9a068 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="bc9a068" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Photo by <a href="<?= $photo_by ?>" target="_blank">Loisenses</a></p>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b149486 wdp-sticky-section-no" data-id="b149486" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-5b6f814 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5b6f814" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-fbe21eb wdp-sticky-section-no" data-id="fbe21eb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8535df1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8535df1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat, <br>acara ini menerapkan</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-767b7e1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="767b7e1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-29067db wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="29067db" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">sesuai dengan peraturan dan <br>rekomendasi pemerintah.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-50fbc21 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="50fbc21" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-5779a05 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5779a05" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9d7cbd3 animated-slow wdp-sticky-section-no elementor-invisible" data-id="9d7cbd3" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-42b1831 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="42b1831" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="<?= $asset_theme ?>protocol/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Gunakan Masker</h3><p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara</p></div></div>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-aa3fb2e animated-slow wdp-sticky-section-no elementor-invisible" data-id="aa3fb2e" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-db074e6 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="db074e6" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="<?= $asset_theme ?>protocol/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Cuci Tangan</h3><p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier</p></div></div>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-2a42ccb elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2a42ccb" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-87d7fee animated-slow wdp-sticky-section-no elementor-invisible" data-id="87d7fee" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0bfcfb4 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="0bfcfb4" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="<?= $asset_theme ?>protocol/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Jaga Jarak </h3><p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undangan lainnya</p></div></div>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-82fd6a1 animated-slow wdp-sticky-section-no elementor-invisible" data-id="82fd6a1" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-deb732a elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="deb732a" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="<?= $asset_theme ?>protocol/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Tidak Berjabat Tangan</h3><p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya</p></div></div>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-4564fc4 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4564fc4" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;wave-brush&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-142cc33 wdp-sticky-section-no" data-id="142cc33" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-de10371 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="de10371" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<!-- <p class="elementor-heading-title elementor-size-default">Doa & Ucapan untuk</p> -->
			<p class="elementor-heading-title elementor-size-default">Doa untuk</p>
				</div>
				</div>
				<div class="elementor-element elementor-element-1e46efa wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1e46efa" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pasangan Mempelai
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-e2127a6 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="e2127a6" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-16891da wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="16891da" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Merupakan suatu kehormatan dan kebahagiaan bagi Kami apabila Bapak/ Ibu/ Saudara/ i berkenan hadir untuk memberikan doa restu kepada kedua mempelai.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-ccd9178 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="ccd9178" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section style="display: none" class="elementor-section elementor-inner-section elementor-element elementor-element-f51afd2 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="f51afd2" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6a36b19 animated-slow wdp-sticky-section-no elementor-invisible" data-id="6a36b19" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500,&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-ae90ba6 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="ae90ba6" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-10cc1da wdp-sticky-section-no" data-id="10cc1da" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-938b8e5 wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="938b8e5" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
				<div class="elementor-widget-container">
			
            <style>
                .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],.wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,.wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit]{height: auto !important;}
                .halooo{background-color:red !important;color:#fff !important;}
                .wdp-wrapper .wdp-wrap-link{text-align: center;padding: 18px;margin-top: 15px;}
                .wdp-wrapper .wdp-wrap-form{border-top:0px;}
                .konfirmasi-kehadiran{display: flex;justify-content: center;flex-direction: row;padding: 10px;margin-top: 10px;}
                .jenis-konfirmasi > i{margin: 0px 5px;}
                .jenis-konfirmasi{padding: 5px 10px;background: red;border-radius: 0.5em;color: #fff;margin:7px;}
            </style>
            <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                <div class='wdp-wrap-link'>
                    <a id='wdp-link-27002' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=27002&amp;comments=2&amp;get=0&amp;order=DESC' title='2 Ucapan'></a>
                    <div class='konfirmasi-kehadiran elementor-screen-only'>
                                    <div class='jenis-konfirmasi cswd-hadir'><i class='fas fa-check'></i> 1 Hadir</div>
                                    <div class='jenis-konfirmasi cswd-tidak-hadir'><i class='fas fa-times'></i> 1 Tidak Hadir</div>
                                </div>
                </div>
                <div id='wdp-wrap-commnent-27002' class='wdp-wrap-comments' style='display:none;'>
                
                    <div id='wdp-wrap-form-27002' class='wdp-wrap-form wdp-clearfix'>
                        <div id='wdp-container-form-27002' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                            <div id='respond-27002' class='respond wdp-clearfix'><form action='https://unityinvitation.com/wp-comments-post.php' method='post' id='commentform-27002'><p class="comment-form-author wdp-field-1"><input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" /><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span></p><div class="wdp-wrap-textarea"><textarea id="wdp-textarea-27002" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span></div>
        <div class="wdp-wrap-select"><select class="waci_comment wdp-select" name="konfirmasi"><option value="" disabled selected>Konfirmasi Kehadiran</option>></option><option value="Hadir">Hadir</option><option value="Tidak Hadir">Tidak Hadir</option></select><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span></div><div class='wdp-wrap-submit wdp-clearfix'><p class='form-submit'><span class="wdp-hide">Do not change these fields following</span><input type="text" class="wdp-hide" name="name" value="username"><input type="text" class="wdp-hide" name="nombre" value=""><input type="text" class="wdp-hide" name="form-wdp" value=""><input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal"><input name='submit' id='submit-27002' value='Kirim' type='submit' /><input type='hidden' name='commentpress' value='true' /><input type='hidden' name='comment_post_ID' value='27002' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p></div></form></div>
                        </div>
                    </div>
                <div id='wdp-comment-status-27002'  class='wdp-comment-status'></div><ul id='wdp-container-comment-27002' class='wdp-container-comments wdp-order-DESC  wdp-has-2-comments wdp-multiple-comments' data-order='DESC'></ul><div class='wdp-holder-27002 wdp-holder'></div>
                </div>
            </div>
        		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-c6977b7 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="c6977b7" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-7a27277 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7a27277" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">"Semoga Allah memberkahimu dan memberkahi apa yang menjadi tanggung jawabmu, serta menyatukan kalian berdua dalam kebaikan."
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-3c1093e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3c1093e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">(HR. Ahmad, at-Tirmidzi, an-Nasa'i,<br> Abu Dawud, dan Ibnu Majah)</p>		</div>
				</div>
				<div class="elementor-element elementor-element-067beb2 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="067beb2" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-97a7307 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="97a7307" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-8731a77 animated-slow wdp-sticky-section-no" data-id="8731a77" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_tablet&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8b01753 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8b01753" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Tanda Kasih</p>		</div>
				</div>
				<div class="elementor-element elementor-element-15fb9c3 wdp-sticky-section-no elementor-visible elementor-widget elementor-widget-heading" data-id="15fb9c3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Doa restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah. Tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentu semakin melengkapi kebahagiaan kami.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-fb04e8c wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="fb04e8c" data-element_type="widget" data-widget_type="html.default">
				<div class="elementor-widget-container">
			<script>
document.addEventListener('DOMContentLoaded', function() {
jQuery(function($){
$('.clicktoshow').each(function(i){
$(this).click(function(){ $('.showclick').eq(i).toggle();
$('.clicktoshow');
$('.showclick2').eq(i).hide();
}); });
}); });
</script>
<style>
.clicktoshow{
cursor: pointer;
}
.showclick{
display: none;
}
</style>



<script>
document.addEventListener('DOMContentLoaded', function() {
jQuery(function($){
$('.clicktoshow2').each(function(i){
$(this).click(function(){ $('.showclick2').eq(i).toggle();
$('.clicktoshow2');
}); });
}); });
</script>
<style>
.clicktoshow2{
cursor: pointer;
}
.showclick2{
display: none;
}
</style>		</div>
				</div>
				<div class="elementor-element elementor-element-e5be361 elementor-align-center clicktoshow wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="e5be361" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</span>
						<span class="elementor-button-text">Klik di sini</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-003a823 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="003a823" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://api.whatsapp.com/send?phone=<?= $gift_confirmation_wa ?>&#038;text=<?= $text_gift_confirm ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fab fa-whatsapp"></i>			</span>
						<span class="elementor-button-text">Konfirmasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9aa88af wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="9aa88af" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-b0e1a05 showclick animated-slow elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="b0e1a05" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-cb6b7bc wdp-sticky-section-no" data-id="cb6b7bc" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-b2e2b03 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="b2e2b03" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="400" height="150" src="<?= $asset_theme ?>bank/Logo-Bank.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>bank/Logo-Bank.png 400w, <?= $asset_theme ?>bank/Logo-Bank-300x113.png 300w" sizes="(max-width: 400px) 100vw, 400px" />															</div>
				</div>
				<div class="elementor-element elementor-element-70d2ee1 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="70d2ee1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n <?= $transfer_gift_user ?></div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent"><?= $transfer_gift_rekening ?></div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
				<div class="elementor-element elementor-element-77eddf6 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="77eddf6" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-45200e8 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="45200e8" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-8e8b737 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="8e8b737" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n <?= $send_gift_user ?></div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent"><?= $send_gift_address ?></div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-ea88ef1 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ea88ef1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;wave-brush&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7	s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7	c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3	c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6	c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7	C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5	c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1	c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7	c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6	C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8	c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2	C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3	C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1	z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1	c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z"/>
	<path class="elementor-shape-fill" d="M269.6,18c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C267.7,18.8,269.7,18,269.6,18z"/>
	<path class="elementor-shape-fill" d="M227.4,9.8c-0.2-0.1-4.5-1-9.5-1.2c-5-0.2-12.7,0.6-12.3,0.5c0.3-0.1,5.9-1.8,13.3-1.2	S227.6,9.9,227.4,9.8z"/>
	<path class="elementor-shape-fill" d="M204.5,13.4c-0.1-0.1,2-1,3.2-1.1c1.2-0.1,2,0,2,0.3c0,0.3-0.1,0.5-1.6,0.4	C206.4,12.9,204.6,13.5,204.5,13.4z"/>
	<path class="elementor-shape-fill" d="M201,10.6c0-0.1-4.4,1.2-6.3,2.2c-1.9,0.9-6.2,3.1-6.1,3.1c0.1,0.1,4.2-1.6,6.3-2.6	S201,10.7,201,10.6z"/>
	<path class="elementor-shape-fill" d="M154.5,26.7c-0.1-0.1-4.6,0.3-7.2,0c-7.3-0.7-17-3.2-16.6-2.9c0.4,0.3,13.7,3.1,17,3.3	C152.6,27.5,154.6,26.8,154.5,26.7z"/>
	<path class="elementor-shape-fill" d="M41.9,19.3c0,0,1.2-0.3,2.9-0.1c1.7,0.2,5.8,0.9,8.2,0.7c4.2-0.4,7.4-2.7,7-2.6	c-0.4,0-4.3,2.2-8.6,1.9c-1.8-0.1-5.1-0.5-6.7-0.4S41.9,19.3,41.9,19.3z"/>
	<path class="elementor-shape-fill" d="M75.5,12.6c0.2,0.1,2-0.8,4.3-1.1c2.3-0.2,2.1-0.3,2.1-0.5c0-0.1-1.8-0.4-3.4,0	C76.9,11.5,75.3,12.5,75.5,12.6z"/>
	<path class="elementor-shape-fill" d="M15.6,13.2c0-0.1,4.3,0,6.7,0.5c2.4,0.5,5,1.9,5,2c0,0.1-2.7-0.8-5.1-1.4	C19.9,13.7,15.7,13.3,15.6,13.2z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f09a420 wdp-sticky-section-no" data-id="f09a420" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-e27139f elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="e27139f" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-5fe2129 animated-fast wdp-sticky-section-no elementor-invisible" data-id="5fe2129" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-277ea33 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="277ea33" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Terima Kasih</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-e087d53 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e087d53" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default"><?= $groom ?> & <?= $bride ?></h1>		</div>
				</div>
				<div class="elementor-element elementor-element-adb4295 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="adb4295" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Keluarga Besar</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-700d116 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="700d116" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Bapak <?= $groom_father ?> 
& Ibu <?= $groom_mother ?> 
<br>
Bapak <?= $bride_father ?> 
& Ibu <?= $bride_mother ?></p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-1c78ade wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="1c78ade" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>

				<br><br><br><br><br><br>
				<div class="elementor-element elementor-element-700d116 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="adb4295" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
					<div class="elementor-widget-container">
						<p class="elementor-heading-title elementor-size-default">
							Made With ♥ by <a href="<?= $base_url ?>">Ara Invitation</a>
						</p>
					</div>
				</div>

				<!-- <section class="elementor-section elementor-inner-section elementor-element elementor-element-d1be672 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="d1be672" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
					<div class="elementor-container elementor-column-gap-default">
						<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-eb4c2c3 wdp-sticky-section-no" data-id="eb4c2c3" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
						<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-ebe347a wdp-sticky-section-no" data-id="ebe347a" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
						<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-1069ae2 wdp-sticky-section-no" data-id="1069ae2" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
						<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-705ce51 animated-slow wdp-sticky-section-no elementor-invisible" data-id="705ce51" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
							<div class="elementor-widget-wrap elementor-element-populated_">
								<div class="elementor-element elementor-element-fd099f3 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="fd099f3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
									<div class="elementor-widget-container">
										<a href="<?= $base_url ?>" target="_blank">
											<img width="768" height="589" src="<?= $asset_theme ?>logo/LogosWhite-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>logo/LogosWhite-768x589.png 768w, <?= $asset_theme ?>logo/LogosWhite-300x230.png 300w, <?= $asset_theme ?>logo/LogosWhite.png 779w" sizes="(max-width: 768px) 100vw, 768px" />								
										</a>
									</div>
								</div>
								<div class="elementor-element elementor-element-8c04b40 elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="8c04b40" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
									<div class="elementor-widget-container">
										<div class="elementor-social-icons-wrapper elementor-grid">
											<span class="elementor-grid-item">
												<a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=<?= $admin_wa ?>&#038;text=<?= $template_question_wa ?>" target="_blank" rel="noopener">
													<span class="elementor-screen-only">Whatsapp</span>
													<i class="fab fa-whatsapp"></i>					
												</a>
											</span>
											<span class="elementor-grid-item">
												<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/<?= $admin_ig ?>/" target="_blank" rel="noopener">
													<span class="elementor-screen-only">Instagram</span>
													<i class="fab fa-instagram"></i>					
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-54058b8 wdp-sticky-section-no" data-id="54058b8" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
						<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-42257b1 wdp-sticky-section-no" data-id="42257b1" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
						<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-12446a0 wdp-sticky-section-no" data-id="12446a0" data-element_type="column">
							<div class="elementor-widget-wrap">
							</div>
						</div>
					</div>
				</section> -->
				<div class="elementor-element elementor-element-18832ee elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="18832ee" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
				<div class="elementor-widget-container">
					
		<script>
			var settingAutoplay = 'yes';
			window.settingAutoplay = settingAutoplay === 'disable' ? !false : !true;
		</script>

		<div id="audio-container" class="audio-box">			
			<style>
				.play_cswd_audio{
				animation-name: putar;
				animation-duration: 5000ms;
				animation-iteration-count: infinite;
				animation-timing-function: linear; 
				}
				@keyframes putar{
					from {
						transform:rotate(0deg);
					}
					to {
						transform:rotate(360deg);
					}
				}
			</style>
			<audio id="song" loop>
				<source src="<?= $asset ?><?= $backsound ?>"
                type="audio/mp3">
			</audio>  

			<div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
				<div class="elementor-icon">
				
				<img src="<?= $asset_theme ?>icon/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
				</div>
			</div> 

			<div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
				<div class="elementor-icon">
				<img src="<?= $asset_theme ?>icon/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
				</div>
			</div>
			
		</div>
		<!-- <script>
			jQuery("document").ready(function (n) {
				var e = window.settingAutoplay;
				e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
					n("#audio-container").click(function (u) {
						e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
					});
			});
		</script> -->

		<script>
			function openInvitation() {
				document.getElementById("unmute-sound").style.display = 'none';
				document.getElementById("mute-sound").style.display = 'block';
			}
		</script>
		
				</div>
				</div>
				<div class="elementor-element elementor-element-7a5ed49 wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="7a5ed49" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
				<div class="elementor-widget-container">
			        <style>
            .elementor-image>img {
                display: initial !important;
            }
        </style>
        <div class="modalx animated " data-sampul='<?= $asset_theme ?>photos/Asset-Platinum-2-cover1.jpg'>

            <div class="overlayy"></div>
            <div class="content-modalx">
                <div class="info_modalx">
                                            <div class="elementor-image img"><img src="<?= $asset_theme ?>photos/Asset-Platinum-2-cover2.png" title="Asset Platinum 2 cover2" alt="Asset Platinum 2 cover2" /></div>
                                                                <div class="text_tambahan" >PERNIKAHAN</div>
                                                                <div class="wdp-mempelai" ><?= $groom ?> & <?= $bride ?></div>
                                                                <div class="wdp-dear" >Yth. Bapak/Ibu/Saudara/i</div>
                                        <div class="wdp-name">
                        <?= $to ?>                    </div>
                                                                <div class="wdp-text" >Tanpa mengurangi rasa hormat, <br> Kami mengundang  Bapak/Ibu/Saudara/i <br>untuk hadir  di acara Pernikahan Kami.
</div>
                                                                <div class="wdp-button-wrapper">
                            <button class="elementor-button" onclick="openInvitation()">
                                                                Buka Undangan                            </button>
                        </div>
                                    </div>
            </div>
        </div>


        <script>
            const sampul = jQuery('.modalx').data('sampul');
            jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
            jQuery('body').css('overflow', 'hidden');
            jQuery('.wdp-button-wrapper button').on('click', function() {
				openInvitation();
				jQuery('.modalx').removeClass('');
				jQuery('.modalx').addClass('fadeIn reverse');
				setTimeout(function() {
					jQuery('.modalx').addClass('removeModals');
				}, 1600);
				jQuery('body').css('overflow', 'auto');
                document.getElementById("song").play();
            });
        </script>
		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
							</div>
				<div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami.
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
				<div class="elementor-widget-container">
					<div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
			<div class="jet-tabs__control-wrapper">
				<div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="<?= $asset_theme ?>Logo-BCA.png" alt=""></div></div><div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="<?= $asset_theme ?>Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt=""></div></div><div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="<?= $asset_theme ?>bank/Logo-Bank.png" alt=""></div></div>			</div>
			<div class="jet-tabs__content-wrapper">
				<div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="<?= $asset_theme ?>dana/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>dana/Dana-Unity.png 486w, <?= $asset_theme ?>dana/Dana-Unity-300x300.png 300w, <?= $asset_theme ?>dana/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="<?= $asset_theme ?>dana/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>dana/Dana-Unity.png 486w, <?= $asset_theme ?>dana/Dana-Unity-300x300.png 300w, <?= $asset_theme ?>dana/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="<?= $asset_theme ?>dana/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset_theme ?>dana/Dana-Unity.png 486w, <?= $asset_theme ?>dana/Dana-Unity-300x300.png 300w, <?= $asset_theme ?>dana/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div>			</div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Bimo Bramantyo</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent"><?= $venue_address ?></div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
				<div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-no">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
				<div class="elementor-widget-container">
			
				<div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
								<nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}"><ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul></nav>
															<div class="pp-menu-toggle pp-menu-toggle-on-tablet">
											<div class="pp-hamburger">
							<div class="pp-hamburger-box">
																	<div class="pp-hamburger-inner"></div>
															</div>
						</div>
														</div>
												<nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
												<ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul>							</nav>
							</div>
						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		<link rel='stylesheet' id='ep-countdown-css'  href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-countdown.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-gallery-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/e-gallery/css/e-gallery.min.css?ver=1.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51208-css'  href='<?= $asset ?>wp-content/uploads/elementor/css/post-51208.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='e-animations-css'  href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
<script type='text/javascript' id='WEDKU_SCRP-js-extra'>
/* <![CDATA[ */
var ceper = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.16' id='WEDKU_SCRP-js'></script>
<script type='text/javascript' id='wdp_js_script-js-extra'>
/* <![CDATA[ */
var WDP_WP = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","wdpNonce":"4bda080e1a","jpages":"true","jPagesNum":"5","textCounter":"true","textCounterNum":"500","widthWrap":"","autoLoad":"true","thanksComment":"Terima kasih atas ucapan & doanya!","thanksReplyComment":"Terima kasih atas balasannya!","duplicateComment":"You might have left one of the fields blank, or duplicate comments","insertImage":"Insert image","insertVideo":"Insert video","insertLink":"Insert link","checkVideo":"Check video","accept":"Accept","cancel":"Cancel","reply":"Balas","textWriteComment":"Tulis Ucapan & Doa","classPopularComment":"wdp-popular-comment","textUrlImage":"Url image","textUrlVideo":"Url video youtube or vimeo","textUrlLink":"Url link","textToDisplay":"Text to display","textCharacteresMin":"Minimal 2 karakter","textNavNext":"Selanjutnya","textNavPrev":"Sebelumnya","textMsgDeleteComment":"Do you want delete this comment?","textLoadMore":"Load more","textLikes":"Likes"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
<script type='text/javascript' id='a33f19b0d-js-extra'>
/* <![CDATA[ */
var localize = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"667cbfe6aa","i18n":{"added":"Added ","compare":"Compare","loading":"Loading..."},"page_permalink":"https:\/\/unityinvitation.com\/unity-platinum-2\/"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/uploads/essential-addons-elementor/734e5f942.min.js?ver=1652086788' id='a33f19b0d-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/pp-bg-effects.min.js?ver=1.0.0' id='pp-bg-effects-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/particles/particles.min.js?ver=2.0.0' id='particles-js'></script>
<script type='text/javascript' id='bdt-uikit-js-extra'>
/* <![CDATA[ */
var element_pack_ajax_login_config = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","language":"en","loadingmessage":"Sending user info, please wait...","unknownerror":"Unknown error, make sure access is correct!"};
var ElementPackConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"268d485f92","data_table":{"language":{"lengthMenu":"Show _MENU_ Entries","info":"Showing _START_ to _END_ of _TOTAL_ entries","search":"Search :","paginate":{"previous":"Previous","next":"Next"}}},"contact_form":{"sending_msg":"Sending message please wait...","captcha_nd":"Invisible captcha not defined!","captcha_nr":"Could not get invisible captcha response!"},"mailchimp":{"subscribing":"Subscribing you please wait..."},"elements_data":{"sections":[],"columns":[],"widgets":[]}};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.6.4","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"theme_builder_v2":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true,"form-submissions":true},"urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"viewport_tablet":1024,"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes"},"post":{"id":27002,"title":"Unity%20Platinum%202","excerpt":"","featuredImage":"https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/10\/Platinum-2-cover-invitation.jpg"}};
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-countdown.min.js?ver=6.0.10' id='ep-countdown-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
<script type='text/javascript' id='powerpack-frontend-js-extra'>
/* <![CDATA[ */
var ppLogin = {"empty_username":"Enter a username or email address.","empty_password":"Enter password.","empty_password_1":"Enter a password.","empty_password_2":"Re-enter password.","empty_recaptcha":"Please check the captcha to verify you are not a robot.","email_sent":"A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.","reset_success":"Your password has been reset successfully.","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
var ppRegistration = {"invalid_username":"This username is invalid because it uses illegal characters. Please enter a valid username.","username_exists":"This username is already registered. Please choose another one.","empty_email":"Please type your email address.","invalid_email":"The email address isn\u2019t correct!","email_exists":"The email is already registered, please choose another one.","password":"Password must not contain the character \"\\\\\"","password_length":"Your password should be at least 8 characters long.","password_mismatch":"Password does not match.","invalid_url":"URL seems to be invalid.","recaptcha_php_ver":"reCAPTCHA API requires PHP version 5.3 or above.","recaptcha_missing_key":"Your reCAPTCHA Site or Secret Key is missing!","show_password":"Show password","hide_password":"Hide password","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min.js?ver=1.2.0' id='elementor-gallery-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"f02c3dc1b5","urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/","rest":"https:\/\/unityinvitation.com\/wp-json\/"},"i18n":{"toc_no_headings_found":"No headings were found on this page."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
<script type='text/javascript' id='jet-elements-js-extra'>
/* <![CDATA[ */
var jetElements = {"ajaxUrl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template","devMode":"false","messages":{"invalidMail":"Please specify a valid e-mail"}};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
<script type='text/javascript' id='jet-tabs-frontend-js-extra'>
/* <![CDATA[ */
var JetTabsSettings = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template","devMode":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
<script type='text/javascript' id='weddingpress-wdp-js-extra'>
/* <![CDATA[ */
var cevar = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","plugin_url":"https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"};
/* ]]> */
</script>
<script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
<div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div><div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>	</body>
</html>
