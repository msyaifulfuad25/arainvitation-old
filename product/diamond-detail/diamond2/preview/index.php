<?php
	include "data-sap.php";
	include "data-client.php";

    $url_save_sap = $base_url.$uri_couple.'/save-sap.php';
    $url_get_sap = $base_url.$uri_couple.'/get-sap.php';
	// Untuk Development Product
	$asset_theme = $base_url.'product/platinum-detail/platinum1/preview/assets/';
	// Untuk Production
	// $asset_theme = $base_url.'satria-erika/assets/';
	$theme_name = 'Ara Platinum 1';
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      .wdp-comment-text img {
        max-width: 100% !important;
      }
    </style>
    <!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
    <title>Unity Diamond 2</title>
    <meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Unity Diamond 2" />
    <meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta property="og:url" content="https://unityinvitation.com/unity-diamond-2/" />
    <meta property="og:site_name" content="Unity Invitation" />
    <meta property="article:section" content="Undangan" />
    <meta property="og:updated_time" content="2022-05-19T22:48:59+07:00" />
    <meta property="og:image" content="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-2-cover-invitation.jpg" />
    <meta property="og:image:secure_url" content="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-2-cover-invitation.jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />
    <meta property="og:image:alt" content="Unity Diamond 2" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Unity Diamond 2" />
    <meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="twitter:image" content="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-2-cover-invitation.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="admin" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="3 minutes" />
    <script type="application/ld+json" class="rank-math-schema">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": "BreadcrumbList",
          "@id": "https://unityinvitation.com/unity-diamond-2/#breadcrumb",
          "itemListElement": [{
            "@type": "ListItem",
            "position": "1",
            "item": {
              "@id": "https://unityinvitation.com",
              "name": "Home"
            }
          }, {
            "@type": "ListItem",
            "position": "2",
            "item": {
              "@id": "https://unityinvitation.com/unity-diamond-2/",
              "name": "Unity Diamond 2"
            }
          }]
        }]
      }
    </script>
    <!-- /Rank Math WordPress SEO plugin -->
    <link rel='dns-prefetch' href='//use.fontawesome.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Feed" href="https://unityinvitation.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Comments Feed" href="https://unityinvitation.com/comments/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Unity Diamond 2 Comments Feed" href="https://unityinvitation.com/unity-diamond-2/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
        "svgExt": ".svg",
        "source": {
          "concatemoji": "https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.0"
        }
      };
      /*! This file is auto-generated */
      ! function(e, a, t) {
        var n, r, o, i = a.createElement("canvas"),
          p = i.getContext && i.getContext("2d");

        function s(e, t) {
          var a = String.fromCharCode,
            e = (p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0), i.toDataURL());
          return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
        }

        function c(e) {
          var t = a.createElement("script");
          t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (o = Array("flag", "emoji"), t.supports = {
            everything: !0,
            everythingExceptFlag: !0
          }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
          if (!p || !p.fillText) return !1;
          switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
            case "flag":
              return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
            case "emoji":
              return !s([129777, 127995, 8205, 129778, 127999], [129777, 127995, 8203, 129778, 127999])
          }
          return !1
        }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
          t.DOMReady = !0
        }, t.supports.everything || (n = function() {
          t.readyCallback()
        }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
          "complete" === a.readyState && t.readyCallback()
        })), (e = t.source || {}).concatemoji ? c(e.concatemoji) : e.wpemoji && e.twemoji && (c(e.twemoji), c(e.wpemoji)))
      }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='bdt-uikit-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ep-helper-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css' href='https://unityinvitation.com/wp-includes/css/dist/block-library/style.min.css?ver=6.0' type='text/css' media='all' />
    <style id='global-styles-inline-css' type='text/css'>
      body {
        --wp--preset--color--black: #000000;
        --wp--preset--color--cyan-bluish-gray: #abb8c3;
        --wp--preset--color--white: #ffffff;
        --wp--preset--color--pale-pink: #f78da7;
        --wp--preset--color--vivid-red: #cf2e2e;
        --wp--preset--color--luminous-vivid-orange: #ff6900;
        --wp--preset--color--luminous-vivid-amber: #fcb900;
        --wp--preset--color--light-green-cyan: #7bdcb5;
        --wp--preset--color--vivid-green-cyan: #00d084;
        --wp--preset--color--pale-cyan-blue: #8ed1fc;
        --wp--preset--color--vivid-cyan-blue: #0693e3;
        --wp--preset--color--vivid-purple: #9b51e0;
        --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
        --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
        --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
        --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
        --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
        --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
        --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
        --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
        --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
        --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
        --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
        --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
        --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
        --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
        --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
        --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
        --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
        --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
        --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
        --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
        --wp--preset--font-size--small: 13px;
        --wp--preset--font-size--medium: 20px;
        --wp--preset--font-size--large: 36px;
        --wp--preset--font-size--x-large: 42px;
      }

      .has-black-color {
        color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-color {
        color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-color {
        color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-color {
        color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-color {
        color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-color {
        color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-color {
        color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-color {
        color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-color {
        color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-color {
        color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-color {
        color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-color {
        color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-background-color {
        background-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-background-color {
        background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-background-color {
        background-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-background-color {
        background-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-background-color {
        background-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-background-color {
        background-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-background-color {
        background-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-background-color {
        background-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-background-color {
        background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-background-color {
        background-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-border-color {
        border-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-border-color {
        border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-border-color {
        border-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-border-color {
        border-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-border-color {
        border-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-border-color {
        border-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-border-color {
        border-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-border-color {
        border-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-border-color {
        border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-border-color {
        border-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
        background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
      }

      .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
        background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
      }

      .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-orange-to-vivid-red-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
      }

      .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
        background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
      }

      .has-cool-to-warm-spectrum-gradient-background {
        background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
      }

      .has-blush-light-purple-gradient-background {
        background: var(--wp--preset--gradient--blush-light-purple) !important;
      }

      .has-blush-bordeaux-gradient-background {
        background: var(--wp--preset--gradient--blush-bordeaux) !important;
      }

      .has-luminous-dusk-gradient-background {
        background: var(--wp--preset--gradient--luminous-dusk) !important;
      }

      .has-pale-ocean-gradient-background {
        background: var(--wp--preset--gradient--pale-ocean) !important;
      }

      .has-electric-grass-gradient-background {
        background: var(--wp--preset--gradient--electric-grass) !important;
      }

      .has-midnight-gradient-background {
        background: var(--wp--preset--gradient--midnight) !important;
      }

      .has-small-font-size {
        font-size: var(--wp--preset--font-size--small) !important;
      }

      .has-medium-font-size {
        font-size: var(--wp--preset--font-size--medium) !important;
      }

      .has-large-font-size {
        font-size: var(--wp--preset--font-size--large) !important;
      }

      .has-x-large-font-size {
        font-size: var(--wp--preset--font-size--x-large) !important;
      }
    </style>
    <link rel='stylesheet' id='WEDKU_STYLE-css' href='https://unityinvitation.com/wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.18' type='text/css' media='all' />
    <link rel='stylesheet' id='pafe-extension-style-css' href='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp_style-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
    <style id='wdp_style-inline-css' type='text/css'>
      .wdp-wrapper {
        font-size: 14px
      }

      .wdp-wrapper {
        background: #ffffff;
      }

      .wdp-wrapper.wdp-border {
        border: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-comments a:link,
      .wdp-wrapper .wdp-wrap-comments a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {
        border-color: #64B6EC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        color: #FFFFFF;
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        background: #a3a3a3;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {
        color: #44525F;
      }

      .wdp-wrapper .wdp-media-btns a>span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-media-btns a>span:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        color: #319342;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {
        color: #C85951;
      }

      .wdp-wrapper .wdp-comment-status.wdp-loading>span {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-bottom: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #54595f !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #2a5782 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {
        color: #2a5782;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span:hover {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {
        color: #2C9E48;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {
        color: #D13D3D;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-counter-info {
        color: #9DA8B7;
      }

      .wdp-wrapper .wdp-holder span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a,
      .wdp-wrapper .wdp-holder a:link,
      .wdp-wrapper .wdp-holder a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a:hover,
      .wdp-wrapper .wdp-holder a:link:hover,
      .wdp-wrapper .wdp-holder a:visited:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {
        max-width: 28px;
        max-height: 28px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {
        margin-left: 38px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {
        max-width: 24px;
        max-height: 24px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {
        max-width: 21px;
        max-height: 21px;
      }
    </style>
    <link rel='stylesheet' id='wdp-centered-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-horizontal-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-fontello-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='exad-main-style-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-css' href='https://unityinvitation.com/wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-theme-style-css' href='https://unityinvitation.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-css' href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-skin-css' href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1399-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-1399.css?ver=1655470780' type='text/css' media='all' />
    <link rel='stylesheet' id='powerpack-frontend-css' href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='weddingpress-wdp-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css' href='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='uael-frontend-css' href='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-tabs-frontend-css' href='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-29833-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-29833.css?ver=1655472595' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-51207-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51207.css?ver=1655470781' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-31584-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-31584.css?ver=1655470781' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-css' href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <link rel='stylesheet' id='loftloader-lite-animation-css' href='https://unityinvitation.com/wp-content/plugins/loftloader/assets/css/loftloader.min.css?ver=2022022501' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-v4shim-css' href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
    <style id='font-awesome-official-v4shim-inline-css' type='text/css'>
      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
        unicode-range: U+F004-F005, U+F007, U+F017, U+F022, U+F024, U+F02E, U+F03E, U+F044, U+F057-F059, U+F06E, U+F070, U+F075, U+F07B-F07C, U+F080, U+F086, U+F089, U+F094, U+F09D, U+F0A0, U+F0A4-F0A7, U+F0C5, U+F0C7-F0C8, U+F0E0, U+F0EB, U+F0F3, U+F0F8, U+F0FE, U+F111, U+F118-F11A, U+F11C, U+F133, U+F144, U+F146, U+F14A, U+F14D-F14E, U+F150-F152, U+F15B-F15C, U+F164-F165, U+F185-F186, U+F191-F192, U+F1AD, U+F1C1-F1C9, U+F1CD, U+F1D8, U+F1E3, U+F1EA, U+F1F6, U+F1F9, U+F20A, U+F247-F249, U+F24D, U+F254-F25B, U+F25D, U+F267, U+F271-F274, U+F279, U+F28B, U+F28D, U+F2B5-F2B6, U+F2B9, U+F2BB, U+F2BD, U+F2C1-F2C2, U+F2D0, U+F2D2, U+F2DC, U+F2ED, U+F328, U+F358-F35B, U+F3A5, U+F3D1, U+F410, U+F4AD;
      }
    </style>
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLora%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLibre+Baskerville%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-wedding-css' href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/wedding/css/wedding.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-unityinv-css' href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-regular-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
    <script type='text/javascript' id='jquery-core-js-extra'>
      /* 
					<![CDATA[ */
      var pp = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
    <link rel="https://api.w.org/" href="https://unityinvitation.com/wp-json/" />
    <link rel="alternate" type="application/json" href="https://unityinvitation.com/wp-json/wp/v2/posts/29833" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://unityinvitation.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://unityinvitation.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0" />
    <link rel='shortlink' href='https://unityinvitation.com/?p=29833' />
    <link rel="alternate" type="application/json+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-diamond-2%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-diamond-2%2F&#038;format=xml" />
    <link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-180x180.png" />
    <meta name="msapplication-TileImage" content="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-270x270.png" />
    <style>
      .pswp.pafe-lightbox-modal {
        display: none;
      }
    </style>
    <style id="loftloader-lite-custom-bg-color">
      #loftloader-wrapper .loader-section {
        background: #ffffff;
      }
    </style>
    <style id="loftloader-lite-custom-bg-opacity">
      #loftloader-wrapper .loader-section {
        opacity: 1;
      }
    </style>
    <style id="loftloader-lite-custom-loader">
      #loftloader-wrapper.pl-imgloading #loader {
        width: 70px;
      }

      #loftloader-wrapper.pl-imgloading #loader span {
        background-size: cover;
        background-image: url(https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png);
      }
    </style>
    <style type="text/css" id="wp-custom-css">
      .elementor-section {
        overflow: hidden;
      }

      .justify-cstm .pp-timeline-card {
        text-align: justify !important;
      }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
  </head>
  <body data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-29833 single-format-standard loftloader-lite-enabled elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-29833">
    <div id="loftloader-wrapper" class="pl-imgloading" data-show-close-time="15000" data-max-load-time="10000">
      <div class="loader-inner">
        <div id="loader">
          <div class="imgloading-container">
            <span style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png);"></span>
          </div>
          <img width="70" height="43" data-no-lazy="1" class="skip-lazy" alt="loader image" src="https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png">
        </div>
      </div>
      <div class="loader-section section-fade"></div>
      <div class="loader-close-button" style="display: none;">
        <span class="screen-reader-text">Close</span>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-dark-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0.49803921568627" />
            <feFuncG type="table" tableValues="0 0.49803921568627" />
            <feFuncB type="table" tableValues="0 0.49803921568627" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-red">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 0.27843137254902" />
            <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-midnight">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0" />
            <feFuncG type="table" tableValues="0 0.64705882352941" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-magenta-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.78039215686275 1" />
            <feFuncG type="table" tableValues="0 0.94901960784314" />
            <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-green">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.44705882352941 0.4" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-orange">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.098039215686275 1" />
            <feFuncG type="table" tableValues="0 0.66274509803922" />
            <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <div data-elementor-type="wp-post" data-elementor-id="29833" class="elementor elementor-29833">
      <section class="pp-bg-effects pp-bg-effects-2fce601b elementor-section elementor-top-section elementor-element elementor-element-2fce601b elementor-section-stretched pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="2fce601b" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="#D9C6A8" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="15" data-part-speed="" data-part-direction="none" data-hover-effect="grab" data-hover-size="" data-id="2fce601b" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;slideshow&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:46054,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-2-home1-scaled.jpg&quot;},{&quot;id&quot;:46055,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-2-home2-scaled.jpg&quot;},{&quot;id&quot;:46056,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-2-home3-scaled.jpg&quot;}],&quot;background_slideshow_transition_duration&quot;:1500,&quot;background_slideshow_slide_duration&quot;:2500,&quot;sticky&quot;:&quot;top&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-20f7f125 wdp-sticky-section-no" data-id="20f7f125" data-element_type="column" id="home" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-74462c14 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="74462c14" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default">PERNIKAHAN</h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-7d482c70 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7d482c70" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h1 class="elementor-heading-title elementor-size-default">Aqeela & Gibran</h1>
                </div>
              </div>
              <div class="elementor-element elementor-element-33066f06 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="33066f06" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-2a4d85fd animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2a4d85fd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default">31 . 12 . 2022</h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-bde98cc elementor-view-default elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="bde98cc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon-box.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-box-wrapper">
                    <div class="elementor-icon-box-icon">
                      <span class="elementor-icon elementor-animation-">
                        <i aria-hidden="true" class="fas fa-angle-double-up"></i>
                      </span>
                    </div>
                    <div class="elementor-icon-box-content">
                      <h3 class="elementor-icon-box-title">
                        <span> Swipe Up </span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7545a443 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7545a443" data-element_type="section" data-settings="{&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2e01f5c5 wdp-sticky-section-no" data-id="2e01f5c5" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-77f741b9 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="77f741b9" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-6913e174 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6913e174" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-656aba2d wdp-sticky-section-no" data-id="656aba2d" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-1a90c1c7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1a90c1c7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-75fe3989 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="75fe3989" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-f0cb91a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f0cb91a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Maha Suci Allah Subhanahu wa Ta'ala yang telah menciptakan makhluk-Nya berpasang-pasangan. Ya Allah, perkenankanlah dan Ridhoilah Pernikahan Kami.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-657478ac wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="657478ac" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6bca783f elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6bca783f" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-7b87dc00 animated-slow wdp-sticky-section-no elementor-invisible" data-id="7b87dc00" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-18a61575 elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="18a61575" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-52cd5b07 animated-slow wdp-sticky-section-no elementor-invisible" data-id="52cd5b07" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-66fa74d0 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="66fa74d0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="439" height="720" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPW.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPW.png 439w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPW-183x300.png 183w" sizes="(max-width: 439px) 100vw, 439px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-5209e6e6 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5209e6e6" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <h3 class="elementor-heading-title elementor-size-default">Aqeela</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-73d60cd0 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="73d60cd0" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Rahma Aqeela </h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-69b30a94 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="69b30a94" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-50a65471 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="50a65471" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Putri Kedua</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-96d871f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="96d871f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak Bob Aqeela <br> & <br> Ibu Dini Aminati </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-272031dc e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="272031dc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4ca558d animated-slow wdp-sticky-section-no" data-id="4ca558d" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-5ae25271 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5ae25271" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">&</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-58e8ec1b animated-slow wdp-sticky-section-no elementor-invisible" data-id="58e8ec1b" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-5e9acb68 elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="5e9acb68" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-438307fe animated-slow wdp-sticky-section-no elementor-invisible" data-id="438307fe" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-7c7f80a0 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="7c7f80a0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="439" height="720" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPP.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPP.png 439w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-CPP-183x300.png 183w" sizes="(max-width: 439px) 100vw, 439px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-43bcf3e8 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="43bcf3e8" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <h3 class="elementor-heading-title elementor-size-default">Gibran</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-32b22bdf wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="32b22bdf" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Chairil Gibran</h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6dbc6908 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6dbc6908" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-29657dc4 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="29657dc4" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Putra Ketiga</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-16020b25 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="16020b25" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak Ahmad Gibran <br>& <br> Ibu Laksmini </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-72ee84b3 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="72ee84b3" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-d50245e wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="d50245e" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-325710c9 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="325710c9" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="couple" class="elementor-menu-anchor"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-47085381 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="47085381" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6169e64a wdp-sticky-section-no" data-id="6169e64a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6a31fb9a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6a31fb9a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-45b37534 wdp-sticky-section-no" data-id="45b37534" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-4ed465a7 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="4ed465a7" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:10,&quot;end&quot;:55}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="298" height="339" src="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-2-1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-2-1.png 298w, https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-2-1-264x300.png 264w" sizes="(max-width: 298px) 100vw, 298px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2d968b8e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2d968b8e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hitung Mundur</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-27f457ac wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="27f457ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-18db15e9 bdt-countdown--align-center bdt-countdown--label-block wdp-sticky-section-no elementor-widget elementor-widget-bdt-countdown" data-id="18db15e9" data-element_type="widget" data-widget_type="bdt-countdown.default">
                        <div class="elementor-widget-container">
                          <div class="bdt-countdown-wrapper bdt-countdown-skin-default" data-settings="{&quot;id&quot;:&quot;#bdt-countdown-18db15e9&quot;,&quot;msgId&quot;:&quot;#bdt-countdown-msg-18db15e9&quot;,&quot;adminAjaxUrl&quot;:&quot;https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php&quot;,&quot;endActionType&quot;:&quot;none&quot;,&quot;redirectUrl&quot;:null,&quot;redirectDelay&quot;:1000,&quot;finalTime&quot;:&quot;2022-12-30T17:00:00+00:00&quot;,&quot;wpCurrentTime&quot;:1655780340,&quot;endTime&quot;:1672419600,&quot;loopHours&quot;:false,&quot;isLogged&quot;:false,&quot;couponTrickyId&quot;:&quot;bdt-sf-18db15e9&quot;,&quot;triggerId&quot;:false}">
                            <div id="bdt-countdown-18db15e9-timer" class="bdt-grid bdt-grid-small bdt-child-width-1-4 bdt-child-width-1-2@s bdt-child-width-1-4@m" data-bdt-countdown="date: 2022-12-30T17:00:00+00:00" data-bdt-grid="" style="x">
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-days-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-days bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Hari</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-hours-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-hours bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Jam</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-minutes-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-minutes bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Menit</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-seconds-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-seconds bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Detik</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-208d66f4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="208d66f4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">"Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir."</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7d545297 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7d545297" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">- Q.S. Ar-Rum : 21 - </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-497cc948 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="497cc948" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6e5e20d0 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6e5e20d0" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-1b371f16 wdp-sticky-section-no" data-id="1b371f16" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-14988f51 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="14988f51" data-element_type="widget" id="event" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Waktu & Tempat</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-52f94aeb wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="52f94aeb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Pernikahan</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5f69fe2c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5f69fe2c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">“Cinta tidak mengenal hambatan. Ia melompati rintangan, melompati pagar, menembus tembok untuk sampai pada tujuannya dengan penuh harapan.” </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-655f4911 elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="655f4911" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-29498663 animated-slow wdp-sticky-section-no" data-id="29498663" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1d57f53 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="1d57f53" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconwedding-photography"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-77445556 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="77445556" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Akad</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-60e3af80 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="60e3af80" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-55eafe0c animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="55eafe0c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Desember</p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-2a2b7094 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2a2b7094" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-20dd8038 wdp-sticky-section-no" data-id="20dd8038" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-c69791e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c69791e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Minggu</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3731874f wdp-sticky-section-no" data-id="3731874f" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-1d4345c2 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="1d4345c2" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5743b464 wdp-sticky-section-no" data-id="5743b464" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-584af73c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="584af73c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">2022 </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-509a380a elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="509a380a" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6266b5f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6266b5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">08.00 WIB</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-221c6aa wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="221c6aa" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-b1fed72 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="b1fed72" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5af56a15 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5af56a15" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3da989e9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3da989e9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-39c6f28b elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="39c6f28b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-65904fa1 animated-slow wdp-sticky-section-no" data-id="65904fa1" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1d480cf8 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="1d480cf8" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconromantic-dinner"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3f2c16ac animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3f2c16ac" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Resepsi</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1f2e6144 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="1f2e6144" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5a66c70 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5a66c70" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Desember</p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-7e96158a elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7e96158a" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6e280b11 wdp-sticky-section-no" data-id="6e280b11" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-635a6f90 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="635a6f90" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Minggu</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-7e84cc85 wdp-sticky-section-no" data-id="7e84cc85" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-323a9c wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="323a9c" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2c41370c wdp-sticky-section-no" data-id="2c41370c" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-2bcf4857 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2bcf4857" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">2022 </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-48302580 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="48302580" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-13a97799 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="13a97799" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">14.00 WIB</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6de53fd2 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="6de53fd2" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-ea08984 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="ea08984" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1dcb46a3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1dcb46a3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7be763ca wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7be763ca" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6974399d elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="6974399d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-5c4b6f5e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5c4b6f5e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-62bb9958 wdp-sticky-section-no" data-id="62bb9958" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-4e1b2a78 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4e1b2a78" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3bb331d8 wdp-sticky-section-no" data-id="3bb331d8" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-36e03921 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="36e03921" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:10,&quot;end&quot;:50}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img src="https://unityinvitation.com/wp-content/uploads/elementor/thumbs/Unity-Diamond-2-virtual-pi4zqhek723tgldjyrmcnqqdeyik961k0w8654yo2g.jpg" title="Unity Diamond 2 virtual" alt="Unity Diamond 2 virtual" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-587b4c32 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="587b4c32" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-392c6817 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="392c6817" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kami akan <br> Melangsungkan </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1ada6771 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1ada6771" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Live Streaming </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-714a4d2e animated-slow wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="714a4d2e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live. </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1fda86ca elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="1fda86ca" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://www.youtube.com/channel/UCC3HlQTFyY0fOkiBbyWqtiA" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fab fa-youtube"></i>
                                </span>
                                <span class="elementor-button-text">Join Streaming</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-424fc4a6 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="424fc4a6" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-692a20ef wdp-sticky-section-no" data-id="692a20ef" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-7c5a6a52 pp-image-slider-carousel pp-ins-normal pp-ins-hover-normal wdp-sticky-section-no elementor-widget elementor-widget-pp-image-slider" data-id="7c5a6a52" data-element_type="widget" data-settings="{&quot;skin&quot;:&quot;carousel&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;infinite_loop&quot;:&quot;yes&quot;}" data-widget_type="pp-image-slider.default">
                <div class="elementor-widget-container">
                  <div class="pp-image-slider-container">
                    <div class="pp-image-slider-wrap swiper-container-wrap">
                      <div class="pp-image-slider-box">
                        <div class="pp-image-slider pp-swiper-slider swiper-container" id="pp-image-slider-7c5a6a52" data-slider-settings="{&quot;direction&quot;:&quot;horizontal&quot;,&quot;speed&quot;:1500,&quot;effect&quot;:&quot;fade&quot;,&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10,&quot;autoHeight&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:{&quot;delay&quot;:2000},&quot;breakpoints&quot;:{&quot;1025&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;768&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;320&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10}}}">
                          <div class="swiper-wrapper">
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-slide.jpg" alt="Unity Diamond 2 slide" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-slide2.jpg" alt="Unity Diamond 2 slide2" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-slide3.jpg" alt="Unity Diamond 2 slide3" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-slide4.jpg" alt="Unity Diamond 2 slide4" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-5a115680 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5a115680" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-49caabd6 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="49caabd6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Sebuah Kisah</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-67ee26d3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="67ee26d3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Perjalanan Kami</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-acca52e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="acca52e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Aku tidak tahu dimana ujung perjalanan ini, aku tidak bisa menjanjikan apapun. Tapi, selama aku mampu, mimpi-mimpi kita adalah prioritas." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-361fb269 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="361fb269" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-575b316f wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="575b316f" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="story" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6c1ce578 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6c1ce578" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6c347011 animated-slow wdp-sticky-section-no" data-id="6c347011" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-48899aec animated-slow justify-cstm wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="48899aec" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:200,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
                        <div class="elementor-widget-container">
                          <div class="pp-timeline-wrapper">
                            <div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
                              <div class="pp-timeline-connector-wrap">
                                <div class="pp-timeline-connector">
                                  <div class="pp-timeline-connector-inner"></div>
                                </div>
                              </div>
                              <div class="pp-timeline-items">
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-3ddf251">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 11 Januari 2016 </div>
                                        <h2 class="pp-timeline-card-title"> Pertama Bertemu </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Pertama kali kita bertemu di bangku SMA tanpa mengenal satu sama lain. Kita hanya saling sapa.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 11 Januari 2016 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-48bb742">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 08 Juni 2019 </div>
                                        <h2 class="pp-timeline-card-title"> Menjalin Hubungan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Setelah pertemuan di bangku SMA dan masing-masing dari kita sudah lulus, barulah kita mulai menjalin hubungan yang berawal dari DM Instagram. Hingga kita memutuskan untuk sama-sama dan LDR.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 08 Juni 2019 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-9e7595d">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 17 Agustus 2021 </div>
                                        <h2 class="pp-timeline-card-title"> Bertunangan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Komunikasi jarak jauh dan kesibukan kita masing-masing bukan penghalang. Pada Agustus 2020 kedua keluarga saling bertemu dan melaksanakan acara tunangan kami.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 17 Agustus 2021 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-255189e">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 01 Desember 2022 </div>
                                        <h2 class="pp-timeline-card-title"> Menikah </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Akhirnya momen spesial pernikahan kami dilaksanakan pada 01 Desember 2022. Ini menjadi tanggal yang kami pilih untuk saling mengikat janji menjadi sebuah keluarga yang setia &amp; saling menyayangi.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 01 Desember 2022 </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7a6dc937 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7a6dc937" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-74bd11b6 wdp-sticky-section-no" data-id="74bd11b6" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-5edeca9c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5edeca9c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Momen</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-53d04308 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="53d04308" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Bahagia Kami</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-016fc73 elementor-aspect-ratio-169 wdp-sticky-section-no elementor-widget elementor-widget-video" data-id="016fc73" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/youtu.be\/T2-z21YJzS4&quot;,&quot;yt_privacy&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;play_on_mobile&quot;:&quot;yes&quot;,&quot;mute&quot;:&quot;yes&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default">
                <div class="elementor-widget-container">
                  <div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                    <div class="elementor-video"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-5090b976 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5090b976" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Kamu adalah sahabat dan kekasihku, dan aku tidak tahu sisi mana darimu yang paling aku nikmati. Aku menghargai setiap sisi, sama seperti aku telah menghargai hidup kita bersama." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-5b4382f0 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5b4382f0" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-4d50c7fa wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="4d50c7fa" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="gallery" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-e1a0093 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="e1a0093" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-298121e wdp-sticky-section-no" data-id="298121e" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-9630821 wdp-sticky-section-no elementor-widget elementor-widget-gallery" data-id="9630821" data-element_type="widget" data-settings="{&quot;gallery_layout&quot;:&quot;justified&quot;,&quot;ideal_row_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:350,&quot;sizes&quot;:[]},&quot;ideal_row_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:130,&quot;sizes&quot;:[]},&quot;gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;gap_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;ideal_row_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;gap_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;link_to&quot;:&quot;file&quot;,&quot;overlay_background&quot;:&quot;yes&quot;,&quot;content_hover_animation&quot;:&quot;fade-in&quot;}" data-widget_type="gallery.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-gallery__container">
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-1.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xLmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-1-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxNDAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS04LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-8-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS03LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-7-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-6.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS02LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-6-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS01LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-5-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-3.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0zLmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-3-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS00LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-4-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-2.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxMzQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0yLmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-2-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-15.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xNS5qcGciLCJzbGlkZXNob3ciOiJhbGwtOTYzMDgyMSJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-15-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-13.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xMy5qcGciLCJzbGlkZXNob3ciOiJhbGwtOTYzMDgyMSJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-13-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xMi5qcGciLCJzbGlkZXNob3ciOiJhbGwtOTYzMDgyMSJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-12-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-14.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xNC5qcGciLCJzbGlkZXNob3ciOiJhbGwtOTYzMDgyMSJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-2-Galeri-14-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxNDMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS0xMS5qcGciLCJzbGlkZXNob3ciOiJhbGwtOTYzMDgyMSJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-11-1024x683.jpg" data-width="800" data-height="534" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-9630821" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDYxNDEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0yLUdhbGVyaS05LmpwZyIsInNsaWRlc2hvdyI6ImFsbC05NjMwODIxIn0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-Galeri-9-1024x683.jpg" data-width="800" data-height="534" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-692b1fcc elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="692b1fcc" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3130a34d wdp-sticky-section-no" data-id="3130a34d" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-7bd09e21 wdp-sticky-section-no" data-id="7bd09e21" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-2c0f6e15 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2c0f6e15" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Photo by</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-53b498cf wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="53b498cf" data-element_type="widget" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="800" height="220" src="https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1024x281.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1024x281.png 1024w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-300x82.png 300w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-768x211.png 768w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1536x422.png 1536w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-2048x563.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3ffd989d elementor-shape-circle elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="3ffd989d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/setangkaiproject/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-75fa96de wdp-sticky-section-no" data-id="75fa96de" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-34949a7b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="34949a7b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-bf88f1e wdp-sticky-section-no" data-id="bf88f1e" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-8888b8e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8888b8e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-13814e4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="13814e4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat, <br>acara ini menerapkan Protokol Kesehatan, sesuai dengan peraturan & rekomendasi pemerintah. </h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-5e2cb58 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="5e2cb58" data-element_type="widget" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-f3d1997 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f3d1997" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d7ef8ef animated-slow wdp-sticky-section-no elementor-invisible" data-id="d7ef8ef" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-693215a elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="693215a" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Gunakan Masker</h3>
                              <p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2c9f0d9 animated-slow wdp-sticky-section-no elementor-invisible" data-id="2c9f0d9" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-0c6b029 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="0c6b029" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Cuci Tangan</h3>
                              <p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-2bbe501 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2bbe501" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-b086652 animated-slow wdp-sticky-section-no elementor-invisible" data-id="b086652" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-18483d0 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="18483d0" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Jaga Jarak </h3>
                              <p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-94cb787 animated-slow wdp-sticky-section-no elementor-invisible" data-id="94cb787" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-12c2db3 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="12c2db3" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Tidak Berjabat Tangan</h3>
                              <p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-5bcb9966 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5bcb9966" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;,&quot;shape_divider_bottom&quot;:&quot;opacity-tilt&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3097888b wdp-sticky-section-no" data-id="3097888b" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-74af725a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="74af725a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Doa & Ucapan untuk</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-2fb05459 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2fb05459" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-2839e0cc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2839e0cc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Merupakan suatu kehormatan dan kebahagiaan bagi Kami apabila Bapak/ Ibu/ Saudara/ i berkenan hadir untuk memberikan doa restu kepada kedua mempelai.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-777d3698 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="777d3698" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-5b8e8a03 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="5b8e8a03" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="wishes" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-769d6a16 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="769d6a16" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-59509678 animated-slow wdp-sticky-section-no elementor-invisible" data-id="59509678" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-486db674 wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="486db674" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
                        <div class="elementor-widget-container">
                          <style>
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit] {
                              height: auto !important;
                            }

                            .halooo {
                              background-color: red !important;
                              color: #fff !important;
                            }

                            .wdp-wrapper .wdp-wrap-link {
                              text-align: center;
                              padding: 18px;
                              margin-top: 15px;
                            }

                            .wdp-wrapper .wdp-wrap-form {
                              border-top: 0px;
                            }

                            .konfirmasi-kehadiran {
                              display: flex;
                              justify-content: center;
                              flex-direction: row;
                              padding: 10px;
                              margin-top: 10px;
                            }

                            .jenis-konfirmasi>i {
                              margin: 0px 5px;
                            }

                            .jenis-konfirmasi {
                              padding: 5px 10px;
                              background: red;
                              border-radius: 0.5em;
                              color: #fff;
                              margin: 7px;
                            }
                          </style>
                          <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                            <div class='wdp-wrap-link'>
                              <a id='wdp-link-29833' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=29833&amp;comments=3&amp;get=0&amp;order=DESC' title='3 Ucapan'></a>
                              <div class='konfirmasi-kehadiran elementor-screen-only'>
                                <div class='jenis-konfirmasi cswd-hadir'>
                                  <i class='fas fa-check'></i> 2 Hadir
                                </div>
                                <div class='jenis-konfirmasi cswd-tidak-hadir'>
                                  <i class='fas fa-times'></i> 1 Tidak Hadir
                                </div>
                              </div>
                            </div>
                            <div id='wdp-wrap-commnent-29833' class='wdp-wrap-comments' style='display:none;'>
                              <div id='wdp-wrap-form-29833' class='wdp-wrap-form wdp-clearfix'>
                                <div id='wdp-container-form-29833' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                  <div id='respond-29833' class='respond wdp-clearfix'>
                                    <form action='https://unityinvitation.com/wp-comments-post.php' method='post' id='commentform-29833'>
                                      <p class="comment-form-author wdp-field-1">
                                        <input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                      </p>
                                      <div class="wdp-wrap-textarea">
                                        <textarea id="wdp-textarea-29833" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                      </div>
                                      <div class="wdp-wrap-select">
                                        <select class="waci_comment wdp-select" name="konfirmasi">
                                          <option value="" disabled selected>Konfirmasi Kehadiran</option>> </option>
                                          <option value="Hadir">Hadir</option>
                                          <option value="Tidak Hadir">Tidak Hadir</option>
                                        </select>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                      </div>
                                      <div class='wdp-wrap-submit wdp-clearfix'>
                                        <p class='form-submit'>
                                          <span class="wdp-hide">Do not change these fields following</span>
                                          <input type="text" class="wdp-hide" name="name" value="username">
                                          <input type="text" class="wdp-hide" name="nombre" value="">
                                          <input type="text" class="wdp-hide" name="form-wdp" value="">
                                          <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                          <input name='submit' id='submit-29833' value='Kirim' type='submit' />
                                          <input type='hidden' name='commentpress' value='true' />
                                          <input type='hidden' name='comment_post_ID' value='29833' id='comment_post_ID' />
                                          <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                        </p>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <div id='wdp-comment-status-29833' class='wdp-comment-status'></div>
                              <ul id='wdp-container-comment-29833' class='wdp-container-comments wdp-order-DESC  wdp-has-3-comments wdp-multiple-comments' data-order='DESC'></ul>
                              <div class='wdp-holder-29833 wdp-holder'></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-5eb6cd89 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5eb6cd89" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-5d2c2ef3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5d2c2ef3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Semoga Allah memberkahimu dan memberkahi apa yang menjadi tanggung jawabmu, serta menyatukan kalian berdua dalam kebaikan." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-50ea54d9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="50ea54d9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">(HR. Ahmad, at-Tirmidzi, an-Nasa'i, <br> Abu Dawud, dan Ibnu Majah) </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-27bbb018 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="27bbb018" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-22e1cc wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="22e1cc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:10,&quot;end&quot;:50}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="297" height="339" src="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-2-1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-2-1.png 297w, https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-2-1-263x300.png 263w" sizes="(max-width: 297px) 100vw, 297px" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-dde4841 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dde4841" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-40b69c4d wdp-sticky-section-no" data-id="40b69c4d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-19cbf5be wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="19cbf5be" data-element_type="widget" data-widget_type="html.default">
                <div class="elementor-widget-container">
                  <script>
                    document.addEventListener('DOMContentLoaded', function() {
                      jQuery(function($) {
                        $('.clicktoshow').each(function(i) {
                          $(this).click(function() {
                            $('.showclick').eq(i).toggle();
                            $('.clicktoshow');
                            $('.showclick2').eq(i).hide();
                          });
                        });
                      });
                    });
                  </script>
                  <style>
                    .clicktoshow {
                      cursor: pointer;
                    }

                    .showclick {
                      display: none;
                    }
                  </style>
                  <script>
                    document.addEventListener('DOMContentLoaded', function() {
                      jQuery(function($) {
                        $('.clicktoshow2').each(function(i) {
                          $(this).click(function() {
                            $('.showclick2').eq(i).toggle();
                            $('.clicktoshow2');
                          });
                        });
                      });
                    });
                  </script>
                  <style>
                    .clicktoshow2 {
                      cursor: pointer;
                    }

                    .showclick2 {
                      display: none;
                    }
                  </style>
                </div>
              </div>
              <div class="elementor-element elementor-element-82fd812 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="82fd812" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-wrapper">
                    <div class="elementor-icon">
                      <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-405aa5e5 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="405aa5e5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hadiah Pernikahan</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-25926903 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="25926903" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Doa restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah. Tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentu semakin melengkapi kebahagiaan kami.</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-7a942c07 elementor-align-center clicktoshow animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="7a942c07" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                <div class="elementor-widget-container">
                  <div class="elementor-button-wrapper">
                    <a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
                      <span class="elementor-button-content-wrapper">
                        <span class="elementor-button-icon elementor-align-icon-left">
                          <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                        </span>
                        <span class="elementor-button-text">Kirim Hadiah</span>
                      </span>
                    </a>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-2f1e4907 showclick elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2f1e4907" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2fb46042 wdp-sticky-section-no" data-id="2fb46042" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-51a3a957 wdp-sticky-section-no" data-id="51a3a957" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-cfb78ab wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cfb78ab" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Amplop</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5f9cf27 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="5f9cf27" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1c488fd5 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="1c488fd5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="400" height="150" src="https://unityinvitation.com/wp-content/uploads/2021/12/LOGO-MANDIRI.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/LOGO-MANDIRI.png 400w, https://unityinvitation.com/wp-content/uploads/2021/12/LOGO-MANDIRI-300x113.png 300w" sizes="(max-width: 400px) 100vw, 400px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-29064d22 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="29064d22" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image img"></div>
                          <div class="head-title">a/n Chairil Gibran</div>
                          <div class="elementor-button-wrapper">
                            <div class="copy-content spancontent">123456&zwj;7891011</div>
                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                              <div class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="far fa-copy"></i>
                                </span>
                                <span class="elementor-button-text">Salin</span>
                              </div>
                            </a>
                          </div>
                          <style type="text/css">
                            .spancontent {
                              padding-bottom: 20px;
                            }

                            .copy-content {
                              color: #6EC1E4;
                              text-align: center;
                            }

                            .head-title {
                              color: #6EC1E4;
                              text-align: center;
                            }
                          </style>
                          <script>
                            function copyText(el) {
                              var content = jQuery(el).siblings('div.copy-content').html()
                              var temp = jQuery(" < textarea > ");
                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                var text = jQuery(el).html()
                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                var interval = setInterval(function() {
                                  counter++;
                                  if (counter == 2) {
                                    jQuery(el).html(text)
                                    Interval(interval);
                                  }
                                }, 5000);
                              }
                          </script>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-feb1a8e wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="feb1a8e" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-9e23f68 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9e23f68" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kado</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-e232d0c elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="e232d0c" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-23b19537 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="23b19537" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image img"></div>
                          <div class="head-title">a/n Rahma Aqeela</div>
                          <div class="elementor-button-wrapper">
                            <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, &zwj;Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                              <div class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="far fa-copy"></i>
                                </span>
                                <span class="elementor-button-text">Salin</span>
                              </div>
                            </a>
                          </div>
                          <style type="text/css">
                            .spancontent {
                              padding-bottom: 20px;
                            }

                            .copy-content {
                              color: #6EC1E4;
                              text-align: center;
                            }

                            .head-title {
                              color: #6EC1E4;
                              text-align: center;
                            }
                          </style>
                          <script>
                            function copyText(el) {
                              var content = jQuery(el).siblings('div.copy-content').html()
                              var temp = jQuery(" < textarea > ");
                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                var text = jQuery(el).html()
                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                var interval = setInterval(function() {
                                  counter++;
                                  if (counter == 2) {
                                    jQuery(el).html(text)
                                    Interval(interval);
                                  }
                                }, 5000);
                              }
                          </script>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-63634e0f wdp-sticky-section-no" data-id="63634e0f" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="pp-bg-effects pp-bg-effects-7a18c62d elementor-section elementor-top-section elementor-element elementor-element-7a18c62d pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="7a18c62d" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="#D9C6A8" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="15" data-part-speed="" data-part-direction="none" data-hover-effect="noeffect" data-hover-size="" data-id="7a18c62d" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;shape_divider_top&quot;:&quot;opacity-tilt&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2600 131.1" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M0 0L2600 0 2600 69.1 0 0z" />
            <path class="elementor-shape-fill" style="opacity:0.5" d="M0 0L2600 0 2600 69.1 0 69.1z" />
            <path class="elementor-shape-fill" style="opacity:0.25" d="M2600 0L0 0 0 130.1 2600 69.1z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-46ca21ad wdp-sticky-section-no" data-id="46ca21ad" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-2b1b18fc elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2b1b18fc" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-666f5268 animated-fast wdp-sticky-section-no elementor-invisible" data-id="666f5268" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-2c5cb57c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2c5cb57c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Terima Kasih</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2d081be7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2d081be7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h1 class="elementor-heading-title elementor-size-default">Aqeela & Gibran</h1>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1ac35d3a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1ac35d3a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Keluarga Besar</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3e1b07a8 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3e1b07a8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Bapak Ahmad Gibran & Ibu Laksmi <br> Bapak Bob Aqeela & Ibu Dini Aminati </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-de2a28c wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="de2a28c" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-1f53cc1 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="1f53cc1" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-f6c2e45 wdp-sticky-section-no" data-id="f6c2e45" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-4f77494 wdp-sticky-section-no" data-id="4f77494" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-deb8f0c wdp-sticky-section-no" data-id="deb8f0c" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-32acef4 animated-slow wdp-sticky-section-no elementor-invisible" data-id="32acef4" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-95e108f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="95e108f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="https://unityinvitation.com/" target="_blank">
                            <img width="768" height="589" src="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png 768w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-300x230.png 300w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite.png 779w" sizes="(max-width: 768px) 100vw, 768px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-569767e elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="569767e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20Digital%20Unity%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Whatsapp</span>
                                <i class="fab fa-whatsapp"></i>
                              </a>
                            </span>
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/unity.invitation/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-69c6ff5 animated-slow wdp-sticky-section-no elementor-invisible" data-id="69c6ff5" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-dba83bc animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="dba83bc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="https://unityinvitation.com/" target="_blank">
                            <img width="1001" height="1001" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png 1001w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-150x150.png 150w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-768x768.png 768w" sizes="(max-width: 1001px) 100vw, 1001px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-f5a5708 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="f5a5708" data-element_type="widget" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">© 2022 Unity Invitation, <br>All Rights Reserved </h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-43c7bcd wdp-sticky-section-no" data-id="43c7bcd" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-ac45f03 wdp-sticky-section-no" data-id="ac45f03" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-2733439 wdp-sticky-section-no" data-id="2733439" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-257654ce elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="257654ce" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
                <div class="elementor-widget-container">
                  <script>
                    var settingAutoplay = 'yes';
                    window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
                  </script>
                  <div id="audio-container" class="audio-box">
                    <style>
                      .play_cswd_audio {
                        animation-name: putar;
                        animation-duration: 5000ms;
                        animation-iteration-count: infinite;
                        animation-timing-function: linear;
                      }

                      @keyframes putar {
                        from {
                          transform: rotate(0deg);
                        }

                        to {
                          transform: rotate(360deg);
                        }
                      }
                    </style>
                    <audio id="song" loop>
                      <source src="https://unityinvitation.com/wp-content/uploads/2022/03/Michael-Learn-To-Rock-Paint-My-Love-Saxophone-Cover.mp3" type="audio/mp3">
                    </audio>
                    <div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
                      </div>
                    </div>
                    <div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
                      </div>
                    </div>
                  </div>
                  <!-- <script>
			jQuery("document").ready(function (n) {
				var e = window.settingAutoplay;
				e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
					n("#audio-container").click(function (u) {
						e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
					});
			});
		</script> -->
                </div>
              </div>
              <div class="elementor-element elementor-element-40ad48b wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="40ad48b" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
                <div class="elementor-widget-container">
                  <style>
                    .elementor-image>img {
                      display: initial !important;
                    }
                  </style>
                  <div class="modalx animated " data-sampul='https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-2-cover1-scaled.jpg'>
                    <div class="overlayy"></div>
                    <div class="content-modalx">
                      <div class="info_modalx">
                        <div class="elementor-image img">
                          <img src="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-2-cover2.png" title="Unity Diamond 2 cover2" alt="Unity Diamond 2 cover2" />
                        </div>
                        <div class="text_tambahan">PERNIKAHAN</div>
                        <div class="wdp-mempelai">Aqeela & Gibran</div>
                        <div class="wdp-dear">Yth. Bapak/Ibu/Saudara/i</div>
                        <div class="wdp-name"> Nama Tamu </div>
                        <div class="wdp-text">Tanpa mengurangi rasa hormat, <br> Kami mengundang Bapak/Ibu/Saudara/i <br>untuk hadir di acara pernikahan kami. </div>
                        <div class="wdp-button-wrapper">
                          <button class="elementor-button"> Buka Undangan </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <script>
                    const sampul = jQuery('.modalx').data('sampul');
                    jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
                    jQuery('body').css('overflow', 'hidden');
                    jQuery('.wdp-button-wrapper button').on('click', function() {
                      jQuery('.modalx').removeClass('');
                      jQuery('.modalx').addClass('fadeIn reverse');
                      setTimeout(function() {
                        jQuery('.modalx').addClass('removeModals');
                      }, 1600);
                      jQuery('body').css('overflow', 'auto');
                      document.getElementById("song").play();
                    });
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer class="elementor-section elementor-top-section elementor-element elementor-element-4d380482 animated-slow elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="4d380482" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:2500,&quot;sticky&quot;:&quot;bottom&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-759bf12b wdp-sticky-section-no" data-id="759bf12b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-background-overlay"></div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-210918db elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="210918db" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-2540b443 animated-slow wdp-sticky-section-no elementor-invisible" data-id="2540b443" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:2500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-515cf7f8 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="515cf7f8" data-element_type="widget" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="#home">
                            <img width="500" height="500" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-menu.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-menu.png 500w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-menu-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-2-menu-150x150.png 150w" sizes="(max-width: 500px) 100vw, 500px" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-7237c2ea animated-slow wdp-sticky-section-no elementor-invisible" data-id="7237c2ea" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3000}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-326eba27 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="326eba27" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#couple">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-couple"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-57266fd8 animated-slow wdp-sticky-section-no elementor-invisible" data-id="57266fd8" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-e0ae4ee elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="e0ae4ee" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#event">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-event"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-231abc22 animated-slow wdp-sticky-section-no elementor-invisible" data-id="231abc22" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4000}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6bcddd47 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6bcddd47" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#story">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-love-story"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-1b09d249 animated-slow wdp-sticky-section-no elementor-invisible" data-id="1b09d249" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-59b62a3c elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="59b62a3c" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#gallery">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gallery"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-1a85f1af animated-slow wdp-sticky-section-no elementor-invisible" data-id="1a85f1af" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:5000}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-749d3efd elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="749d3efd" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#wishes">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-wishes"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-widget-wrap elementor-element-populated">
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-spacer">
                              <div class="elementor-spacer-inner"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                  <div class="elementor-widget-container">
                    <p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami. </p>
                  </div>
                </div>
                <div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
                  <div class="elementor-widget-container">
                    <div class="elementor-spacer">
                      <div class="elementor-spacer-inner"></div>
                    </div>
                  </div>
                </div>
                <div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
                  <div class="elementor-widget-container">
                    <div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
                      <div class="jet-tabs__control-wrapper">
                        <div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-Mandiri.png" alt="">
                          </div>
                        </div>
                      </div>
                      <div class="jet-tabs__content-wrapper">
                        <div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                          <div class="elementor-widget-container">
                            <h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-icon-wrapper">
                              <div class="elementor-icon">
                                <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-image img"></div>
                            <div class="head-title">a/n Bimo Bramantyo</div>
                            <div class="elementor-button-wrapper">
                              <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                              <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                <div class="elementor-button-content-wrapper">
                                  <span class="elementor-button-icon elementor-align-icon-left">
                                    <i aria-hidden="true" class="far fa-copy"></i>
                                  </span>
                                  <span class="elementor-button-text">Salin</span>
                                </div>
                              </a>
                            </div>
                            <style type="text/css">
                              .spancontent {
                                padding-bottom: 20px;
                              }

                              .copy-content {
                                color: #6EC1E4;
                                text-align: center;
                              }

                              .head-title {
                                color: #6EC1E4;
                                text-align: center;
                              }
                            </style>
                            <script>
                              function copyText(el) {
                                var content = jQuery(el).siblings('div.copy-content').html()
                                var temp = jQuery(" < textarea > ");
                                  jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                  var text = jQuery(el).html()
                                  jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                  var interval = setInterval(function() {
                                    counter++;
                                    if (counter == 2) {
                                      jQuery(el).html(text)
                                      Interval(interval);
                                    }
                                  }, 5000);
                                }
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
          <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
              <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
                  <div class="elementor-widget-container">
                    <div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
                      <nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                      <div class="pp-menu-toggle pp-menu-toggle-on-tablet">
                        <div class="pp-hamburger">
                          <div class="pp-hamburger-box">
                            <div class="pp-hamburger-inner"></div>
                          </div>
                        </div>
                      </div>
                      <nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <link rel='stylesheet' id='ep-countdown-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-countdown.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox-css' href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-gallery-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/e-gallery/css/e-gallery.min.css?ver=1.2.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-51208-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51208.css?ver=1655470782' type='text/css' media='all' />
    <link rel='stylesheet' id='e-animations-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
    <script type='text/javascript' id='WEDKU_SCRP-js-extra'>
      /* 
																																								<![CDATA[ */
      var ceper = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.18' id='WEDKU_SCRP-js'></script>
    <script type='text/javascript' id='wdp_js_script-js-extra'>
      /* 
																																								<![CDATA[ */
      var WDP_WP = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "wdpNonce": "3ae8897e57",
        "jpages": "true",
        "jPagesNum": "5",
        "textCounter": "true",
        "textCounterNum": "500",
        "widthWrap": "",
        "autoLoad": "true",
        "thanksComment": "Terima kasih atas ucapan & doanya!",
        "thanksReplyComment": "Terima kasih atas balasannya!",
        "duplicateComment": "You might have left one of the fields blank, or duplicate comments",
        "insertImage": "Insert image",
        "insertVideo": "Insert video",
        "insertLink": "Insert link",
        "checkVideo": "Check video",
        "accept": "Accept",
        "cancel": "Cancel",
        "reply": "Balas",
        "textWriteComment": "Tulis Ucapan & Doa",
        "classPopularComment": "wdp-popular-comment",
        "textUrlImage": "Url image",
        "textUrlVideo": "Url video youtube or vimeo",
        "textUrlLink": "Url link",
        "textToDisplay": "Text to display",
        "textCharacteresMin": "Minimal 2 karakter",
        "textNavNext": "Selanjutnya",
        "textNavPrev": "Sebelumnya",
        "textMsgDeleteComment": "Do you want delete this comment?",
        "textLoadMore": "Load more",
        "textLikes": "Likes"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/loftloader/assets/js/loftloader.min.js?ver=2022022501' id='loftloader-lite-front-main-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/pp-bg-effects.min.js?ver=1.0.0' id='pp-bg-effects-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/particles/particles.min.js?ver=2.0.0' id='particles-js'></script>
    <script type='text/javascript' id='bdt-uikit-js-extra'>
      /* 
																																								<![CDATA[ */
      var element_pack_ajax_login_config = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "language": "en",
        "loadingmessage": "Sending user info, please wait...",
        "unknownerror": "Unknown error, make sure access is correct!"
      };
      var ElementPackConfig = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "ebe2274658",
        "data_table": {
          "language": {
            "lengthMenu": "Show _MENU_ Entries",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "search": "Search :",
            "paginate": {
              "previous": "Previous",
              "next": "Next"
            }
          }
        },
        "contact_form": {
          "sending_msg": "Sending message please wait...",
          "captcha_nd": "Invisible captcha not defined!",
          "captcha_nr": "Could not get invisible captcha response!"
        },
        "mailchimp": {
          "subscribing": "Subscribing you please wait..."
        },
        "elements_data": {
          "sections": [],
          "columns": [],
          "widgets": []
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
    <script type='text/javascript' id='elementor-frontend-js-before'>
      var elementorFrontendConfig = {
        "environmentMode": {
          "edit": false,
          "wpPreview": false,
          "isScriptDebug": false
        },
        "i18n": {
          "shareOnFacebook": "Share on Facebook",
          "shareOnTwitter": "Share on Twitter",
          "pinIt": "Pin it",
          "download": "Download",
          "downloadImage": "Download image",
          "fullscreen": "Fullscreen",
          "zoom": "Zoom",
          "share": "Share",
          "playVideo": "Play Video",
          "previous": "Previous",
          "next": "Next",
          "close": "Close"
        },
        "is_rtl": false,
        "breakpoints": {
          "xs": 0,
          "sm": 480,
          "md": 768,
          "lg": 1025,
          "xl": 1440,
          "xxl": 1600
        },
        "responsive": {
          "breakpoints": {
            "mobile": {
              "label": "Mobile",
              "value": 767,
              "default_value": 767,
              "direction": "max",
              "is_enabled": true
            },
            "mobile_extra": {
              "label": "Mobile Extra",
              "value": 880,
              "default_value": 880,
              "direction": "max",
              "is_enabled": false
            },
            "tablet": {
              "label": "Tablet",
              "value": 1024,
              "default_value": 1024,
              "direction": "max",
              "is_enabled": true
            },
            "tablet_extra": {
              "label": "Tablet Extra",
              "value": 1200,
              "default_value": 1200,
              "direction": "max",
              "is_enabled": false
            },
            "laptop": {
              "label": "Laptop",
              "value": 1366,
              "default_value": 1366,
              "direction": "max",
              "is_enabled": false
            },
            "widescreen": {
              "label": "Widescreen",
              "value": 2400,
              "default_value": 2400,
              "direction": "min",
              "is_enabled": false
            }
          }
        },
        "version": "3.6.4",
        "is_static": false,
        "experimentalFeatures": {
          "e_dom_optimization": true,
          "a11y_improvements": true,
          "e_import_export": true,
          "e_hidden_wordpress_widgets": true,
          "theme_builder_v2": true,
          "landing-pages": true,
          "elements-color-picker": true,
          "favorite-widgets": true,
          "admin-top-bar": true,
          "form-submissions": true
        },
        "urls": {
          "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"
        },
        "settings": {
          "page": [],
          "editorPreferences": []
        },
        "kit": {
          "viewport_tablet": 1024,
          "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
          "global_image_lightbox": "yes"
        },
        "post": {
          "id": 29833,
          "title": "Unity%20Diamond%202",
          "excerpt": "",
          "featuredImage": "https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/11\/Unity-Diamond-2-cover-invitation.jpg"
        }
      };
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-countdown.min.js?ver=6.0.10' id='ep-countdown-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.js?ver=2.8.2' id='jquery-fancybox-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/jquery-resize/jquery.resize.min.js?ver=0.5.3' id='jquery-resize-js'></script>
    <script type='text/javascript' id='powerpack-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var ppLogin = {
        "empty_username": "Enter a username or email address.",
        "empty_password": "Enter password.",
        "empty_password_1": "Enter a password.",
        "empty_password_2": "Re-enter password.",
        "empty_recaptcha": "Please check the captcha to verify you are not a robot.",
        "email_sent": "A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.",
        "reset_success": "Your password has been reset successfully.",
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      var ppRegistration = {
        "invalid_username": "This username is invalid because it uses illegal characters. Please enter a valid username.",
        "username_exists": "This username is already registered. Please choose another one.",
        "empty_email": "Please type your email address.",
        "invalid_email": "The email address isn\u2019t correct!",
        "email_exists": "The email is already registered, please choose another one.",
        "password": "Password must not contain the character \"\\\\\"",
        "password_length": "Your password should be at least 8 characters long.",
        "password_mismatch": "Password does not match.",
        "invalid_url": "URL seems to be invalid.",
        "recaptcha_php_ver": "reCAPTCHA API requires PHP version 5.3 or above.",
        "recaptcha_missing_key": "Your reCAPTCHA Site or Secret Key is missing!",
        "show_password": "Show password",
        "hide_password": "Hide password",
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min.js?ver=1.2.0' id='elementor-gallery-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
    <script type='text/javascript' id='elementor-pro-frontend-js-before'>
      var ElementorProFrontendConfig = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "1c6e428e33",
        "urls": {
          "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/",
          "rest": "https:\/\/unityinvitation.com\/wp-json\/"
        },
        "i18n": {
          "toc_no_headings_found": "No headings were found on this page."
        },
        "shareButtonsNetworks": {
          "facebook": {
            "title": "Facebook",
            "has_counter": true
          },
          "twitter": {
            "title": "Twitter"
          },
          "linkedin": {
            "title": "LinkedIn",
            "has_counter": true
          },
          "pinterest": {
            "title": "Pinterest",
            "has_counter": true
          },
          "reddit": {
            "title": "Reddit",
            "has_counter": true
          },
          "vk": {
            "title": "VK",
            "has_counter": true
          },
          "odnoklassniki": {
            "title": "OK",
            "has_counter": true
          },
          "tumblr": {
            "title": "Tumblr"
          },
          "digg": {
            "title": "Digg"
          },
          "skype": {
            "title": "Skype"
          },
          "stumbleupon": {
            "title": "StumbleUpon",
            "has_counter": true
          },
          "mix": {
            "title": "Mix"
          },
          "telegram": {
            "title": "Telegram"
          },
          "pocket": {
            "title": "Pocket",
            "has_counter": true
          },
          "xing": {
            "title": "XING",
            "has_counter": true
          },
          "whatsapp": {
            "title": "WhatsApp"
          },
          "email": {
            "title": "Email"
          },
          "print": {
            "title": "Print"
          }
        },
        "facebook_sdk": {
          "lang": "en_US",
          "app_id": ""
        },
        "lottie": {
          "defaultAnimationUrl": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"
        }
      };
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
    <script type='text/javascript' id='jet-elements-js-extra'>
      /* 
																																								<![CDATA[ */
      var jetElements = {
        "ajaxUrl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template",
        "devMode": "false",
        "messages": {
          "invalidMail": "Please specify a valid e-mail"
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
    <script type='text/javascript' id='jet-tabs-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var JetTabsSettings = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template",
        "devMode": "false"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
    <script type='text/javascript' id='weddingpress-wdp-js-extra'>
      /* 
																																								<![CDATA[ */
      var cevar = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "plugin_url": "https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
    <div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div>
    <div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>
  </body>
</html>