jQuery("document").ready(function(t) {
    t("#post-guestbook-box-me").submit(function(o) {
        o.preventDefault();
        var e = "action=guestbook_box_submit_me&id=" + t(".guestbook-box-content").data("id") + "&avatar=" + t("#hidden-avatar img").attr("src") + "&" + t(this).serialize();
        t.post(ceper.ajax_url, e, function(o) {
            t(".guestbook-list").prepend(o), t("#post-guestbook-box-me")[0].reset();
        });
    });
});