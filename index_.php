
<!doctype html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
        <style type="text/css">

            .wdp-comment-text img {

                max-width: 100% !important;

            }

        </style>

        
<!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
<title>Ara Invitation</title>
<meta name="description" content="Bagikan momen bahagiamu bersama Ara Invitation"/>
<meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet"/>
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Ara Invitation" />
<meta property="og:description" content="Bagikan momen bahagiamu bersama Ara Invitation" />
<meta property="og:url" content="https://unityinvitation.com/" />
<meta property="og:site_name" content="Ara Invitation" />
<meta property="og:updated_time" content="2022-04-08T23:57:49+07:00" />
<meta property="og:image" content="https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg" />
<meta property="og:image:secure_url" content="https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg" />
<meta property="og:image:width" content="533" />
<meta property="og:image:height" content="532" />
<meta property="og:image:alt" content="Ara Invitation" />
<meta property="og:image:type" content="image/jpeg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Ara Invitation" />
<meta name="twitter:description" content="Bagikan momen bahagiamu bersama Ara Invitation" />
<meta name="twitter:image" content="https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg" />
<meta name="twitter:label1" content="Written by" />
<meta name="twitter:data1" content="admin" />
<meta name="twitter:label2" content="Time to read" />
<meta name="twitter:data2" content="5 minutes" />
<script type="application/ld+json" class="rank-math-schema">{"@context":"https://schema.org","@graph":[{"@type":["LocalBusiness","Organization"],"@id":"https://unityinvitation.com/#organization","name":"Ara Invitation","url":"https://unityinvitation.com","logo":{"@type":"ImageObject","@id":"https://unityinvitation.com/#logo","url":"https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos.png","caption":"Ara Invitation","inLanguage":"en-US"},"openingHours":["Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday 09:00-17:00"],"image":{"@id":"https://unityinvitation.com/#logo"}},{"@type":"WebSite","@id":"https://unityinvitation.com/#website","url":"https://unityinvitation.com","name":"Ara Invitation","publisher":{"@id":"https://unityinvitation.com/#organization"},"inLanguage":"en-US","potentialAction":{"@type":"SearchAction","target":"https://unityinvitation.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg","url":"https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg","width":"533","height":"532","inLanguage":"en-US"},{"@type":"Person","@id":"https://unityinvitation.com/#author","name":"admin","image":{"@type":"ImageObject","@id":"https://secure.gravatar.com/avatar/77fc409529b91929e5a17a03f66309c3?s=96&amp;d=mm&amp;r=g","url":"https://secure.gravatar.com/avatar/77fc409529b91929e5a17a03f66309c3?s=96&amp;d=mm&amp;r=g","caption":"admin","inLanguage":"en-US"},"sameAs":["https://unityinvitation.com"],"worksFor":{"@id":"https://unityinvitation.com/#organization"}},{"@type":"WebPage","@id":"https://unityinvitation.com/#webpage","url":"https://unityinvitation.com/","name":"Ara Invitation","datePublished":"2022-03-31T16:09:24+07:00","dateModified":"2022-04-08T23:57:49+07:00","author":{"@id":"https://unityinvitation.com/#author"},"isPartOf":{"@id":"https://unityinvitation.com/#website"},"primaryImageOfPage":{"@id":"https://unityinvitation.com/wp-content/uploads/2022/04/Unity-Logo.jpg"},"inLanguage":"en-US"}]}</script>
<meta name="google-site-verification" content="ND5_h62FQC5vO8prrGAFFT8O2IcUyGkRorciPbDCPMA" />
<!-- /Rank Math WordPress SEO plugin -->

<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Ara Invitation &raquo; Feed" href="https://unityinvitation.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Ara Invitation &raquo; Comments Feed" href="https://unityinvitation.com/comments/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.9.3"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='bdt-uikit-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
<link rel='stylesheet' id='ep-helper-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://unityinvitation.com/wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
</style>
<link rel='stylesheet' id='WEDKU_STYLE-css'  href='https://unityinvitation.com/wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.16' type='text/css' media='all' />
<link rel='stylesheet' id='pafe-extension-style-css'  href='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
<link rel='stylesheet' id='wdp_style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
<style id='wdp_style-inline-css' type='text/css'>


        .wdp-wrapper {

          font-size: 14px

        }

    

        .wdp-wrapper {

          background: #ffffff;

        }

        .wdp-wrapper.wdp-border {

          border: 1px solid #d5deea;

        }



        .wdp-wrapper .wdp-wrap-comments a:link,

        .wdp-wrapper .wdp-wrap-comments a:visited {

          color: #54595f;

        }



        .wdp-wrapper .wdp-wrap-link a.wdp-link {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-wrap-form {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {

          border-color: #64B6EC;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {

          color: #FFFFFF;

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {

          background: #a3a3a3;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {

          color: #44525F;

        }



        .wdp-wrapper .wdp-media-btns a > span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-media-btns a > span:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-comment-status {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {

          color: #319342;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {

          color: #C85951;

        }

        .wdp-wrapper .wdp-comment-status.wdp-loading > span {

          color: #54595f;

        }



        .wdp-wrapper ul.wdp-container-comments {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {

          border-bottom: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {

          color: #54595f !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {

          color: #2a5782 !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {

          color: #54595f;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {

          color: #2a5782;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span:hover {

          color: #3D7DBC;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {

          color: #2C9E48;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {

          color: #D13D3D;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-counter-info {

          color: #9DA8B7;

        }



        .wdp-wrapper .wdp-holder span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a,

        .wdp-wrapper .wdp-holder a:link,

        .wdp-wrapper .wdp-holder a:visited {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a:hover,

        .wdp-wrapper .wdp-holder a:link:hover,

        .wdp-wrapper .wdp-holder a:visited:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled, .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {

          color: #9DA8B7;

        }

        

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {

        max-width: 28px;

        max-height: 28px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {

        margin-left: 38px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {

        max-width: 24px;

        max-height: 24px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {

        max-width: 21px;

        max-height: 21px;

    }

    
</style>
<link rel='stylesheet' id='wdp-centered-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-horizontal-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-fontello-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='exad-main-style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-theme-style-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-skin-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-1399-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-1399.css?ver=1652056007' type='text/css' media='all' />
<link rel='stylesheet' id='powerpack-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='weddingpress-wdp-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-pro-css'  href='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='uael-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
<link rel='stylesheet' id='jet-tabs-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-61379-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-61379.css?ver=1652056604' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-8253-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-8253.css?ver=1652056604' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-8886-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-8886.css?ver=1652056604' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-68563-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-68563.css?ver=1652056604' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51207-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51207.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-31584-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-31584.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-official-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
<link rel='stylesheet' id='font-awesome-official-v4shim-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
<style id='font-awesome-official-v4shim-inline-css' type='text/css'>
@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
unicode-range: U+F004-F005,U+F007,U+F017,U+F022,U+F024,U+F02E,U+F03E,U+F044,U+F057-F059,U+F06E,U+F070,U+F075,U+F07B-F07C,U+F080,U+F086,U+F089,U+F094,U+F09D,U+F0A0,U+F0A4-F0A7,U+F0C5,U+F0C7-F0C8,U+F0E0,U+F0EB,U+F0F3,U+F0F8,U+F0FE,U+F111,U+F118-F11A,U+F11C,U+F133,U+F144,U+F146,U+F14A,U+F14D-F14E,U+F150-F152,U+F15B-F15C,U+F164-F165,U+F185-F186,U+F191-F192,U+F1AD,U+F1C1-F1C9,U+F1CD,U+F1D8,U+F1E3,U+F1EA,U+F1F6,U+F1F9,U+F20A,U+F247-F249,U+F24D,U+F254-F25B,U+F25D,U+F267,U+F271-F274,U+F279,U+F28B,U+F28D,U+F2B5-F2B6,U+F2B9,U+F2BB,U+F2BD,U+F2C1-F2C2,U+F2D0,U+F2D2,U+F2DC,U+F2ED,U+F328,U+F358-F35B,U+F3A5,U+F3D1,U+F410,U+F4AD;
}
</style>
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display+SC%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-unityinv-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
<script type='text/javascript' id='jquery-core-js-extra'>
/* <![CDATA[ */
var pp = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
<link rel="https://api.w.org/" href="https://unityinvitation.com/wp-json/" /><link rel="alternate" type="application/json" href="https://unityinvitation.com/wp-json/wp/v2/pages/61379" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://unityinvitation.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://unityinvitation.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.9.3" />
<link rel='shortlink' href='https://unityinvitation.com/' />
<link rel="alternate" type="application/json+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2F&#038;format=xml" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-180x180.png" />
<meta name="msapplication-TileImage" content="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-270x270.png" />
<style>.pswp.pafe-lightbox-modal {display: none;}</style>		<style type="text/css" id="wp-custom-css">
			.elementor-section{
	overflow:hidden;
}

.justify-cstm .pp-timeline-card{
	text-align:justify !important;
}		</style>
		</head>
<body data-rsssl=1 class="home page-template page-template-elementor_header_footer page page-id-61379 elementor-default elementor-template-full-width elementor-kit-1399 elementor-page elementor-page-61379">

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>		<div data-elementor-type="header" data-elementor-id="8253" class="elementor elementor-8253 elementor-location-header">
		<div class="elementor-section-wrap">
					<header class="elementor-section elementor-top-section elementor-element elementor-element-4d3db0b elementor-section-height-min-height elementor-section-full_width elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="4d3db0b" data-element_type="section" data-settings="{&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_effects_offset&quot;:100,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;sticky_effects_offset_mobile&quot;:100,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e8e4520 wdp-sticky-section-no" data-id="e8e4520" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-7bf0d26 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7bf0d26" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-732e28e wdp-sticky-section-no" data-id="732e28e" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ed1a5bb logo wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="ed1a5bb" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="https://unityinvitation.com">
							<img width="779" height="480" src="/public/images/logo.png" class="attachment-large size-large" alt="" loading="lazy" srcset="/public/images/logo.png 779w, https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1-300x185.png 300w, https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1-768x473.png 768w" sizes="(max-width: 779px) 100vw, 779px" />								</a>
															</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</header>
				</div>
		</div>
				<div data-elementor-type="wp-post" data-elementor-id="61379" class="elementor elementor-61379">
									<section class="elementor-section elementor-top-section elementor-element elementor-element-400b879c elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="400b879c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-bottom" data-negative="true">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-844cb3c wdp-sticky-section-no" data-id="844cb3c" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2ea1a0c wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="2ea1a0c" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="home" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-eeb5441 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="eeb5441" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-cd3d5a9 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="cd3d5a9" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN<br>
<i>DIGITAL</i></h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-39b2faa elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="39b2faa" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a0ed427 wdp-sticky-section-no" data-id="a0ed427" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-28137a5 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="28137a5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a372e16 wdp-sticky-section-no" data-id="a372e16" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0f4cb48 elementor-hidden-desktop elementor-hidden-tablet wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="0f4cb48" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_rotateZ_effect&quot;:&quot;yes&quot;,&quot;motion_fx_rotateZ_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]},&quot;motion_fx_rotateZ_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="800" src="https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1024x1024.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1024x1024.png 1024w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-150x150.png 150w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-768x768.png 768w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1536x1536.png 1536w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-2048x2048.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
				<div class="elementor-element elementor-element-4d65149 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="4d65149" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-3c23e56 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3c23e56" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bagikan <i> Momenmu </i> bersama</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-8fc102d elementor-hidden-mobile wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="8fc102d" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Ara Invitation</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-9cd2440 elementor-hidden-desktop elementor-hidden-tablet wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9cd2440" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Ara</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-06f53ef elementor-hidden-desktop elementor-hidden-tablet wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="06f53ef" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Invitation</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ff5f269 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="ff5f269" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-82b1004 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="82b1004" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Dengan produk kami yang</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-8ff9412 uael-show-cursor-yes wdp-sticky-section-no elementor-widget elementor-widget-uael-fancy-heading" data-id="8ff9412" data-element_type="widget" data-settings="{&quot;fancytext_align_mobile&quot;:&quot;center&quot;,&quot;fancytext_align&quot;:&quot;center&quot;,&quot;fancytext_space_prefix&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;0&quot;,&quot;sizes&quot;:[]},&quot;fancytext_space_prefix_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;fancytext_space_prefix_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;fancytext_min_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;fancytext_min_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]},&quot;fancytext_min_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:[]}}" data-widget_type="uael-fancy-heading.default">
				<div class="elementor-widget-container">
					<div class="uael-module-content uael-fancy-text-node uael-clip-cursor-yes" data-type-speed="120" data-animation="type" data-back-speed="60" data-start-delay="0" data-back-delay="1200" data-loop="true" data-show-cursor="true" data-cursor-char="|" data-strings="[&quot;Minimalist&quot;,&quot;Simple&quot;,&quot;Elegant&quot;,&quot;Aesthetic&quot;]">
											<h3 class="uael-fancy-text-wrap uael-fancy-text-type">
											<span class="uael-fancy-stack">
											<span class="uael-fancy-heading uael-fancy-text-main uael-typed-main-wrap "><span class="uael-typed-main"></span><span class="uael-text-holder">.</span></span>
												</span>
									</h3>					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-49a7809 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="49a7809" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-36e44d5 wdp-sticky-section-no" data-id="36e44d5" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-19768a3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="19768a3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-9505f6c elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="9505f6c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;sticky&quot;:&quot;top&quot;,&quot;sticky_offset&quot;:100,&quot;sticky_offset_mobile&quot;:60,&quot;sticky_effects_offset_mobile&quot;:70,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_effects_offset&quot;:0}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-0b27f6f wdp-sticky-section-no" data-id="0b27f6f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f10233 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3f10233" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><a href="#home">HOME</a></h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7252e48 wdp-sticky-section-no" data-id="7252e48" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-d094013 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d094013" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><a href="#produk">PRODUK</a></h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-5a0f0f9 wdp-sticky-section-no" data-id="5a0f0f9" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-12b4d2c wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="12b4d2c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><a href="#FAQ">FAQ</a></h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-90981f5 wdp-sticky-section-no" data-id="90981f5" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ac3672f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ac3672f" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><a href="#tentang">TENTANG</a></h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-3cb0168 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="3cb0168" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e1b3373 wdp-sticky-section-no" data-id="e1b3373" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0901b1a pp-image-slider-carousel pp-ins-normal pp-ins-hover-normal wdp-sticky-section-no elementor-widget elementor-widget-pp-image-slider" data-id="0901b1a" data-element_type="widget" data-settings="{&quot;skin&quot;:&quot;carousel&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;infinite_loop&quot;:&quot;yes&quot;}" data-widget_type="pp-image-slider.default">
				<div class="elementor-widget-container">
							<div class="pp-image-slider-container">
			<div class="pp-image-slider-wrap swiper-container-wrap">
				<div class="pp-image-slider-box">
					<div class="pp-image-slider pp-swiper-slider swiper-container" id="pp-image-slider-0901b1a" data-slider-settings="{&quot;direction&quot;:&quot;horizontal&quot;,&quot;speed&quot;:1500,&quot;effect&quot;:&quot;slide&quot;,&quot;slidesPerView&quot;:3,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10,&quot;autoHeight&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:{&quot;delay&quot;:1000},&quot;breakpoints&quot;:{&quot;1025&quot;:{&quot;slidesPerView&quot;:3,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;768&quot;:{&quot;slidesPerView&quot;:2,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;320&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10}}}">
						<div class="swiper-wrapper">
										<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2022/03/Adat-Mock-Display-Home-1.png" alt="Adat Mock Display Home" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2022/03/Diamond-Mock-Display-Home.png" alt="Diamond Mock Display Home" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2022/03/Gold-Mock-Display-Home.png" alt="Gold Mock Display Home" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2022/03/Platinum-Mock-Display-Home.png" alt="Platinum Mock Display Home" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2022/03/Silver-Mock-Display-Home.png" alt="Silver Mock Display Home" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
									</div>
					</div>
									</div>
			</div>
					</div>
					</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-cd2fd4e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="cd2fd4e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-753331f wdp-sticky-section-no" data-id="753331f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-61d3b10 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="61d3b10" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_rotateZ_effect&quot;:&quot;yes&quot;,&quot;motion_fx_rotateZ_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1,&quot;sizes&quot;:[]},&quot;motion_fx_rotateZ_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="800" src="https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1024x1024.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1024x1024.png 1024w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-150x150.png 150w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-768x768.png 768w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-1536x1536.png 1536w, https://unityinvitation.com/wp-content/uploads/2022/04/star-icon-2048x2048.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
				<div class="elementor-element elementor-element-7fb240d wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="7fb240d" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">APA KATA <br><i>MEREKA</i></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-5e3946b bdt-navigation-type-dots wdp-sticky-section-no elementor-widget elementor-widget-bdt-fancy-slider" data-id="5e3946b" data-element_type="widget" data-widget_type="bdt-fancy-slider.default">
				<div class="elementor-widget-container">
					<div id="bdt-fancy-slider-5e3946b" class="bdt-fancy-slider bdt-dots-align-bottom-center" data-settings="{&quot;autoplay&quot;:{&quot;delay&quot;:4000},&quot;loop&quot;:true,&quot;speed&quot;:2500,&quot;effect&quot;:&quot;fade&quot;,&quot;fadeEffect&quot;:{&quot;crossFade&quot;:true},&quot;lazy&quot;:true,&quot;slidesPerView&quot;:1,&quot;navigation&quot;:{&quot;nextEl&quot;:&quot;#bdt-fancy-slider-5e3946b .bdt-navigation-next&quot;,&quot;prevEl&quot;:&quot;#bdt-fancy-slider-5e3946b .bdt-navigation-prev&quot;},&quot;pagination&quot;:{&quot;el&quot;:&quot;#bdt-fancy-slider-5e3946b .swiper-pagination&quot;,&quot;type&quot;:&quot;bullets&quot;,&quot;clickable&quot;:&quot;true&quot;,&quot;dynamicBullets&quot;:false},&quot;scrollbar&quot;:{&quot;el&quot;:&quot;#bdt-fancy-slider-5e3946b .swiper-scrollbar&quot;,&quot;hide&quot;:&quot;true&quot;}}">
			<div class="swiper-container">
				<div class="swiper-wrapper">
		
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-a96aab3">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-AditBonita.jpg" alt="Adit &amp; Bonita">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Jakarta Pusat)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Adit &amp; Bonita													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Pengalaman order undangan web di Ara Invitation
emang mantep banget. Adminnya sabar banget
ngelayanin apa maunya cost, Design dan perpaduan
warna dari produknya juga kekinian banget. Apalagi
pas aku order lagi dapet dis." 						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-573fa76">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-FerdianSari.jpg" alt="Ferdian &amp; Sari Suria">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kab. Karangasem, Bali)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Ferdian &amp; Sari Suria													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Sudah fix kak, salam dari pacar saya, terima kasih banyak katanya,
maksimal sekalai hasilnya."						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-99ed085">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-ChristianGrace.jpg" alt="Christian &amp; Grace">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Jakarta Pusat)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Christian &amp; Grace													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Saya sm pasangan merasa puas pake jasa kakak,
bagus banget hasilnya dan diluar ekspektasi kita
sih, sampe bagus seperti ini.. Pengerjaan cepat
dan harga okee bangett dengan hasilnya.. Tp banget
deh kak.."						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-c86186a">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-InggridYudha.jpg" alt="Inggrid &amp; Yudha">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Jakarta Timur)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Inggrid &amp; Yudha													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Seneng banget pokoknya setelah milih2 mau pake 
undangan website yang mana.. tiba2 muncul
Ara invitation di instagram, trs langsung fallin in love
sma template nya yang aesthetic bgt, pilihannya jg banyak,
udh gitu kakaknya juga ramah bgt, jelasinnya detail dan
cuma revisi dikit doang, Lucky bgt deh ganyesel pokoknya,
sukses terus kak..!!"
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-2315514">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-RanaAzhar.jpg" alt=" Rana &amp; Azhar">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Bandung)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															 Rana &amp; Azhar													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Terima kasih Unity atas jasanya.. Pengerjaan yang teliti, cepat
dan konsisten dalam pengerjaannya..
Serata ramah dalam konsultasi, cepat reaspon, saya
ga salah pilih unity,
Terima Kasih sukses terus buat unity.. semoga semakin maju yaa."
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-0b01588">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-ValerieWisnu.jpg" alt="Valerie &amp; Wisnu">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Bogor, Jawa Barat)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Valerie &amp; Wisnu													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Desainnya menarik, editornya baik banget, revisi berkali kali
tapi tetep sabar ngelayanin, pokoknya keren banget."						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-cb8e94e">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-AnnisaReza.jpg" alt="Anissa &amp; Reza">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Kendari, Sulawesi Tenggara)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Anissa &amp; Reza													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Makasih yaaa kak btw aku suka banget pelayanan di unity, fast respon, solutif, dan mau denger konsumen. Fiturnya juga lengkap dan afordable tentunya. Semoga bisa lebih berkembang lagi yaaa kak, aku doain semoga makin dikenal banyak orang."						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-89bf562">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-FerryReza.jpg" alt="Ferry &amp; Risa">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kab. Tangerang, Banten)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Ferry &amp; Risa													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Thank you so much Ara invitation. Puas sekali dengan pelayanan nya, puas sama hasil akhir nya. Beberapa kali revisi, tapi di tanggapi dengan ramah. Sangat rekomen, sukses trus untuk Ara invitation."
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-e1498b9">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-ArgyaRavli.jpg" alt="Argya &amp; Ravli">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Jakarta Timur)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Argya &amp; Ravli													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Waah servicenya oke banget kak, ini gak perez.. Selalu fast response even udah malem atau lagi weekend/tgl merah. Penjelasannya selalu jelas dan selalu dijawab pertanyaanku tentang beberapa requestku. Selesai lengkap ngirim datapun cepet bgt undangannya selesai, ada sedikit revisipun jg cepet bgt direvisinya"
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-78c6cf0">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-DesianaHandoko.jpg" alt="Desiana &amp; Handoko">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kab. Jepara, Jawa Tengah)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Desiana &amp; Handoko													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Sukses selalu yaaa kaakkk untuk Ara invitation..  Alhamdulillah puas dg hasilnyaa dan ga nyesel pilih unity sbg salah satu bagian dr hari bahagiaku."
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-547e4cb">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-Bayutessa.jpg" alt="Bayu &amp; Tessa">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Palembang, Sumatera Selatan)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Bayu &amp; Tessa													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Desainnyaa bagus, sesuai ekspektasi, &amp; adminnyaa jugaa fast respon, semoga sukses terus ya."
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-ae09979">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-LuberPuja.jpg" alt="Luber &amp; Puja">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kab. Kerinci, Jambi)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Luber &amp; Puja													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Alhamdulilah adminnya ramah banget, pas pertama kali chat nanya-nanya, fast respon, baik banget.. Pokoknya syuka."
						</div>
					
									</div>

		
			        </div>

				
					<div class="swiper-slide bdt-fancy-slider-item elementor-repeater-item-4796ffd">

						
				<div class="bdt-fancy-slider-image">
											<div class="bdt-slide-image swiper-lazy">
							
            <img src="https://unityinvitation.com/wp-content/uploads/2022/04/Display-Testi-Home-AmmandaDanang.jpg" alt="Ammanda &amp; Danang">

        							<div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
						</div>
									</div>

				<div class="bdt-fancy-slider-content">
											<div class="bdt-fancy-slider-subtitle">
							(Kota Surabaya, Jawa Timur)						</div>
					
											<h2 class="bdt-fancy-slider-title">
															Ammanda &amp; Danang													</h2>
					
											<div class="bdt-fancy-slider-description">
							"Thankyou kakak kakak unity yang menyanggupi segala keinginan kami. Semoga lancar usahanya dan sampai jumpa di agenda2 lainnya."
						</div>
					
									</div>

		
			        </div>

							</div>
						</div>
			
											<div class="bdt-position-z-index bdt-position-bottom-center">
					<div class="bdt-dots-container">
						<div class="swiper-pagination"></div>
					</div>
				</div>
			
													
			</div>
			
					</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-63c8a768 elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="63c8a768" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;curve&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
    <path class="elementor-shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-18bef385 wdp-sticky-section-no" data-id="18bef385" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-fc1b942 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="fc1b942" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">KENAPA <i> KAMU</i> HARUS<br>MEMILIH KAMI</h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-c29a2a8 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c29a2a8" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-109e06e wdp-sticky-section-no" data-id="109e06e" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-d26fbc0 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d26fbc0" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d84c5e3 wdp-sticky-section-no" data-id="d84c5e3" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-a93097e wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="a93097e" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Desain Eksklusif</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-1754d10 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1754d10" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Banyak pilihan desain yang Estetik, Minimalis, Simpel, & Elegan.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-585309f wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="585309f" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-2297fba wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="2297fba" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Harga Terjangkau</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ff0963c wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ff0963c" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Dengan harga yang terjangkau, kamu tetap dapat menyebarkan hari bahagiamu secara eksklusif.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-cc67ab0 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="cc67ab0" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9ea4fad wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9ea4fad" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Pemesanan Mudah</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-719eecb wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="719eecb" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Sudah kami sediakan cara mudah dalam pemesanan undangan.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-054c18f wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="054c18f" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-9732feb wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="9732feb" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Fitur Terbaik</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-7ea79b0 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="7ea79b0" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Kami menyajikan fitur-fitur terbaik, menarik & kekinian.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-7337ba5 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7337ba5" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-243ccf7 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="243ccf7" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Professional Team</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-3018db5 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3018db5" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Dihandle langsung oleh tim professional.</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-7ae1e95e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7ae1e95e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;curve&quot;,&quot;shape_divider_bottom&quot;:&quot;curve&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;}">
					<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
    <path class="elementor-shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"/>
</svg>		</div>
				<div class="elementor-shape elementor-shape-bottom" data-negative="true">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
	<path class="elementor-shape-fill" d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-30a0be2c wdp-sticky-section-no" data-id="30a0be2c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-b796e28 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="b796e28" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="produk" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-bb2d2ae wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="bb2d2ae" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-ec998ec wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ec998ec" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN <i>WEBSITE</i></h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-256e452 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="256e452" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-4c6cdf7 wdp-sticky-section-no" data-id="4c6cdf7" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-7414603 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7414603" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-74723a9 wdp-sticky-section-no" data-id="74723a9" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8f7a703 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="8f7a703" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN WEBSITE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-90f6d35 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="90f6d35" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Adat</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-129e4a1 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="129e4a1" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-b51c561 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="b51c561" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="427" height="640" src="https://unityinvitation.com/wp-content/uploads/2021/12/Adat-Mock.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/Adat-Mock.png 427w, https://unityinvitation.com/wp-content/uploads/2021/12/Adat-Mock-200x300.png 200w" sizes="(max-width: 427px) 100vw, 427px" />															</div>
				</div>
				<div class="elementor-element elementor-element-9f7e4bd elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="9f7e4bd" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="adat-detail/" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Lihat Detail</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-04a87bc wdp-sticky-section-no" data-id="04a87bc" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ed3f7ef wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="ed3f7ef" data-element_type="widget" data-widget_type="html.default">
				<div class="elementor-widget-container">
			<div class="box">
   <div class="ribbon"><span>BEST SELLER</span></div>
</div>

<style>
    
 .ribbon {
  position: absolute;
  right: -5px; top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px; height: 75px;
  text-align: right;
}
.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#2989d8 0%, #1e5799 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px; right: -21px;
}
.ribbon span::before {
  content: "";
  position: absolute; left: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid #1e5799;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #1e5799;
}
.ribbon span::after {
  content: "";
  position: absolute; right: 0px; top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #1e5799;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #1e5799;
}
</style>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-1a19438 elementor-section-full_width myPricing elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="1a19438" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-0d65422 wdp-sticky-section-no" data-id="0d65422" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-11551c0 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="11551c0" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN WEBSITE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-a2f51d7 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="a2f51d7" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Diamond</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-90236f9 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="90236f9" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-4306cab wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="4306cab" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="427" height="640" src="https://unityinvitation.com/wp-content/uploads/2021/12/Diamond-Mock.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/Diamond-Mock.png 427w, https://unityinvitation.com/wp-content/uploads/2021/12/Diamond-Mock-200x300.png 200w" sizes="(max-width: 427px) 100vw, 427px" />															</div>
				</div>
				<div class="elementor-element elementor-element-02731b9 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="02731b9" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="diamond-detail/" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Lihat Detail</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-7e9733d wdp-sticky-section-no" data-id="7e9733d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-cdb9cba elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="cdb9cba" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-c1e3a8e wdp-sticky-section-no" data-id="c1e3a8e" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c6af459 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="c6af459" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN WEBSITE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-256ed64 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="256ed64" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Platinum</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-95f33bc elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="95f33bc" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-949abb7 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="949abb7" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="427" height="640" src="https://unityinvitation.com/wp-content/uploads/2021/12/Platinum-Mock1.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/Platinum-Mock1.png 427w, https://unityinvitation.com/wp-content/uploads/2021/12/Platinum-Mock1-200x300.png 200w" sizes="(max-width: 427px) 100vw, 427px" />															</div>
				</div>
				<div class="elementor-element elementor-element-b8e477e elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="b8e477e" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="platinum-detail/" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Lihat Detail</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-4fbcc23 wdp-sticky-section-no" data-id="4fbcc23" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-c890a74 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c890a74" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a78de24 wdp-sticky-section-no" data-id="a78de24" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ade29ae wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ade29ae" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN WEBSITE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-6f414cf wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6f414cf" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Gold</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c8b58c1 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="c8b58c1" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-a584476 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="a584476" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="427" height="640" src="https://unityinvitation.com/wp-content/uploads/2021/12/Gold-Mock3.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/Gold-Mock3.png 427w, https://unityinvitation.com/wp-content/uploads/2021/12/Gold-Mock3-200x300.png 200w" sizes="(max-width: 427px) 100vw, 427px" />															</div>
				</div>
				<div class="elementor-element elementor-element-56d69b9 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="56d69b9" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="gold-detail/" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Lihat Detail</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-24507a0 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="24507a0" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-e7e123e wdp-sticky-section-no" data-id="e7e123e" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-f57d924 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f57d924" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d63f898 wdp-sticky-section-no" data-id="d63f898" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-d2ff0de wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="d2ff0de" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">UNDANGAN WEBSITE</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-6dae5ed wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6dae5ed" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Silver</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-f41df89 elementor-icon-list--layout-inline elementor-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="f41df89" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-star"></i>						</span>
										<span class="elementor-icon-list-text"></span>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-2a28d16 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="2a28d16" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="427" height="640" src="https://unityinvitation.com/wp-content/uploads/2021/12/Silver-Mock2.png" class="elementor-animation-grow attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/12/Silver-Mock2.png 427w, https://unityinvitation.com/wp-content/uploads/2021/12/Silver-Mock2-200x300.png 200w" sizes="(max-width: 427px) 100vw, 427px" />															</div>
				</div>
				<div class="elementor-element elementor-element-61f7eb7 elementor-align-center wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="61f7eb7" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="silver-detail/" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Lihat Detail</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-cf620f0 wdp-sticky-section-no" data-id="cf620f0" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-7986c59 wdp-sticky-section-no" data-id="7986c59" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-675f3a2 wdp-sticky-section-no" data-id="675f3a2" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-776c7a9 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="776c7a9" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-14d67dc elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="14d67dc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4998694 wdp-sticky-section-no" data-id="4998694" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-98c5c3b wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="98c5c3b" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="testimoni" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-c80aa4b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="c80aa4b" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-820eab9 elementor-pagination-position-outside wdp-sticky-section-no elementor-widget elementor-widget-image-carousel" data-id="820eab9" data-element_type="widget" data-settings="{&quot;slides_to_show&quot;:&quot;6&quot;,&quot;slides_to_show_mobile&quot;:&quot;2&quot;,&quot;navigation&quot;:&quot;dots&quot;,&quot;autoplay_speed&quot;:2000,&quot;speed&quot;:1500,&quot;image_spacing_custom&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;autoplay&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;infinite&quot;:&quot;yes&quot;}" data-widget_type="image-carousel.default">
				<div class="elementor-widget-container">
					<div class="elementor-image-carousel-wrapper swiper-container" dir="ltr">
			<div class="elementor-image-carousel swiper-wrapper">
								<!-- <div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE1OTQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvQmFudGEtVWxmYS5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Banta-Ulfa.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Banta-Ulfa.png" alt="Banta &amp; Ulfa" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MDMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvVGFtaS1EaWRpay5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Tami-Didik.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Tami-Didik.png" alt="Tami &amp; Didik" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MDIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvU2FudGktRGhpa2EucG5nIiwic2xpZGVzaG93IjoiODIwZWFiOSJ9" href="https://unityinvitation.com/wp-content/uploads/2022/04/Santi-Dhika.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Santi-Dhika.png" alt="Santi &amp; Dhika" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MDEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvUml6a2EtTnVyaXMucG5nIiwic2xpZGVzaG93IjoiODIwZWFiOSJ9" href="https://unityinvitation.com/wp-content/uploads/2022/04/Rizka-Nuris.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Rizka-Nuris.png" alt="Rizka &amp; Nuris" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MDAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvUmVuaS1ZYW5hLnBuZyIsInNsaWRlc2hvdyI6IjgyMGVhYjkifQ%3D%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Reni-Yana.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Reni-Yana.png" alt="Reni &amp; Yana" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE1OTcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvSGVkaS1XaW5keS5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Hedi-Windy.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Hedi-Windy.png" alt="Hedi &amp; Windy" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE1OTYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvRmFya2hhdHVuLUZhdGhhbi5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Farkhatun-Fathan.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Farkhatun-Fathan.png" alt="Farkhatun &amp; Fathan" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE1OTUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvRWxseXRhLVJvc3lpZC5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Ellyta-Rosyid.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Ellyta-Rosyid.png" alt="Ellyta &amp; Rosyid" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MTAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvSGVuaS1XYW4tMS5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Heni-Wan-1.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Heni-Wan-1.png" alt="Heni &amp; Wan" /></figure></a></div><div class="swiper-slide"><a data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="820eab9" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NjE2MTEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNFwvSWluLUxlc3RhcmktWWFzaW4tMS5wbmciLCJzbGlkZXNob3ciOiI4MjBlYWI5In0%3D" href="https://unityinvitation.com/wp-content/uploads/2022/04/Iin-Lestari-Yasin-1.png"><figure class="swiper-slide-inner"><img class="swiper-slide-image" src="https://unityinvitation.com/wp-content/uploads/2022/04/Iin-Lestari-Yasin-1.png" alt="Iin Lestari &amp; Yasin" /></figure></a></div>			</div>
												<div class="swiper-pagination"></div>
													</div> -->
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-8402511 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="8402511" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;curve&quot;}">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-shape elementor-shape-top" data-negative="false">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
    <path class="elementor-shape-fill" d="M1000,4.3V0H0v4.3C0.9,23.1,126.7,99.2,500,100S1000,22.7,1000,4.3z"/>
</svg>		</div>
					<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-67be24a7 wdp-sticky-section-no" data-id="67be24a7" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-55f65fa wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="55f65fa" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="FAQ" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-6b44821 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="6b44821" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0e37860 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="0e37860" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">FAQ</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-0c3c2cd wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="0c3c2cd" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Pertanyaan yang <br> sering ditanyakan</h2>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-6ed3d89 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6ed3d89" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-acec9c3 wdp-sticky-section-no" data-id="acec9c3" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-d1c13ef elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d1c13ef" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-21fb12b wdp-sticky-section-no" data-id="21fb12b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-bb099d5 wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-accordion" data-id="bb099d5" data-element_type="widget" data-settings="{&quot;toggle_speed&quot;:1000,&quot;accordion_type&quot;:&quot;accordion&quot;}" data-widget_type="pp-advanced-accordion.default">
				<div class="elementor-widget-container">
					<div class="pp-advanced-accordion pp-toggle-icon-align-right" id="pp-advanced-accordion-bb099d5" data-accordion-id="bb099d5" role="tablist">
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1961" class="pp-accordion-tab-title" tabindex="0" data-tab="1" role="tab" aria-controls="pp-accordion-tab-content-1961" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Bagaimana cara pemesanannya?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1961" class="pp-accordion-tab-content" data-tab="1" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1961">
						<p>1. Menentukan desain yang telah disediakan (bisa dikonsultasikan dengan admin sesuai kebutuhan).</p><p>2. Mengisi format order sesuai dengan ketentuan.</p><p>3. Konfirmasi pembayaran (pembayaran dilakukan full payment karena setiap pemesan yang masuk akan diproses setelah pembayaranb lunas).</p><p>4. Mengisi form data pernikahan yang sudah disediakan.</p><p>5. Proses pembuatan (estimasi pengerjaan 5 hari setelah melakukan order &amp; melengkapi data).</p><p>6. Setelah selesai, undangan akan dikirim ke pemesan.</p><p>7. Silahkan lakukan review hasil undangan apakah ada yang perlu dikoreksi atau revisi.</p><p>8. Jika ada revisi, infokan list revisi apa saja yang diperlukan jadi silahkan langsung list revisinya secara keseluruhan karena jatah revisi terbatas. Jadi silahkan manfaatkan seefektif mungkin.</p><p>9. Jika telah memberikan list revisi, revisi akan dikerjakan hari itu juga atau h+1<br />10. DONE! Undangan siap disebar dengan tools yang telah kami sediakan.</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1962" class="pp-accordion-tab-title" tabindex="0" data-tab="2" role="tab" aria-controls="pp-accordion-tab-content-1962" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Berapa lama proses pembuatannya?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1962" class="pp-accordion-tab-content" data-tab="2" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1962">
						<p>Pengerjaan 5 hari setelah melakukan order &amp; melengkapi data</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1963" class="pp-accordion-tab-title" tabindex="0" data-tab="3" role="tab" aria-controls="pp-accordion-tab-content-1963" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Kapan data harus lengkap?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1963" class="pp-accordion-tab-content" data-tab="3" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1963">
						<p>Maksimal 3 hari setelah melakukan pemesanan</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1964" class="pp-accordion-tab-title" tabindex="0" data-tab="4" role="tab" aria-controls="pp-accordion-tab-content-1964" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Undangan website yang mengisi nama tamu siapa?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1964" class="pp-accordion-tab-content" data-tab="4" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1964">
						<p>Untuk nama tamu nanti yang mengisi <strong>kaka sendiri</strong> ya, setelah undangan jadi akan kami kasih tools &amp; memberitahu cara mengisi dan mengubah nama tamunya.</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1965" class="pp-accordion-tab-title" tabindex="0" data-tab="5" role="tab" aria-controls="pp-accordion-tab-content-1965" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Apa itu masa aktif website?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1965" class="pp-accordion-tab-content" data-tab="5" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1965">
						<p>Setiap paket undangan website memiliki masa aktif yang berbeda.</p><p>Masa Aktif artinya seberapa lama link undangan website kakak dapat diakses oleh publik. Terhitung dari sejak tanggal acara undangan website kakak.</p><p>Setelah melewati masa aktif, undangan website kakak akan memasuki masa <em>expired</em> secara otomatis dan tidak lagi dapat diakses oleh siapapun.</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1966" class="pp-accordion-tab-title" tabindex="0" data-tab="6" role="tab" aria-controls="pp-accordion-tab-content-1966" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Bisa upgrade tema yang dipesan tidak?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1966" class="pp-accordion-tab-content" data-tab="6" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1966">
						<p>Bisa, asal konfirmasi terlebih dahulu sebelum proses pengerjaan</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-1967" class="pp-accordion-tab-title" tabindex="0" data-tab="7" role="tab" aria-controls="pp-accordion-tab-content-1967" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Ucapan pada undangan website apakah bisa dihapus?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-1967" class="pp-accordion-tab-content" data-tab="7" role="tabpanel" aria-labelledby="pp-accordion-tab-title-1967">
						<p>Ucapan yang telah diisi oleh tamu undangan pada undangan website BISA DIHAPUS.</p><p>Ketentuan untuk undangan dengan ucapan langsung di Website :</p><p>Jika ada komentar atau ucapan yang tidak diinginkan, bisa dihapus sesuai request.</p>					</div>
				</div>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-50268ae wdp-sticky-section-no" data-id="50268ae" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-51d41b3 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="51d41b3" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-da032fb wdp-sticky-section-no" data-id="da032fb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-28c256e wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-accordion" data-id="28c256e" data-element_type="widget" data-settings="{&quot;toggle_speed&quot;:1000,&quot;accordion_type&quot;:&quot;accordion&quot;}" data-widget_type="pp-advanced-accordion.default">
				<div class="elementor-widget-container">
					<div class="pp-advanced-accordion pp-toggle-icon-align-right" id="pp-advanced-accordion-28c256e" data-accordion-id="28c256e" role="tablist">
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4271" class="pp-accordion-tab-title" tabindex="0" data-tab="1" role="tab" aria-controls="pp-accordion-tab-content-4271" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Apakah jika mengurangi fitur, mengurangi harga juga?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4271" class="pp-accordion-tab-content" data-tab="1" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4271">
						<p>Tidak, Harga tetap sama dengan paket yang dipilih, Mengurangi fitur tidak mengurangi harga</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4272" class="pp-accordion-tab-title" tabindex="0" data-tab="2" role="tab" aria-controls="pp-accordion-tab-content-4272" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Jika tanggal pernikahan belum jelas, apakah bisa pesan undangan terlebih dahulu?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4272" class="pp-accordion-tab-content" data-tab="2" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4272">
						<p>Bisa ya kak</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4273" class="pp-accordion-tab-title" tabindex="0" data-tab="3" role="tab" aria-controls="pp-accordion-tab-content-4273" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Apakah bisa DP terlebih dahulu?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4273" class="pp-accordion-tab-content" data-tab="3" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4273">
						<p>Tidak bisa kak. Wajib full payment ya</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4274" class="pp-accordion-tab-title" tabindex="0" data-tab="4" role="tab" aria-controls="pp-accordion-tab-content-4274" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Apa saja list backsound/music untuk undangannya?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4274" class="pp-accordion-tab-content" data-tab="4" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4274">
						Untuk list Backsound bisa dicek di sini ya &gt;&gt;<a title="Daftar Musik Latar" href="https://unityinvitation.com/musik/">Daftar Musik Latar</a>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4275" class="pp-accordion-tab-title" tabindex="0" data-tab="5" role="tab" aria-controls="pp-accordion-tab-content-4275" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Apakah bisa revisi?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4275" class="pp-accordion-tab-content" data-tab="5" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4275">
						<p>Tentu bisa kak.</p><p> </p><p>Revisi terbagi 2 yaitu revisi minor dan revisi mayor.</p><p>Revisi Minor, yaitu revisi data, revisi redaksi tulisan pada paragraf tertentu, penambahan gelar pada nama pengantin, ubah tanggal acara pernikahan, ubah informasi link streaming, hilangkan bagian tertentu atau perubahan info lainnya seperti yang tercantum pada saat Anda mengisi form pemesanan. (Jatah 3x revisi)</p><p> </p><p>Revisi Mayor, yaitu revisi yang merubah foto, video, ataupun warna (tidak merubah warna dari sebuah gambar atau ornament lainnya seperti bunga). (Jatah 1x revisi)</p><p> </p><p>Adapun untuk request penambahan / perubahan diluar dari desain atau paket yang sudah disediakan maka ini tidak termasuk revisi dan akan dikenakan biaya tambahan.</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4276" class="pp-accordion-tab-title" tabindex="0" data-tab="6" role="tab" aria-controls="pp-accordion-tab-content-4276" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Kenapa kami membatasi revisi?							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4276" class="pp-accordion-tab-content" data-tab="6" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4276">
						<p>Karena client kami bukan hanya kakak seorang dan jika kami memberikan revisi minor &amp; mayor sebanyak-banyaknya itu akan membutuhkan waktu yang sangat lama hanya untuk 1 client saja. Namun jika kakak merasa masih ada yang perlu di revisi setelah jatah revisi habis, kakak bisa memesan Addon Fitur Revisi. Fitur ini akan memberikan kakak tambahan jatah revisi 1 kali list lagi (berlaku kelipatan).</p>					</div>
				</div>
							<div class="pp-accordion-item">
										<div id="pp-accordion-tab-title-4277" class="pp-accordion-tab-title" tabindex="0" data-tab="7" role="tab" aria-controls="pp-accordion-tab-content-4277" aria-expanded="false">
						<span class="pp-accordion-title-icon">
															<span class="pp-accordion-tab-icon pp-icon">
									<i aria-hidden="true" class="fas fa-asterisk"></i>								</span>
														<span class="pp-accordion-title-text">
								Perlu Diperhatikan							</span>
						</span>
													<div class="pp-accordion-toggle-icon">
																	<span class='pp-accordion-toggle-icon-close pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-down"></i>									</span>
																									<span class='pp-accordion-toggle-icon-open pp-icon'>
										<i aria-hidden="true" class="fas fa-caret-up"></i>									</span>
															</div>
											</div>

					<div id="pp-accordion-tab-content-4277" class="pp-accordion-tab-content" data-tab="7" role="tabpanel" aria-labelledby="pp-accordion-tab-title-4277">
						<p>Perlu diperhatikan bahwa : “Data yang digunakan dan disebarkan di dalam undangan bukan menjadi tanggung jawab kami.”</p>					</div>
				</div>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-1de9c6a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="1de9c6a" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-f7005bc wdp-sticky-section-no" data-id="f7005bc" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ffd96f4 elementor-shape-circle elementor-widget__width-auto elementor-fixed animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="ffd96f4" data-element_type="widget" data-settings="{&quot;sticky&quot;:&quot;bottom&quot;,&quot;_position&quot;:&quot;fixed&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:3000,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-8da879e" href="https://api.whatsapp.com/send?phone=62895367255770&#038;text=Halo%20Kak,%20saya%20mau%20order%20Undangan%20WEBnya%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
						<span class="elementor-screen-only">Whatsapp</span>
						<i class="fab fa-whatsapp"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-c1d04a5 wdp-sticky-section-no" data-id="c1d04a5" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-c3777be wdp-sticky-section-no" data-id="c3777be" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-b0887b6 wdp-sticky-section-no" data-id="b0887b6" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
							</div>

				<div id="tentang" class="elementor-menu-anchor"></div>
				<div data-elementor-type="footer" data-elementor-id="8886" class="elementor elementor-8886 elementor-location-footer">
		<div class="elementor-section-wrap">
					<footer class="elementor-section elementor-top-section elementor-element elementor-element-3f774c2 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="3f774c2" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7b2cb37 wdp-sticky-section-no" data-id="7b2cb37" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-6dd445d elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6dd445d" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-699a44b wdp-sticky-section-no" data-id="699a44b" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-65013ee wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="65013ee" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Kenal lebih <i> dekat</i><br> dengan kami</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-cba8df7 elementor-icon-list--layout-inline elementor-align-center elementor-mobile-align-center elementor-list-item-link-full_width wdp-sticky-section-no elementor-widget elementor-widget-icon-list" data-id="cba8df7" data-element_type="widget" data-widget_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items elementor-inline-items">
							<li class="elementor-icon-list-item elementor-inline-item">
											<a href="https://www.instagram.com/ar.rtr/" target="_blank">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-instagram"></i>						</span>
										<span class="elementor-icon-list-text"></span>
											</a>
									</li>
								<li class="elementor-icon-list-item elementor-inline-item">
											<a href="https://api.whatsapp.com/send?phone=62895367255770&#038;text=Halo%20Kak,%20saya%20mau%20order%20Undangan%20WEBnya%20nih.%20Bisa%20dibantu?">

												<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fab fa-whatsapp"></i>						</span>
										<span class="elementor-icon-list-text"></span>
											</a>
									</li>
						</ul>
				</div>
				</div>
				<div class="elementor-element elementor-element-f2c7673 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="f2c7673" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Jln. Raya Sumberdadi, Sumberdadi, Sumbergempol, Tulungagung, Jawa Timur, 66291</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-7b35851 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="7b35851" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="779" height="480" src="public/images/logo-reverse.png" class="attachment-large size-large" alt="" loading="lazy" srcset="public/images/logo-reverse.png 779w, https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos2-1-300x185.png 300w, https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos2-1-768x473.png 768w" sizes="(max-width: 779px) 100vw, 779px" />															</div>
				</div>
				<div class="elementor-element elementor-element-98a8c2e wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="98a8c2e" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">© 2020 Ara Invitation,<br>All Rights Reserved</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</footer>
				</div>
		</div>
		
		<!-- <div data-elementor-type="popup" data-elementor-id="68563" class="elementor elementor-68563 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeIn&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.2,&quot;sizes&quot;:[]},&quot;triggers&quot;:{&quot;page_load_delay&quot;:7,&quot;page_load&quot;:&quot;yes&quot;},&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-5a00900 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5a00900" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f3a8122 wdp-sticky-section-no" data-id="f3a8122" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-aa0c100 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="aa0c100" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="576" height="1024" src="https://unityinvitation.com/wp-content/uploads/2022/05/Promo-Bulan-May-rried-for-WEB-profile1-576x1024.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/05/Promo-Bulan-May-rried-for-WEB-profile1-576x1024.jpg 576w, https://unityinvitation.com/wp-content/uploads/2022/05/Promo-Bulan-May-rried-for-WEB-profile1-169x300.jpg 169w, https://unityinvitation.com/wp-content/uploads/2022/05/Promo-Bulan-May-rried-for-WEB-profile1.jpg 608w" sizes="(max-width: 576px) 100vw, 576px" />															</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div> -->
				<div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami.
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
				<div class="elementor-widget-container">
					<div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
			<div class="jet-tabs__control-wrapper">
				<div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png" alt=""></div></div><div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt=""></div></div><div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-Mandiri.png" alt=""></div></div>			</div>
			<div class="jet-tabs__content-wrapper">
				<div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div>			</div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Bimo Bramantyo</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
				<div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-no">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
				<div class="elementor-widget-container">
			
				<div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
								<nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}"><ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li> -->
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li> -->
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li> -->
</ul>
</li>
</ul></nav>
															<div class="pp-menu-toggle pp-menu-toggle-on-tablet">
											<div class="pp-hamburger">
							<div class="pp-hamburger-box">
																	<div class="pp-hamburger-inner"></div>
															</div>
						</div>
														</div>
												<nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
												<ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li> -->
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li> -->
	<!-- <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li> -->
</ul>
</li>
</ul>							</nav>
							</div>
						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		<link rel='stylesheet' id='fancybox-css'  href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='ep-font-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-font.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='ep-fancy-slider-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-fancy-slider.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51208-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51208.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='e-animations-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
<script type='text/javascript' id='WEDKU_SCRP-js-extra'>
/* <![CDATA[ */
var ceper = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.16' id='WEDKU_SCRP-js'></script>
<script type='text/javascript' id='wdp_js_script-js-extra'>
/* <![CDATA[ */
var WDP_WP = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","wdpNonce":"4bda080e1a","jpages":"true","jPagesNum":"5","textCounter":"true","textCounterNum":"500","widthWrap":"","autoLoad":"true","thanksComment":"Terima kasih atas ucapan & doanya!","thanksReplyComment":"Terima kasih atas balasannya!","duplicateComment":"You might have left one of the fields blank, or duplicate comments","insertImage":"Insert image","insertVideo":"Insert video","insertLink":"Insert link","checkVideo":"Check video","accept":"Accept","cancel":"Cancel","reply":"Balas","textWriteComment":"Tulis Ucapan & Doa","classPopularComment":"wdp-popular-comment","textUrlImage":"Url image","textUrlVideo":"Url video youtube or vimeo","textUrlLink":"Url link","textToDisplay":"Text to display","textCharacteresMin":"Minimal 2 karakter","textNavNext":"Selanjutnya","textNavPrev":"Sebelumnya","textMsgDeleteComment":"Do you want delete this comment?","textLoadMore":"Load more","textLikes":"Likes"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
<script type='text/javascript' id='uael-frontend-script-js-extra'>
/* <![CDATA[ */
var uael_script = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-js/uael-frontend.min.js?ver=1.36.5' id='uael-frontend-script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/lib/typed/typed.min.js?ver=1.36.5' id='uael-fancytext-typed-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/lib/rvticker/rvticker.min.js?ver=1.36.5' id='uael-fancytext-slidev-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.js?ver=2.8.2' id='jquery-fancybox-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/jquery-resize/jquery.resize.min.js?ver=0.5.3' id='jquery-resize-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' id='powerpack-frontend-js-extra'>
/* <![CDATA[ */
var ppLogin = {"empty_username":"Enter a username or email address.","empty_password":"Enter password.","empty_password_1":"Enter a password.","empty_password_2":"Re-enter password.","empty_recaptcha":"Please check the captcha to verify you are not a robot.","email_sent":"A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.","reset_success":"Your password has been reset successfully.","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
var ppRegistration = {"invalid_username":"This username is invalid because it uses illegal characters. Please enter a valid username.","username_exists":"This username is already registered. Please choose another one.","empty_email":"Please type your email address.","invalid_email":"The email address isn\u2019t correct!","email_exists":"The email is already registered, please choose another one.","password":"Password must not contain the character \"\\\\\"","password_length":"Your password should be at least 8 characters long.","password_mismatch":"Password does not match.","invalid_url":"URL seems to be invalid.","recaptcha_php_ver":"reCAPTCHA API requires PHP version 5.3 or above.","recaptcha_missing_key":"Your reCAPTCHA Site or Secret Key is missing!","show_password":"Show password","hide_password":"Hide password","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/imagesloaded.min.js?ver=4.1.4' id='imagesloaded-js'></script>
<script type='text/javascript' id='bdt-uikit-js-extra'>
/* <![CDATA[ */
var element_pack_ajax_login_config = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","language":"en","loadingmessage":"Sending user info, please wait...","unknownerror":"Unknown error, make sure access is correct!"};
var ElementPackConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"268d485f92","data_table":{"language":{"lengthMenu":"Show _MENU_ Entries","info":"Showing _START_ to _END_ of _TOTAL_ entries","search":"Search :","paginate":{"previous":"Previous","next":"Next"}}},"contact_form":{"sending_msg":"Sending message please wait...","captcha_nd":"Invisible captcha not defined!","captcha_nr":"Could not get invisible captcha response!"},"mailchimp":{"subscribing":"Subscribing you please wait..."},"elements_data":{"sections":[],"columns":[],"widgets":[]}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.6.4","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"theme_builder_v2":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true,"form-submissions":true},"urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"viewport_tablet":1024,"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes"},"post":{"id":61379,"title":"Unity%20Invitation","excerpt":"","featuredImage":"https:\/\/unityinvitation.com\/wp-content\/uploads\/2022\/04\/Unity-Logo.jpg"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-fancy-slider.min.js?ver=6.0.10' id='ep-fancy-slider-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"f02c3dc1b5","urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/","rest":"https:\/\/unityinvitation.com\/wp-json\/"},"i18n":{"toc_no_headings_found":"No headings were found on this page."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
<script type='text/javascript' id='jet-elements-js-extra'>
/* <![CDATA[ */
var jetElements = {"ajaxUrl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template","devMode":"false","messages":{"invalidMail":"Please specify a valid e-mail"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
<script type='text/javascript' id='jet-tabs-frontend-js-extra'>
/* <![CDATA[ */
var JetTabsSettings = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template","devMode":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
<script type='text/javascript' id='weddingpress-wdp-js-extra'>
/* <![CDATA[ */
var cevar = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","plugin_url":"https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
<div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div><div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>
</body>
</html>
