<?php
	include "data.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Ara Invitation</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="public/images/favicon.png" rel="icon">
  <link href="public/images/favicon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="public/landingpage/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="public/landingpage/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="public/landingpage/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="public/landingpage/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="public/landingpage/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="public/landingpage/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="public/landingpage/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: KnightOne - v4.7.0
  * Template URL: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container-fluid">

      <div class="row justify-content-center">
        <div class="col-xl-9 d-flex align-items-center justify-content-lg-between">
          <h1 class="logo me-auto me-lg-0">
            <!-- <a href="index.html">KnightOne</a> -->
            <img src="public/images/logo-reverse.png" alt="">
          </h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="index.html" class="logo me-auto me-lg-0"><img src="public/landingpage/assets/img/logo.png" alt="" class="img-fluid"></a>-->

          <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
              <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
              <li><a class="nav-link scrollto" href="#about">Tentang</a></li>
              <li><a class="nav-link scrollto" href="#services">Produk</a></li>
              <li><a class="nav-link scrollto" href="#info">Info</a></li>
              <!-- <li><a class="nav-link scrollto " href="#portfolio">Portfolio</a></li> -->
              <li><a class="nav-link scrollto" href="#faq">FAQ</a></li>
              <!-- <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                <ul>
                  <li><a href="#">Drop Down 1</a></li>
                  <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                    <ul>
                      <li><a href="#">Deep Drop Down 1</a></li>
                      <li><a href="#">Deep Drop Down 2</a></li>
                      <li><a href="#">Deep Drop Down 3</a></li>
                      <li><a href="#">Deep Drop Down 4</a></li>
                      <li><a href="#">Deep Drop Down 5</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Drop Down 2</a></li>
                  <li><a href="#">Drop Down 3</a></li>
                  <li><a href="#">Drop Down 4</a></li>
                </ul>
              </li> -->
              <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
          </nav><!-- .navbar -->

          <!-- <a href="#about" class="get-started-btn scrollto">Get Started</a> -->
        </div>
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-xl-8">
          <h1>Ara Invitation - Undangan website no.1 di Indonesia</h1>
          <h2>Jasa pembuatan undangan website murah namun hasil tidak murahan</h2>
          <!-- <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a> -->
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <div class="text-center pt-10">
      <img src="public/images/promo/promo-new-year.jpg" alt="" style="width: 100%;">
    </div>

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container">
        
        <div class="section-title">
          <h2>Tentang kami</h2>
          <p>Ara Invitation menerima pembuatan undangan pernikahan website yang sangat menarik dan interaktif dengan harga yang sangat terjangkau untuk semua kalangan. Banyak promo yang bisa ada dapatkan setiap harinya.</p>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <!-- <div class="row content">
          <div class="col-lg-6">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
              <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit</li>
              <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
            </ul>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <a href="#" class="btn-learn-more">Learn More</a>
          </div>
        </div> -->

      </div>
    </section>
    <!-- End About Us Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services about">
      <div class="container">

        <div class="section-title">
          <h2>Produk</h2>
          <p>Terdapat banyak macam tema yang bisa dipilih.</p>
        </div>

        <div class="row">
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bxl-dribbble"></i></div>
                  <h4><a href="">Adat Jawa</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 500.000</del><p>
                  <p>IDR 250.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Adat-Jawa.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/adat-detail/jawa/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bxl-dribbble"></i></div>
                  <h4><a href="">Adat Bali</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 500.000</del><p>
                  <p>IDR 250.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Adat-Bali.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="#product" class="btn btn-dark btn-sm">Coming Soon</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bxl-dribbble"></i></div>
                  <h4><a href="">Adat Minang</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 500.000</del><p>
                  <p>IDR 250.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Adat-Minang.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="#product" class="btn btn-dark btn-sm">Coming Soon</a>
                </div>
              </div>
            </div>
          </div> -->

          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <h4 class="text-success"><a href="">Diamond 1</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 400.000</del><p>
                  <p>IDR 200.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Diamond-1.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="#product" class="btn btn-dark btn-sm">Coming Soon</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <h4><a href="">Diamond 2</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 400.000</del><p>
                  <p>IDR 200.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Diamond-2.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="#product" class="btn btn-dark btn-sm">Coming Soon</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-file"></i></div>
                  <h4><a href="">Diamond 3</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 400.000</del><p>
                  <p>IDR 200.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Diamond-3.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/diamond-detail/diamond3/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-file"></i></div>
                  <h4><a href="">Diamond 4</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 400.000</del><p>
                  <p>IDR 200.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Diamond-4.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/diamond-detail/diamond4/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-file"></i></div>
                  <h4><a href="">Diamond 5</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 400.000</del><p>
                  <p>IDR 200.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Diamond-Mock.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/diamond-detail/diamond5/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <!-- <div class="icon"><i class="bx bx-tachometer"></i></div> -->
                  <h4><a href="">Platinum 1</a></h4>
                  <p>Disc Up to 45%</p>
                  <p class="text-danger"><del>IDR 299.000</del><p>
                  <p>IDR 169.900<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Platinum-1.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/platinum-detail/platinum1/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Updating</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <!-- <div class="icon"><i class="bx bx-tachometer"></i></div> -->
                  <h4><a href="">Platinum 2</a></h4>
                  <p>Disc Up to 45%</p>
                  <p class="text-danger"><del>IDR 299.000</del><p>
                  <p>IDR 169.900<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Platinum-2.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/platinum-detail/platinum2/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Updating...</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-tachometer"></i></div>
                  <h4><a href="">Platinum 3</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 300.000</del><p>
                  <p>IDR 150.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Platinum-3.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/platinum-detail/platinum3/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-tachometer"></i></div>
                  <h4><a href="">Platinum 4</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 300.000</del><p>
                  <p>IDR 150.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Platinum-4.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/platinum-detail/platinum4/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-tachometer"></i></div>
                  <h4><a href="">Platinum 5</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 300.000</del><p>
                  <p>IDR 150.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Platinum-5.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/platinum-detail/platinum5/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div> -->
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <!-- <div class="icon"><i class="bx bx-world"></i></div> -->
                  <h4><a href="">Gold 1</a></h4>
                  <p>Disc Up to 30%</p>
                  <p class="text-danger"><del>IDR 199.000</del><p>
                  <p>IDR 139.900<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Gold-1.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/gold-detail/gold1/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Coming Soon</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <!-- <div class="icon"><i class="bx bx-world"></i></div> -->
                  <h4><a href="">Gold 2</a></h4>
                  <p>Disc Up to 30%</p>
                  <p class="text-danger"><del>IDR 199.000</del><p>
                  <p>IDR 139.900<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Gold-2.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/gold-detail/gold2/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Coming Soon</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-world"></i></div>
                  <h4><a href="">Gold 3</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 200.000</del><p>
                  <p>IDR 100.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Gold-3.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/gold-detail/gold3/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-world"></i></div>
                  <h4><a href="">Gold 4</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 200.000</del><p>
                  <p>IDR 100.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Gold-4.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/gold-detail/gold4/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-world"></i></div>
                  <h4><a href="">Gold 5</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 200.000</del><p>
                  <p>IDR 100.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Gold-5.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/gold-detail/gold5/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div> -->

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <h4><a href="">Silver 1</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 99.000</del><p>
                  <p>IDR 49.500<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Silver-1.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/silver-detail/silver1/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Coming Soon</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mb-3">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <h4><a href="">Silver 2</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 99.000</del><p>
                  <p>IDR 49.500<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Silver-2.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/silver-detail/silver2/preview?to=Nama Tamu" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                  <!-- <a href="#product" class="btn btn-dark btn-sm disabled">Coming Soon</a> -->
                </div>
              </div>
            </div>
          </div>
          
          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-slideshow"></i></div>
                  <h4><a href="">Silver 3</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 100.000</del><p>
                  <p>IDR 50.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Silver-3.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/silver-detail/silver3/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-slideshow"></i></div>
                  <h4><a href="">Silver 4</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 100.000</del><p>
                  <p>IDR 50.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Silver-4.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/silver-detail/silver4/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
            <div class="icon-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="icon"><i class="bx bx-slideshow"></i></div>
                  <h4><a href="">Silver 5</a></h4>
                  <p>Disc 50%</p>
                  <p class="text-danger"><del>IDR 100.000</del><p>
                  <p>IDR 50.000<p>
                </div>
                <div class="col-md-6 col-sm-6">
                  <img src="public/landingpage/images/Silver-5.png" style="width: 150px" alt="">
                </div>
                <div class="col-md-12 col-sm-12">
                  <a href="product/silver-detail/silver5/preview" class="btn btn-dark btn-sm"><i class="fa fa-eye"></i> Preview</a>
                </div>
              </div>
            </div>
          </div> -->

          <!-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-arch"></i></div>
              <h4><a href="">Divera don</a></h4>
              <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
            </div>
          </div> -->

        </div>

        <br>
        <div class="row content">
          <div class="col-lg-6">
            <p>
              <b>Fitur</b>
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Nama URL undangan</li>
              <li><i class="ri-check-double-line"></i> Unlimited custom nama tamu undangan</li>
              <li><i class="ri-check-double-line"></i> Cover pembuka</li>
              <li><i class="ri-check-double-line"></i> Data profil mempelai</li>
              <li><i class="ri-check-double-line"></i> Countdown timer</li>
              <li><i class="ri-check-double-line"></i> Navigasi lokasi pernikahan (Google Maps)</li>
              <li><i class="ri-check-double-line"></i> Love story</li>
              <li><i class="ri-check-double-line"></i> Galeri foto (10 foto)</li>
              <li><i class="ri-check-double-line"></i> Video prewed (1 Video)</li>
              <li><i class="ri-check-double-line"></i> Backsound music</li>
              <li><i class="ri-check-double-line"></i> Live streaming (Zoom/ Google Meet/ Youtube Live/ Instagram Live)</li>
              <li><i class="ri-check-double-line"></i> Amplop gift / Online gift</li>
              <li><i class="ri-check-double-line"></i> Panduan protokol kesehatan</li>
              <li><i class="ri-check-double-line"></i> Ucapan tamu undangan & konfirmasi kehadiran</li>
              <li><i class="ri-check-double-line"></i> Masa aktif 6 bulan</li>
            </ul>
          </div>
          <div class="col-lg-6">
            <p>
              <b>Fitur Tambahan</b>
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Revisi Minor (15k)</li>
              <li><i class="ri-check-double-line"></i> Custom/Tambahan Mayor (Start from 50k)</li>
              <!-- <li><i class="ri-check-double-line"></i> Konfirmasi Kehadiran (20k)</li> -->
              <!-- <li><i class="ri-check-double-line"></i> Pengingat Acara (15k)</li> -->
              <li><i class="ri-check-double-line"></i> Tambah foto Galeri (25k)</li>
              <!-- <li><i class="ri-check-double-line"></i> Tambah foto Love Story (25k)</li> -->
              <!-- <li><i class="ri-check-double-line"></i> Custom Font (15k)</li> -->
              <li><i class="ri-check-double-line"></i> Custom Warna (50k)</li>
              <li><i class="ri-check-double-line"></i> Request Musik / Backsong (20k)</li>
              <li><i class="ri-check-double-line"></i> Express 1 hari jadi (50k) <span style="font-size: 10px; color: red">*</span></li>
              <!-- <li><i class="ri-check-double-line"></i> Ucapan tamu (50k)</li> -->
              <li><i class="ri-check-double-line"></i> Custom Domain (50k) <span style="font-size: 10px; color: red">* Harga belum termasuk harga sewa domain</span></li>
              <!-- <li><i class="ri-check-double-line"></i> Fitur Buku Tamu (Coming Soon)</li> -->
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- End Services Section -->
    
    <!-- ======= Info Section ======= -->
    <section id="info" class="about">
      <div class="container">

        <div class="section-title">
          <h2>Info</h2>
          <p>Ara Invitation menerima pembuatan undangan pernikahan website yang sangat menarik dan interaktif dengan harga yang sangat terjangkau untuk semua kalangan. Banyak promo yang bisa ada dapatkan setiap harinya.</p>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
              <b>Disclaimer</b>
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Untuk tampilan terbaik, silahkan akses link melalui browser Chrome.</li>
              <li><i class="ri-check-double-line"></i> Pastikan menggunakan browser dengan versi yang terbaru.</li>
              <li><i class="ri-check-double-line"></i> Undangan tidak support force Darkmode dari gadget/browser. Karena akan berpengaruh pada tampilan undangan.</li>
            </ul>
          </div>
          <div class="col-lg-6">
            <p>
              <b>Estimasi Pengerjaan</b>
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> 5 hari.</li>
              <!-- <li><i class="ri-check-double-line"></i> Pastikan menggunakan browser dengan versi yang terbaru.</li> -->
              <!-- <li><i class="ri-check-double-line"></i> Undangan tidak support force Darkmode dari gadget/browser. Karena akan berpengaruh pada tampilan undangan.</li> -->
            </ul>
          </div>
          <!-- <div class="col-lg-6 pt-4 pt-lg-0">
            <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
              culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <a href="#" class="btn-learn-more">Learn More</a>
          </div> -->
        </div>

      </div>
    </section>
    <!-- End Info Section -->

    <!-- ======= Cta Section ======= -->
    <!-- <section id="cta" class="cta">
      <div class="container">

        <div class="row">
          <div class="col-lg-9 text-center text-lg-start">
            <h3>Call To Action</h3>
            <p> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Call To Action</a>
          </div>
        </div>

      </div>
    </section> -->
    <!-- End Cta Section -->

    <!-- ======= Features Section ======= -->
    <!-- <section id="features" class="features">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 order-2 order-lg-1">
            <div class="icon-box mt-5 mt-lg-0">
              <i class="bx bx-receipt"></i>
              <h4>Est labore ad</h4>
              <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
            </div>
            <div class="icon-box mt-5">
              <i class="bx bx-cube-alt"></i>
              <h4>Harum esse qui</h4>
              <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p>
            </div>
            <div class="icon-box mt-5">
              <i class="bx bx-images"></i>
              <h4>Aut occaecati</h4>
              <p>Aut suscipit aut cum nemo deleniti aut omnis. Doloribus ut maiores omnis facere</p>
            </div>
            <div class="icon-box mt-5">
              <i class="bx bx-shield"></i>
              <h4>Beatae veritatis</h4>
              <p>Expedita veritatis consequuntur nihil tempore laudantium vitae denat pacta</p>
            </div>
          </div>
          <div class="image col-lg-6 order-1 order-lg-2" style='background-image: url("public/landingpage/assets/img/features.jpg");'></div>
        </div>

      </div>
    </section> -->
    <!-- End Features Section -->

    <!-- ======= Clients Section ======= -->
    <!-- <section id="clients" class="clients">
      <div class="container">

        <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-1.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-2.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-3.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-4.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-5.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-6.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-7.png" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-3 col-md-4 col-xs-6">
            <div class="client-logo">
              <img src="public/landingpage/assets/img/clients/client-8.png" class="img-fluid" alt="">
            </div>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Clients Section -->

    <!-- ======= Counts Section ======= -->
    <!-- <section id="counts" class="counts">
      <div class="container">

        <div class="text-center title">
          <h3>What we have achieved so far</h3>
          <p>Iusto et labore modi qui sapiente xpedita tempora et aut non ipsum consequatur illo.</p>
        </div>

        <div class="row counters position-relative">

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" class="purecounter"></span>
            <p>Projects</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1" class="purecounter"></span>
            <p>Hours Of Support</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1" class="purecounter"></span>
            <p>Hard Workers</p>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Counts Section -->

    <!-- ======= Portfolio Section ======= -->
    <!-- <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <h2>Portfolio</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="public/landingpage/assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 1</h4>
              <p>App</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="public/landingpage/assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-2.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="public/landingpage/assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 2</h4>
              <p>App</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="public/landingpage/assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 2</h4>
              <p>Card</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="public/landingpage/assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 2</h4>
              <p>Web</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="public/landingpage/assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 3</h4>
              <p>App</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-6.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="public/landingpage/assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 1</h4>
              <p>Card</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-7.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="public/landingpage/assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 3</h4>
              <p>Card</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-8.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="public/landingpage/assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="public/landingpage/assets/img/portfolio/portfolio-9.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Portfolio Section -->

    <!-- ======= Pricing Section ======= -->
    <!-- <section id="pricing" class="pricing">
      <div class="container">

        <div class="section-title">
          <h2>Pricing</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="box">
              <h3>Free</h3>
              <h4><sup>$</sup>0<span> / month</span></h4>
              <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li class="na">Pharetra massa</li>
                <li class="na">Massa ultricies mi</li>
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 mt-4 mt-md-0">
            <div class="box recommended">
              <span class="recommended-badge">Recommended</span>
              <h3>Business</h3>
              <h4><sup>$</sup>19<span> / month</span></h4>
              <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li>Pharetra massa</li>
                <li class="na">Massa ultricies mi</li>
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 mt-4 mt-lg-0">
            <div class="box">
              <h3>Developer</h3>
              <h4><sup>$</sup>29<span> / month</span></h4>
              <ul>
                <li>Aida dere</li>
                <li>Nec feugiat nisl</li>
                <li>Nulla at volutpat dola</li>
                <li>Pharetra massa</li>
                <li>Massa ultricies mi</li>
              </ul>
              <div class="btn-wrap">
                <a href="#" class="btn-buy">Buy Now</a>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Pricing Section -->

    <!-- ======= Faq Section ======= -->
    <section id="faq" class="faq">
      <div class="container-fluid">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3><strong>FAQ</strong></h3>
              <p>
                Pertanyaan yang sering diajukan
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapsed" data-bs-target="#accordion-list-1">Bagaimana cara pemesanannya? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse" data-bs-parent=".accordion-list">
                    <p>1. Menentukan desain yang telah disediakan (bisa dikonsultasikan dengan admin sesuai kebutuhan).</p>
                    <p>2. Mengisi format order sesuai dengan ketentuan.</p>
                    <p>3. Konfirmasi pembayaran (pembayaran dilakukan full payment karena setiap pemesan yang masuk akan diproses setelah pembayaranb lunas).</p>
                    <p>4. Mengisi form data pernikahan yang sudah disediakan.</p>
                    <p>5. Proses pembuatan (estimasi pengerjaan 5 hari setelah melakukan order & melengkapi data).</p>
                    <p>6. Setelah selesai, undangan akan dikirim ke pemesan.</p>
                    <p>7. Silahkan lakukan review hasil undangan apakah ada yang perlu dikoreksi atau revisi.</p>
                    <p>8. Jika ada revisi, infokan list revisi apa saja yang diperlukan jadi silahkan langsung list revisinya secara keseluruhan karena jatah revisi terbatas. Jadi silahkan manfaatkan seefektif mungkin.</p>
                    <p>9. Jika telah memberikan list revisi, revisi akan dikerjakan hari itu juga atau h+1</p>
                    <p>10. DONE! Undangan siap disebar dengan tools yang telah kami sediakan.</p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed">Berapa lama proses pembuatannya? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>Pengerjaan 5 hari setelah melakukan order & melengkapi data</p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed">Kapan data harus lengkap? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>Maksimal 3 hari setelah melakukan pemesanan</p>
                  </div>
                </li>
                
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed">Siapa yang mengisi nama tamu? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                    <p>Untuk nama tamu nanti yang mengisi kaka sendiri ya, setelah undangan jadi akan kami kasih tools & memberitahu cara mengisi dan mengubah nama tamunya.</p>
                  </div>
                </li>
                
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5" class="collapsed">Apa itu masa aktif website? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-5" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Setiap paket undangan website memiliki masa aktif yang berbeda. <br><br>
                      Masa Aktif artinya seberapa lama link undangan website kakak dapat diakses oleh publik. Terhitung dari sejak tanggal acara undangan website kakak. <br><br>
                      Setelah melewati masa aktif, undangan website kakak akan memasuki masa expired secara otomatis dan tidak lagi dapat diakses oleh siapapun.</p>
                  </div>
                </li>
                
                <!-- <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-6" class="collapsed">Bisa upgrade tema yang dipesan tidak? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-6" class="collapse" data-bs-parent=".accordion-list">
                    <p>Bisa, asal konfirmasi terlebih dahulu sebelum proses pengerjaan</p>
                  </div>
                </li> -->
                
                <!-- <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-7" class="collapsed">Ucapan pada undangan website apakah bisa dihapus? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-7" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Ucapan yang telah diisi oleh tamu undangan pada undangan website BISA DIHAPUS. <br><br>
                      Ketentuan untuk undangan dengan ucapan langsung di Website : <br><br>
                      Jika ada komentar atau ucapan yang tidak diinginkan, bisa dihapus sesuai request.
                    </p>
                  </div>
                </li> -->

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("public/landingpage/assets/img/faq.jpg");'>&nbsp;</div>
        </div>

      </div>
    </section><!-- End Faq Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Kontak</h2>
          <p>Hubungi kami melalui kontak dibawah ini.</p>
        </div>
      </div>

      <div>
        <iframe style="border:0; width: 100%; height: 350px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15800.864854629168!2d111.93943276662193!3d-8.079418356927109!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e78e37f1ddb7057%3A0x522305a0c55ac0bd!2sSumberdadi%2C%20Sumbergempol%2C%20Tulungagung%20Regency%2C%20East%20Java!5e0!3m2!1sen!2sid!4v1652158651699!5m2!1sen!2sid" frameborder="0" allowfullscreen></iframe>
      </div>

      <div class="container">

        <div class="row mt-5">

          <div class="col-lg-12">
            <div class="info">
              <div class="address">
                <i class="ri-map-pin-line"></i>
                <h4>Lokasi:</h4>
                <p><?= $admin_address ?></p>
              </div>

              <div class="email">
                <i class="ri-instagram-line"></i>
                <h4>Instagram:</h4>
                <p>@<?= $admin_ig ?></p>
              </div>

              <div class="phone">
                <i class="ri-phone-line"></i>
                <h4>Call / WA:</h4>
                <p>+<?= $admin_wa ?></p>
              </div>

            </div>

          </div>

          <!-- <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>

          </div> -->

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>Ara Invitation</h3>
      <p>Jasa pembuatan undangan website murah namun hasil tidak murahan</p>
      <div class="social-links">
        <!-- <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a> -->
        <!-- <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a> -->
        <a href="https://api.whatsapp.com/send?phone=<?= $admin_wa ?>&#038;text=<?= $template_question_wa ?>" class="whatsapp"><i class="bx bxl-whatsapp"></i></a>
        <a href="https://instagram.com/<?= $admin_ig ?>" class="instagram"><i class="bx bxl-instagram"></i></a>
        <!-- <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a> -->
        <!-- <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> -->
      </div>
      <div class="copyright">
        &copy; Copyright <strong><span>Ara Invitation</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-simple-one-page-bootstrap-template/ -->
        <!-- Designed by <a href="https://instagram.com/syaifulfu">CPG</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="public/landingpage/assets/vendor/purecounter/purecounter.js"></script>
  <script src="public/landingpage/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="public/landingpage/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="public/landingpage/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="public/landingpage/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="public/landingpage/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="public/landingpage/assets/js/main.js"></script>

</body>

</html>